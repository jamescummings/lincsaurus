---
sidebar_position: 6
title: "Physical Objects Application Profile"
description: "Represent physical objects"
---

## Purpose

To document how various facets of LINCS data are modelled, along with reference :Term[authorities]{#authority-file} for the populating :Term[vocabularies]{#vocabulary}. This will provide a basis for instruction for how to model data in a LINCS-compatible manner, as well as aid in navigation and discovery.

“Physical Objects” describes patterns that are unique or specific to representing information about physical objects, and includes reference to their relationship to conceptual things.

This document introduces the concepts as used by LINCS, and are not complete definitions of the :Term[CIDOC CRM]{#cidoc-crm} ontology class or :Term[property]{#property} concepts. Consult [CIDOC CRM v. 7.1.1 documentation](https://cidoc-crm.org/Version/version-7.1.1) for full class descriptions and property descriptions.

## Acronyms

**Ontology Acronyms:**

* CRM - [CIDOC Conceptual Reference Model](http://cidoc-crm.org/)
* CRMtex - [Model for the Study of Ancient Texts](https://cidoc-crm.org/crmtex/)

**Vocabulary and Authority Acronyms:**

* AAT - [Getty Art & Architecture Thesaurus](https://www.getty.edu/research/tools/vocabularies/aat/)
* LINCS - LINCS minted entities
* Wikidata - [Wikimedia Knowledge Base](https://www.wikidata.org/wiki/Wikidata:Main_Page)

## Written and Ancient Texts: CRMtex

CRMtex is used for domain-specific requirements of written and ancient texts; LINCS uses aspects of it which extend the sections of CIDOC CRM related to Physical Objects.

## Main Classes

<table>
  <tr>
    <td>
      <strong>Entity type</strong>
    </td>
    <td>
      <strong>Class</strong>
    </td>
    <td>
      <strong>Declaration Snippet (TTL)</strong>
    </td>
  </tr>
  <tr>
    <td>Human-Made Object</td>
    <td>crm:E22_Human-Made_Object</td>
    <td>
        ```turtle
        <object> a crm:E22_Human-Made_Object ;
          rdfs:label "<object label>" .
        ```
    </td>
  </tr>
  <tr>
    <td>Identifier</td>
    <td>crm:E42_Identifier</td>
    <td>
        <code>
          &lt;identifier> a crm:E42_Identifier ;
          <br />
          rdfs:label “&lt;identifier>” .
        </code>
    </td>
  </tr>
  <tr>
    <td>Title</td>
    <td>crm:E33_E41_Linguistic_Appellation</td>
    <td>
      <p>
        <code>
          &lt;title> a crm:E33_E41_Linguistic_Appellation ;
          <br />
          rdfs:label “&lt;title>” .
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>Classification</td>
    <td>crm:E55_Type</td>
    <td>
      <p>
        <code>
          &lt;classification> a crm:E55_Type ;
          <br />
          rdfs:label “&lt;classification>” .
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>Credit line, statement, note, description, etc.</td>
    <td>crm:E33_Linguistic_Object</td>
    <td>
      <p>
        <code>
          &lt;text> a crm:E33_Linguistic_Object ;
          <br />
          rdfs:label “&lt;text_descriptor>” .
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>Actors (e.g., makers, owners)</td>
    <td>crm:E39_Actor \ (subclasses E21, E74 where possible)</td>
    <td>
      <p>
        <code>
          &lt;actor> a crm:E39_Actor ;
          <br />
          rdfs:label “&lt;actor>” .
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>Production</td>
    <td>crm:E12_Production</td>
    <td>
      <p>
        <code>
          &lt;production> a crm:E12_Production ;
          <br />
          rdfs:label “Production event of &lt;project> object
          &lt;object_identifier>” .
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>Material</td>
    <td>crm:E57_Material</td>
    <td>
      <p>
        <code>
          &lt;medium> a crm:E57_Material ;
          <br />
          rdfs:label “&lt;medium>” .
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>Dimension</td>
    <td>crm:E54_Dimension</td>
    <td>
      <p>
        <code>
          &lt;dimension> a crm:E54_Dimension ;
          <br />
          rdfs:label “&lt;dimension>” .
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>Measurement Unit</td>
    <td>crm:E58_Measurement_Unit</td>
    <td>
      <p>
        <code>
          &lt;measurement_unit> a crm:E58_Measurement_Unit ;
          <br />
          rdfs:label “&lt;measurement_unit>” .
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>Acquisition</td>
    <td>E8_Acquisition</td>
    <td>
      <p>
        <code>
          &lt;acquisition> a crm:E8_Acquisition ;
          <br />
          rdfs:label “Acquisition event of &lt;object>” .
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>Collection</td>
    <td>crm:E78_Curated_Holding</td>
    <td>
      <p>
        <code>
          &lt;collection> a crm:E78_Curated_Holding ;
          <br />
          rdfs:label “&lt;collection>” .
        </code>
      </p>
    </td>
  </tr>
</table>

## Overview Diagram

Below is an image of the application profile overview diagram. Follow this link for a [zoomable, more readable version](https://drive.google.com/file/d/1sJEju5QjJ_cuooMEHpk8fin6Hfg4yvBg/view?usp=share_link). The segments below align with the document sections.

![Application profile overview diagram.](/img/documentation/application-profile-physical-overview-(c-LINCS).jpg)

## Nodes

### Physical Objects; Conceptual Texts, Images, and Ideas

Physical things consist of all persistent physical items with a relatively stable form, human-made or natural. They are carriers for conceptual objects. Conceptual objects comprise non-material products of our minds and other human produced data. They can exist on more than one carrier at the same time, such as paper, electronic signals, marks, audio media, paintings, photos, human memories, etc. Their existence ends when the last carrier and the last memory are lost.

#### Physical Carriers of Conceptual Things

Physical things can be carriers of conceptual elements: for example, a physical book carries its text, or a physical collection carries the intellectual contents of that collection.

![Application profile physical carriers](/img/documentation/application-profile-physical-carriers-(c-LINCS).png)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>
      This pattern declares that a physical thing serves as a carrier for
      conceptual things.
    </td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      <code>
        crm:E18_Physical Thing → crm:P128_carries →{" "}
        <strong>crm:E73_Information_Object</strong>
      </code>
    </td>
  </tr>
  <tr>
    <td rowspan="3">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>
      URI from project dataset, existing linked data authority, or minted by
      LINCS
    </td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>URI (preferably dereferenceable)</td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      <p>
        The HistSex dataset states that the Northwest Lesbian and Gay History
        Project archive carries the intellectual contents of that archival
        collection.
      </p>
      <p>
        <code>
          &lt;lincs:Northwest_Lesbian_and_Gay_History_Project> → crm:P128_carries **&lt;lincs:Northwest_Lesbian_and_Gay_History_Project_Contents>**
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to This Pattern</strong>
    </td>
    <td>
      crm:E18_Physical Thing is not directly used in the data, but represents
      here subclasses such as crm:E22_Human-Made_Object,
      crm:E78_Curated_Holding, crmtex:T1_Written_Text, and
      crmtex:TX7_Written_Text_Fragment that are used for this pattern.
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Projects Following This Pattern</strong>
    </td>
    <td>Anthologia graeca, Canadian Centre for Ethnomusicology, HistSex</td>
  </tr>
</table>

**Pattern in TTL:**

```turtle
<object> a crm:E18_Physical_Thing ; 
    rdfs:label "<object>" ;
    crm:P128_carries <conceptual_thing> .

<conceptual_thing> a crm:E73_Information_Object ; 
    rdfs:label "<conceptual_thing>" .
```

#### Transcribing Written Texts (CRMtex)

![Application profile transcribing text](/img/documentation/application-profile-physical-transcribingtext-(c-LINCS).png)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>This pattern declares that a written text has a transcription.</td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      <p>
        <code>
          crmtex:TX7_Written_Text_Segment → crm:P16i_was_used_for →
          TX6_Transcription, crm:E65_Creation
          <br />→ crm:P94_has_created → **crm:E33_Linguistic_Object**
          <pbr />→ crm:P190_has_symbolic_content → <strong>rdfs:Literal</strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td rowspan="3">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI); literal value (text)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>
      URI minted by LINCS; literal value (text) from Anthologia graeca dataset
    </td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>URI (preferably dereferenceable); rdfs:literal</td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      <p>
        The Anthologia graeca dataset declares that a passage was transcribed.
      </p>
      <p>
        <code>
          &lt;lincs:passage/realization/1298> → crm:P16i_was_used_for →
          TX6_Transcription, crm:E65_Creation
          <br />→ crm:P94_has_created → <strong>&lt;lincs:text/4392></strong>
          <br />→ crm:P190_has_symbolic_content →{" "}
          <strong>
            “En jouant de la trompette, Marcus le fluet émit un tout petit
            souffle et, tout droit, tête première, il s’en alla dans l’Hadès!”
          </strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to This Pattern</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Projects Following This Pattern</strong>
    </td>
    <td>Anthologia graeca</td>
  </tr>
</table>

**Pattern in TTL:**

```turtle
<written_text> a crmtex:TX1_Written_Text ;
    rdfs:label "<written_text>" ; 
    crm:P16i_was_used_for <transcription_event> .

<transcription_event> a crmtex:TX6_Transcription, crm:E65_Creation ;
    rdfs:label "Transcription of <written_text>" ;
    crm:P94_has_created <text> .

<text> a crm:E33_Linguistic_Object ;
    rdfs:label "Transcription text of <written_text>" ; 
    crm:P190_has_symbolic_content "<text>" .
```

#### Talking about Entities: References to, and Representations of, Physical Things

When a physical object is referenced by a text or is represented in an image, this is a case of a conceptual object linking to a physical thing. For more on references to and representations of things, see the [Talking about Entities section of the Basic Patterns Application Profile](/docs/explore-data/understand-lincs-data/application-profiles-main/basic-patterns#talking-about-entities).

##### Curatorial Notes, Descriptions, and Other Comments

Different types of references are differentiated by the P2_has_type → E55_Type pattern associated with them (for more on this, see the [Types section of the Basic Patterns Application Profile](/docs/explore-data/understand-lincs-data/application-profiles-main/basic-patterns#types)). For example:

* General description: `http://vocab.getty.edu/aat/300411780`
* Descriptive note: `http://vocab.getty.edu/aat/300435416`
* Comment or note: `http://vocab.getty.edu/aat/300027200`
* Materials statement: `http://vocab.getty.edu/aat/300435429`
* Dimensions statement: `http://vocab.getty.edu/aat/300435430`
* Credit line: `http://vocab.getty.edu/aat/300435418`

![Application profile notes](/img/documentation/application-profile-physical-notes-(c-LINCS).png)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>This pattern declares that an entity has a note.</td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      <p>
        <code>
          crm:E1_CRM_Entity → crm:P67i_is_referred_to_by →
          crm:E33_Linguistic_Object
          <br />→ crm:P2_has_type → <strong>crm:E55_Type</strong>
          <br />→ crm:P190_has_symbolic_content → <strong>rdfs:literal</strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td rowspan="3">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI); literal value (text)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>
      URI from project dataset, existing linked data authority, or minted by
      LINCS; literal value from project dataset
    </td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>URI (preferably dereferenceable); rdfs:literal</td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      <p>
        The University of Saskatchewan Art Gallery dataset states that there is
        an object that has a statement describing the materials used to make it.
      </p>
      <p>
        <code>
          &lt;usask:2899> → crm:P67i_is_referred_to_by →
          crm:E33_Linguistic_Object
          <br />→ crm:P2_has_type → <strong>aat:300435429 </strong>
          <br />→ crm:P190_has_symbolic_content →{" "}
          <strong>“cibachrome on fujicolor paper”</strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>
      <p>
        The Map of Early Modern London Gazetteer states that St. Saviour
        (Southwark) is the main subject of a text.
      </p>
      <p>
        <code>
          &lt;moeml:STSA1> → crm:P129i_is_subject_of → crm:E33_Linguistic_Object
          <br />→ crm:P2_has_type → <strong>&lt;lincs:description></strong>
          <br />→ crm:P190_has_symbolic_content →{" "}
          <strong>
            “St. Saviour (Southwark) dates back at least to 1106. It was
            originally known by the name St. Mary Overies, with Overies
            referring to its being over the Thames, that is, on its southern
            bank. After the period of the Dissolution, the church was
            rededicated and renamed St. Saviour (Sugden 335). St. Saviour
            (Southwark) is visible on the Agas map along New Rents street in
            Southwark. It is marked with the label S. Mary Owber.”
          </strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>
      The Getty Research Institute. (2017, March 7).{" "}
      <em>Art & Architecture Thesaurus Online</em>.{" "}
      <a href="https://www.getty.edu/research/tools/vocabularies/aat/">
        https://www.getty.edu/research/tools/vocabularies/aat/
      </a>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to The Pattern</strong>
    </td>
    <td>
      <p>
        When using E33_Linguistic_Object, use at least one (1) E55_Type on each
        instance specifying what kind of text it is.
      </p>
      <p>
        This pattern can be used for any information item that discusses the
        entity in question. When seeking to say that the entity is not just
        discussed or referenced by the text statement, but is the main subject
        of it, P129i_is_subject_of can be used instead.
      </p>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Projects Following This Pattern</strong>
    </td>
    <td>All projects</td>
  </tr>
</table>

**Pattern in TTL:**

```turtle
<entity> a crm:E1_CRM_Entity ; 
    rdfs:label "<entity>" ;
    crm:P67i_is_referred_to_by <entity_note> .

<entity_note> a crm:E33_Linguistic_Object ; 
    rdfs:label "<type> statement for <entity>" ;
    crm:P2_has_type <type> ; 
    crm:P190_has_symbolic_content “<note>” .

<type> a crm:E55_Type ; 
    rdfs:label "<type>" .
```

##### Visual Representation

For more on how to say that an object has a visual representation, see the [Visual Representations of an Entity Identifiers section of the Basic Patterns Application Profile](/docs/explore-data/understand-lincs-data/application-profiles-main/basic-patterns#visual-representations-of-an-entity).

![Application profile visual representation](/img/documentation/application-profile-physical-visualrepresentation-(c-LINCS).png)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>This pattern declares that an entity is represented by an image.</td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      <code>
        crm:E1_CRM_Entity → crm:P138i_is_represented_by →{" "}
        <strong>crm:E36_Visual_Item</strong>
      </code>
    </td>
  </tr>
  <tr>
    <td rowspan="3">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>
      URI from project dataset, existing linked data authority, or minted by
      LINCS
    </td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>URI, preferably dereferenceable</td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      <p>
        The University of Saskatchewan Art Gallery dataset states that there is
        an object that has an image which is a visual representation of it.
      </p>
      <p>
        <code>
          &lt;usask:2899> → crm:P138i_has_representation →{" "}
          <strong>
            &lt;https://saskcollections.org/kenderdine/media/sask_kenderdine/images/7/5/61983_ca_object_representations_media_7585_page.jpg>
          </strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to the Pattern</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Projects Following this Pattern</strong>
    </td>
    <td>
      AdArchive, Anthologia graeca, HistSex, University of Saskatchewan Art
      Gallery
    </td>
  </tr>
</table>

**Pattern in TTL:**

```turtle
<entity> a crm:E1_CRM_Entity ; 
    rdfs:label "<entity>" ;
    crm:P138i_has_representation <image> .

<image> a crm:E36_Visual_Item ; 
    rdfs:label "Image of <entity>" .
```

### Identifiers

For more on identifiers, see the [Identifiers section of the Basic Patterns Application Profile](/docs/explore-data/understand-lincs-data/application-profiles-main/basic-patterns#identifiers).

#### Unique Identifiers (e.g., Accession Numbers)

Different types of identifiers are differentiated by the P2_has_type → E55_Type pattern associated with them. For more on this, see the [Types section of the Basic Patterns Application Profile](/docs/explore-data/understand-lincs-data/application-profiles-main/basic-patterns#types). For example:

> Accession number: `http://vocab.getty.edu/aat/300312355`

![Application profile identifiers](/img/documentation/application-profile-physical-identifiers-(c-LINCS).png)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>This pattern declares that an entity has an identifier.</td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      <code>
      crm\:E1_CRM_Entity → crm\:P1_is_identified_by → crm\:E42_Identifier
      <br />→ crm\:P2_has_type → <strong>crm\:E55_Type[aat\:300404012]</strong>
      <br />→ crm\:P190_has_symbolic_content → <strong>rdfs\:literal</strong>
      </code>
    </td>
  </tr>
  <tr>
    <td rowspan="3">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI); literal value (text)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>
      URI from project dataset, existing linked data authority, or minted by
      LINCS; literal value from project dataset
    </td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>URI (preferably dereferenceable); rdfs:literal</td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      <p>
        The University of Saskatchewan Art Gallery dataset states that there is
        an object that is identified by the accession number “2020.008.008”.
      </p>
      <p>
        <code>
          &lt;usask:3359> → crm:P1_is_identified_by → crm:E42_Identifier
          <br />→ crm:P2_has_type → **&lt;aat:300404012>, &lt;aat:300312355>, &lt;lincs:gVZ8ETZwMQS>**
          <br />→ crm:P190_has_symbolic_content →{" "}
          <strong>“2020.008.008”</strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>
      The Getty Research Institute. (2017, March 7).{" "}
      <em>Art & Architecture Thesaurus Online</em>.{" "}
      <a href="https://www.getty.edu/research/tools/vocabularies/aat/">
        https://www.getty.edu/research/tools/vocabularies/aat/
      </a>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to This Pattern</strong>
    </td>
    <td>
      Use at least two (2) E55_Types on each identifier: generic (from existing linked data vocabulary) or project-specific (minted for project). For generic, always use the{" "}
      <a href="http://vocab.getty.edu/aat/300404012">Getty AAT term for unique identifiers</a>. 
      Additional are optional for precision (e.g., the <a href="http://vocab.getty.edu/aat/300312355">  Getty AAT term for accession numbers</a>
      ).
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Projects Following This Pattern</strong>
    </td>
    <td>All projects</td>
  </tr>
</table>

**Pattern in TTL:**

```turtle
<entity> a crm:E1_CRM_Entity ;  
    rdfs:label "<entity>" ;
    crm:P1_is_identified_by <entity_identifier> .

<entity_identifier> a crm:E42_Identifier ;
    rdfs:label "Unique identifier of <entity>" 
    crm:P2_has_type <http://vocab.getty.edu/aat/300404012>, 
        <project_identifier_type> ; 
    crm:P190_has_symbolic_content "<entity_identifier>" .

<http://vocab.getty.edu/aat/300404012> a crm:E55_Type ; 
    rdfs:label "unique identifiers" . 

<project_identifier_type> a crm:E55_Type ; 
    rdfs:label "<project> identifiers" .
```

#### Titles

Different types of identifiers are differentiated by the P2_has_type → E55_Type pattern associated with them. For more on this, see the [Types section of the Basic Patterns Application Profile](/docs/explore-data/understand-lincs-data/application-profiles-main/basic-patterns#types). For example:

> Title: `http://vocab.getty.edu/aat/300417193`

![Application profile titles](/img/documentation/application-profile-physical-titles-(c-LINCS).png)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>This pattern declares that an entity has a linguistic identifier.</td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      <p>
        <code>
          crm:E1_CRM_Entity → crm:P1_is_identified_by →
          crm:E33_E41_Linguistic_Appellation
          <br />
          crm:P2_has_type → <strong>crm:E55_Type</strong>
          <br />
          crm:P190_has_symbolic_content → <strong>rdfs:literal</strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td rowspan="3">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI); literal value (text)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>
      URI from project dataset, existing linked data authority, or minted by
      LINCS; literal value from project dataset
    </td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>URI (preferably dereferenceable); rdfs:literal</td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      <p>
        The University of Saskatchewan Art Gallery dataset states that there is
        an object with the title “Antics.”
      </p>
      <p>
        <code>
          &lt;usask:3359> → crm:P1_is_identified_by →
          crm:E33_E41_Linguistic_Appellation
          <br />
          crm:P2_has_type → <strong>&lt;aat:300417193></strong>
          <br />
          crm:P190_has_symbolic_content → <strong>“Antics”</strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>
      The Getty Research Institute. (2017, March 7).{" "}
      <em>Art & Architecture Thesaurus Online</em>.{" "}
      <a href="https://www.getty.edu/research/tools/vocabularies/aat/">
        https://www.getty.edu/research/tools/vocabularies/aat/
      </a>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to This Pattern</strong>
    </td>
    <td>
      Use at least one (1) E55_Type on each linguistic identifier specifying
      what it is (e.g., name, title, etc.).
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Projects Following This Pattern</strong>
    </td>
    <td>All projects</td>
  </tr>
</table>

**Pattern in TTL:**

```turtle
<entity> a crm:E1_CRM_Entity ; 
    rdfs:label "<entity>" ; 
    crm:P1_is_identified_by <entity_name_or_title> .

<entity_name_or_title> a crm:E33_E41_Linguistic_Appellation ; 
    rdfs:label "Linguistic identifier of <entity>" ; 
    crm:P2_has_type <type> ; 
    crm:P190_has_symbolic_content "<entity_name_or_title>" .

<type> a crm:E55_Type ; 
    rdfs:label "<type>" .
```

### Categories and Classifications

For more on types, categorization, and classification, as well as vocabularies, see the [Types section of the Basic Patterns Application Profile](/docs/explore-data/understand-lincs-data/application-profiles-main/basic-patterns#types).

![Application profile categories](/img/documentation/application-profile-physical-categories-(c-LINCS).png)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/ Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>This pattern declares that an object has a classification.</td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      <code>
        crm:E22_Human-Made_Object → crm:P2_has_type →{" "}
        <strong>crm:E55_Type</strong>
      </code>
    </td>
  </tr>
  <tr>
    <td rowspan="3">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>
      URI from project dataset, existing linked data authority, or minted by
      LINCS
    </td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>URI (preferably dereferenceable)</td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      <p>
        The University of Saskatchewan Art Gallery dataset states that there is
        an object with the types “work of art” and “painting.”
      </p>
      <p>
        <code>
          &lt;usask:3359> → crm:P2_has_type →{" "}
          <strong>&lt;aat:300133025>, &lt;aat:300033618></strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>
      The Getty Research Institute. (2017, March 7).{" "}
      <em>Art & Architecture Thesaurus Online</em>.{" "}
      <a href="https://www.getty.edu/research/tools/vocabularies/aat/">
        https://www.getty.edu/research/tools/vocabularies/aat/
      </a>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to This Pattern</strong>
    </td>
    <td>
      This pattern connects entities to vocabularies. For more on this, see the
      Vocabularies section of the Basic Patterns Application Profile.
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Projects Following This Pattern</strong>
    </td>
    <td> All projects</td>
  </tr>
</table>

**Pattern in TTL:**

```turtle
<object> a crm:E22_Human-Made_Object ;
    rdfs:label "<object>" ;
    crm:P2_has_type <type>.

<type> a crm:E55_Type ;
    rdfs:label "<type>".
```

### Production

A production date adds temporal information to the production event. For production date, see the [Time-Spans of Activities section of the Basic Patterns Application Profile](/docs/explore-data/understand-lincs-data/application-profiles-main/basic-patterns#time-spans-of-activities).

A production place adds location information to the production event. For production place, see the [Location of Activities section of the Basic Patterns Application Profile](/docs/explore-data/understand-lincs-data/application-profiles-main/basic-patterns#locations-of-activities).

For information about how to say that an actor was involved in or carried out an activity, see the [Roles section of the Basic Patterns Application Profile](/docs/explore-data/understand-lincs-data/application-profiles-main/basic-patterns#roles).

![Application profile production](/img/documentation/application-profile-physical-production-(c-LINCS).png)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>This pattern declares that an object was produced.</td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      <code>
        crm:E22_Human-Made_Object → crm:P108i_was_produced_by →{" "}
        <strong>crm:E12_Production</strong>
      </code>
    </td>
  </tr>
  <tr>
    <td rowspan="3">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>
      URI from project dataset, existing linked data authority, or minted by
      LINCS
    </td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>URI (preferably dereferenceable)</td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      <p>
        The University of Saskatchewan Art Gallery dataset states that there is
        an object that was produced through a production event.
      </p>
      <p>
        <code>
          &lt;usask:3359> → crm:P108i_was_produced_by →{" "}
          <strong>&lt;lincs:iGOuugzzrKB></strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to This Pattern</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Projects Following This Pattern</strong>
    </td>
    <td>
      Anthologia graeca, Canadian Centre for Ethnomusicology, University of
      Saskatchewan Art Gallery
    </td>
  </tr>
</table>

**Pattern in TTL:**

```turtle
<object> a crm:E22_Human-Made_Object ;
    rdfs:label "<object>" ; 
    crm:P108i_was_produced_by <production_event> .

<production_event> a crm:E12_Production ; 
    rdfs:label "Production event of <object>" .
```

### Physical Characteristics

Information about physical characteristics, such as materials and dimensions, are often entered as text statements instead of or in addition to as individual facets. This section therefore covers both.

#### Materials

For information on how to model a materials statement, see the section “Curatorial notes, descriptions, and other comments” above. Materials statements are identified by the pattern: `E33_Linguistic_Object → P2_has_type → <http://vocab.getty.edu/aat/300435429> .`

![Application profile materials](/img/documentation/application-profile-physical-materials-(c-LINCS).png)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>This pattern declares that an object is made of a type of material.</td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      <code>
        crm:E22_Human-Made_Thing → crm:P45_consists_of →{" "}
        <strong>crm:E57_Material</strong>
      </code>
    </td>
  </tr>
  <tr>
    <td rowspan="3">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>
      URI from project dataset, existing linked data authority, or minted by
      LINCS
    </td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>URI (preferably dereferenceable)</td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      <p>
        The University of Saskatchewan Art Gallery dataset states that there is
        an object that is made of oil paint.
      </p>
      <p>
        <code>
          &lt;usask:801> → crm:P45_consists_of →{" "}
          <strong>&lt;aat:300015050></strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>
      <p>
        The University of Saskatchewan Art Gallery dataset states that there is
        an object that is made of archival inkjet print. This compound material
        is combined through a LINCS material URI.
      </p>
      <p>
        <code>
          &lt;usask:4961> → crm:P45_consists_of →{" "}
          <strong>&lt;http://lincsproject.ca/HT4DabLEijO></strong>
          <br />→ crm:P2_has_type → <strong>&lt;aat:300218588></strong>
          <br />→ crm:P2_has_type → <strong>&lt;aat:300265621></strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>
      The Getty Research Institute. (2017, March 7).{" "}
      <em>Art & Architecture Thesaurus Online</em>.{" "}
      <a href="https://www.getty.edu/research/tools/vocabularies/aat/">
        https://www.getty.edu/research/tools/vocabularies/aat/
      </a>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to This Pattern</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Projects Following This Pattern</strong>
    </td>
    <td>
      Canadian Centre for Ethnomusicology, University of Saskatchewan Art
      Gallery
    </td>
  </tr>
</table>

**Pattern in TTL:**

```turtle
<object> a crm:E22_Human-Made_Object ;
    rdfs:label "<object>" ; 
    crm:P45_consists_of <medium>.

<medium> a crm:E57_Material ; 
    rdfs:label "<medium>" .
```

#### Dimensions

For information on how to model a dimensions statement, see the section “Curatorial notes, descriptions, and other comments” above. Dimensions statements are identified by the pattern: `E33_Linguistic_Object → P2_has_type → <http://vocab.getty.edu/aat/300435430> .`

![Application profile dimensions](/img/documentation/application-profile-physical-dimensions-(c-LINCS).png)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>
      This pattern declares that an object has one or more dimensions; an object
      can have a separate dimension for length, width, height, depth, and
      thickness.
    </td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      <p>
        <code>
          crm:E22_Human-Made_Object → crm:P43_has_dimension → crm:E54_Dimension
          <br />→ crm:P2_has_type → <strong>crm:E55_Type</strong>
          <br />→ crm:P90_has_value →<strong> rdfs:literal</strong>
          <br />→ crm:P91_has_unit → <strong>crm:E58_Measurement_Unit</strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td rowspan="3">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI); literal value (numeric)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>
      URI from project dataset, existing linked data authority, or minted by
      LINCS; literal value from project dataset
    </td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>URI (preferably dereferenceable); rdfs:literal</td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      <p>
        The University of Saskatchewan Art Gallery dataset states that there is
        an object with a width of 66 centimetres.
      </p>
      <p>
        <code>
          &lt;usask:3359> → crm:P43_has_dimension → crm:E54_Dimension
          <br />→ crm:P2_has_type → <strong>&lt;aat:300055647></strong>
          <br />→ crm:P90_has_value → <strong>“66”</strong>
          <br />→ crm:P91_has_unit → <strong>&lt;aat:300379098></strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>
      The Getty Research Institute. (2017, March 7).{" "}
      <em>Art & Architecture Thesaurus Online</em>.{" "}
      <a href="https://www.getty.edu/research/tools/vocabularies/aat/">
        https://www.getty.edu/research/tools/vocabularies/aat/
      </a>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to This Pattern</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Projects Following This Pattern</strong>
    </td>
    <td>
      Canadian Centre for Ethnomusicology, University of Saskatchewan Art
      Gallery
    </td>
  </tr>
</table>

**Pattern in TTL:**

```turtle
<object> a crm:E22_Human-Made_Object ;
    rdfs:label "<object>" ; 
    crm:P43_has_dimension <dimension> . 

<dimension> a crm:E54_Dimension ; 
    rdfs:label "<dimension_type> of <object>" ; 
    crm:P2_has_type <dimension_type> ; 
    crm:P90_has_value <rdfs:literal> ; 
    crm:P91_has_unit <measurement_unit> .

<dimension_type> a crm:E55_Type ; 
    rdfs:label "<dimension_type>" .

<measurement_unit> a crm:E58_Measurement_Unit ; 
    rdfs:label "<measurement_unit>" .
```

#### Number of Physical Parts

![Application profile number of parts](/img/documentation/application-profile-physical-numberofparts-(c-LINCS).png)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>
      This pattern declares that an object is made up of a number of parts.
    </td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      <code>
        crm:E71_Human-Made_Thing → crm:P57_has_number_of_parts →{" "}
        <strong>E60_Number</strong>
      </code>
    </td>
  </tr>
  <tr>
    <td rowspan="3">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>literal value (numeric)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>literal value from project dataset</td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>rdfs:literal</td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      <p>
        The Canadian Centre for Ethnomusicology dataset states that there is an
        object that has 2 parts.
      </p>
      <p>
        <code>
          &lt;cce:14-61829> → crm:P57_has_number_of_parts → <strong>“2”</strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to This Pattern</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Projects Following This Pattern</strong>
    </td>
    <td>Canadian Centre for Ethnomusicology</td>
  </tr>
</table>

**Pattern in TTL:**

```turtle
<object> a crm:E22_Human-Made_Object ;
    rdfs:label "<object>" ; 
    crm:P57_has_number_of_parts “<number>” .
```

#### Objects as Parts of Other Objects

![Application profile part of other objects](/img/documentation/application-profile-physical-partofother-(c-LINCS).png)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>This pattern declares that an object is part of another object.</td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      <code>
        crm:E22_Human_Made_Object → crm:P46i_forms_part_of →{" "}
        <strong>crm:E22_Human_Made_Object</strong>
      </code>
    </td>
  </tr>
  <tr>
    <td rowspan="3">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>
      URI from project dataset, existing linked data authority, or minted by
      LINCS
    </td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>URI (preferably dereferenceable)</td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      <p>
        The Canadian Centre for Ethnomusicology dataset states that there is an
        object which is part of another object.
      </p>
      <p>
        <code>
          &lt;cce:14-61682> → crm:P46i_forms_part_of →{" "}
          <strong>&lt;cce:14-61742></strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to This Pattern</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Projects Following This Pattern</strong>
    </td>
    <td>Canadian Centre for Ethnomusicology</td>
  </tr>
</table>

**Pattern in TTL:**

```turtle
<object1> a crm:E22_Human-Made_Object ;
    rdfs:label "<object1>" ; 
    crm:P46i_forms_part_of <object2> .

<object2> a crm:E22_Human-Made_Object ;
    rdfs:label "<object2>" .
```

#### Written and Ancient Texts (CRMtex)

##### Physical Features of Objects

![Application profile physical features](/img/documentation/application-profile-physical-physicalfeatures-(c-LINCS).png)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>This pattern declares that an object bears a feature.</td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      <p>
        <code>
          crm:E22_Human-Made_Object → crm:P56_bears_feature →
          crm:E25_Human-Made_Feature
          <br />→ crm:P56i_is_found_on → crm:E22_Human-Made_Object
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td rowspan="3">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>URI minted by LINCS</td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>URI (preferably dereferenceable)</td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      <p>
        The Anthologia graeca dataset declares that scholium 5.203.2 is found
        on Codex Palatinus 23 p. 117.
      </p>
      <p>
        <code>
          &lt;lincs:manuscript/4286> → crm:P56_bears_feature →
          <br />
          &lt;lincs:scholium/realization/2583>
          <br />→ crm:P56i_is_found_on → &lt;lincs:manuscript/4286>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to This Pattern</strong>
    </td>
    <td>
      In the Anthologia graeca dataset, this is modelled as E22 → P56 → TX7;
      TX7 is a subclass of E25 from the CRMtex extension.
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Projects Following This Pattern</strong>
    </td>
    <td>Anthologia graeca</td>
  </tr>
</table>

**Pattern in TTL:**

```turtle
<object> a crm:E22_Human-Made_Object ;
    rdfs:label "<object" ; 
    crm:P56_bears_feature <feature> .

<feature> a crm:E25_Human-Made Feature ;
    rdfs:label "<feature>" ; 
    crm:P56i_is_found_on <object> .
```

##### Segments of Written Texts

![Application profile segments of texts](/img/documentation/application-profile-physical-segmentsoftext-(c-LINCS).png)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>This pattern declares that a written text has segments.</td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      <p>
        <code>
          crmtex:TX1_Written_Text → crmtex:TXP4_has_segment →
          crmtex:TX7_Written_Text_Segment
          <br />→ crmtex:TXP4i_is_segment_of → crmtex:TX1_Written_Text
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td rowspan="3">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>URI minted by LINCS</td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>URI (preferably dereferenceable)</td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      <p>
        The Anthologia graeca dataset declares that passage 10.3 is a segment
        of Anthologia graeca book 10.
      </p>
      <p>
        <code>
          &lt;lincs:passage/realization/2849> → crmtex:TXP4i_is_segment_of →
          &lt;lincs:book/11>
          <br />→ crmtex:TXP4i_is_segment_of → &lt;lincs:passage/realization/2849>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to This Pattern</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Projects Following This Pattern</strong>
    </td>
    <td>Anthologia graeca</td>
  </tr>
</table>

**Pattern in TTL:**

```turtle
<written_text> a crmtex:TX1_Written_Text ;
    rdfs:label "<written_text>" ; 
    crmtex:TXP4_has_segment <text_segment> .

<text_segment> a crmtex:TX7_Written_Text_Segment ;
    rdfs:label "<text_segment>" ; 
    crmtex:TXP4i_is_segment_of <written_Text> .
```

### Provenance

Details about provenance, such as history of ownership, are often entered as text statements instead of or in addition to as individual facets. This section therefore covers both.

#### Provenance statement (e.g., Credit Line)

For information on how to model a provenance statement, see the section “Curatorial notes, descriptions, and other comments” above. Credit lines are identified by the pattern: `E33_Linguistic_Object → P2_has_type → <http://vocab.getty.edu/aat/300435418> .`

#### Owners

##### Current Owner

![Application profile current owner](/img/documentation/application-profile-physical-currentowner-(c-LINCS).png)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>This pattern declares that an object has a current owner.</td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      <code>
        crm:E22_Human-Made_Object → crm:P52_has_current_owner →{" "}
        <strong>crm:E39_Actor</strong>
      </code>
    </td>
  </tr>
  <tr>
    <td rowspan="3">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>
      URI from project dataset, existing linked data authority, or minted by
      LINCS
    </td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>URI (preferably dereferenceable)</td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      <p>
        The University of Saskatchewan Art Gallery dataset states that there is
        an object that is currently owned by the University of Saskatchewan.
      </p>
      <p>
        <code>
          &lt;usask:801> → crm:P52_has_current_owner →{" "}
          <strong>&lt;wikidata:Q1514848></strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>
      The Wikimedia Foundation. (2021). <em>Wikidata</em>.{" "}
      <a href="https://www.wikidata.org/">https://www.wikidata.org/</a>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to This Pattern</strong>
    </td>
    <td>
      crm:E39_Actor should be implemented through one of its subclasses:
      crm:E21_Person when it is one (1) person, and crm:E74_Group when it is
      more than one.
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Projects Following This Pattern</strong>
    </td>
    <td>
      Canadian Centre for Ethnomusicology, University of Saskatchewan Art
      Gallery
    </td>
  </tr>
</table>

**Pattern in TTL:**

```turtle
<object> a crm:E22_Human-Made_Object ;
    rdfs:label "<object>" ; 
    crm:P52_has_current_owner <current_owner> .

<current_owner> a crm:E39_Actor ;
    rdfs:label "<current_owner>" .
```

##### Former Owner

![Application profile former owner](/img/documentation/application-profile-physical-formerowner-(c-LINCS).png)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/ Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>This pattern declares that an object has a former owner.</td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      <code>
        crm:E22_Human-Made_Object → crm:P51_has_former_or_current_owner →{" "}
        <strong>crm:E39_Actor</strong>
      </code>
    </td>
  </tr>
  <tr>
    <td rowspan="3">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>
      URI from project dataset, existing linked data authority, or minted by
      LINCS
    </td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>URI (preferably dereferenceable)</td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      The University of Saskatchewan Art Gallery dataset states that there is an
      object that was previously owned by Norm Biram.
      <p>
        <code>
          &lt;usask:801> → crm:P51_has_former_or_current_owner →{" "}
          <strong>&lt;lincs:N2eZQC7kYJ7></strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to This Pattern</strong>
    </td>
    <td>
      crm:E39_Actor should be implemented through one of its subclasses:
      crm:E21_Person when it is one (1) person, and crm:E74_Group when it is
      more than one.
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Projects Following This Pattern</strong>
    </td>
    <td>
      Canadian Centre for Ethnomusicology, University of Saskatchewan Art
      Gallery
    </td>
  </tr>
</table>

**Pattern in TTL:**

```turtle
<object> a crm:E22_Human-Made_Object ;
    rdfs:label "<object>" ; 
    crm:P51_has_former_or_current_owner <former_owner> .

<former_owner> a crm:E39_Actor ;
    rdfs:label "<former_owner>" .
```

#### Acquisition

An acquisition date adds temporal information to the acquisition event. For acquisition date, see the [Time-Spans of Activities section of the Basic Patterns Application Profile](/docs/explore-data/understand-lincs-data/application-profiles-main/basic-patterns#time-spans-of-activities).

An acquisition place adds location information to the acquisition event. For acquisition place, see the [Location of Activities section of the Basic Patterns Application Profile](/docs/explore-data/understand-lincs-data/application-profiles-main/basic-patterns#locations-of-activities).

![Application profile acquisition](/img/documentation/application-profile-physical-acquisition-(c-LINCS).png)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>
      This pattern declares that an object changed ownership through an
      acquisition event.
    </td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      <code>
        crm:E22_Human-Made_Object → crm:P24i_changed_ownership_through →{" "}
        <strong>crm:E8_Acquisition</strong>
      </code>
    </td>
  </tr>
  <tr>
    <td rowspan="3">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>
      URI from project dataset, existing linked data authority, or minted by
      LINCS
    </td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>URI (preferably dereferenceable)</td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      <p>
        The University of Saskatchewan Art Gallery dataset states that there is
        an object that was acquired through an acquisition event.
      </p>
      <p>
        <code>
          &lt;usask:3359> → crm:P24i_changed_ownership_through →{" "}
          <strong>&lt;lincs:nVxmKOVmRHI></strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to This Pattern</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Projects Following This Pattern</strong>
    </td>
    <td>University of Saskatchewan Art Gallery</td>
  </tr>
</table>

**Pattern in TTL:**

```turtle
<object> a crm:E22_Human-Made_Object ;
    rdfs:label "<object>" ; 
    crm:P24i_changed_ownership_through <acquisition> . 

<acquisition> a crm:E8_Acquisition ; 
    rdfs:label "Acquisition of <object>" .
```

##### Relinquishing Ownership

![Application profile relinquishing ownership](/img/documentation/application-profile-physical-relinquishingownership-(c-LINCS).png)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>
      This pattern declares that an actor relinquished ownership of an object
      through an acquisition event.
    </td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      <p>
        <code>
          crm:E39_Actor → crm:P23i_surrendered_title_through →
          <br />
          <strong>crm:E8_Acquisition</strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td rowspan="3">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>
      URI from project dataset, existing linked data authority, or minted by
      LINCS
    </td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>URI (preferably dereferenceable)</td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      <p>
        The University of Saskatchewan Art Gallery dataset states that Norm
        Biram (lincs:sdKIgNCvFZl) surrendered an object through an acquisition
        event (lincs:GlmoPI8JhWS)
      </p>
      <p>
        <code>
          &lt;lincs:sdKIgNCvFZl> → crm:P23i_surrendered_title_through →{" "}
          <strong>&lt;lincs:GlmoPI8JhWS></strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to This Pattern</strong>
    </td>
    <td>
      crm:E39_Actor should be implemented through one of its subclasses:
      crm:E21_Person when it is one (1) person, and crm:E74_Group when it is
      more than one.
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Projects Following This Pattern</strong>
    </td>
    <td>University of Saskatchewan Art Gallery</td>
  </tr>
</table>

**Pattern in TTL:**

```turtle
<object> a crm:E22_Human-Made_Object ;
    rdfs:label "<object>" ; 
    crm:P24i_changed_ownership_through <acquisition> . 

<actor> a crm:E39_Actor ; 
    rdfs:label "<actor>" ; 
    crm:P23i_surrendered_title_through <acquisition> . 

<acquisition> a crm:E8_Acquisition ; 
    rdfs:label "Acquisition of <object>" .
```

##### Gaining Ownership

![Application profile gaining ownership](/img/documentation/application-profile-physical-gainingownership-(c-LINCS).png)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>
      This pattern declares that an actor gained ownership of an object through
      an acquisition event.
    </td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      <code>
        crm:E39_Actor → crm:P22i_aquired_title_through →{" "}
        <strong>crm:E8_Acquisition</strong>
      </code>
    </td>
  </tr>
  <tr>
    <td rowspan="3">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>
      URI from project dataset, existing linked data authority, or minted by
      LINCS
    </td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>URI (preferably dereferenceable)</td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      <p>
        The University of Saskatchewan Art Gallery dataset states that
        University of Saskatchewan gained ownership of an object through an
        acquisition event.
      </p>
      <p>
        <code>
          &lt;wikidata:Q1514848> → crm:P22i_aquired_title_through →{" "}
          <strong>&lt;lincs:GlmoPI8JhWS></strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>
      The Wikimedia Foundation. (2021). <em>Wikidata</em>.
      https://www.wikidata.org/
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to This Pattern</strong>
    </td>
    <td>
      crm:E39_Actor should be implemented through one of its subclasses:
      crm:E21_Person when it is one (1) person, and crm:E74_Group when it is
      more than one.
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Projects Following This Pattern</strong>
    </td>
    <td>University of Saskatchewan Art Gallery</td>
  </tr>
</table>

**Pattern in TTL:**

```turtle
<actor> a crm:E39_Actor ; 
    rdfs:label "<name>" ; 
    crm:P22i_aquired_title_through <Acquisition_Event> . 

<Acquisition_Event> a crm:E8_Acquisition ; 
    rdfs:label "Acquisition of LINCS object <ObjectIdentifier>" .
```

### Collections

#### Curator

![Application profile curator](/img/documentation/application-profile-physical-curator-(c-LINCS).png)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>
      This pattern declares that an actor has curatorial responsibility for a
      collection.
    </td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      <code>
        crm:E78_Curated_Holding → crm:P109_has_current_or_former_curator →
        **crm:E74_Group**
      </code>
    </td>
  </tr>
  <tr>
    <td rowspan="3">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI); literal value (text)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>
      URI from project dataset, existing linked data authority, or minted by
      LINCS; literal value from project dataset
    </td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>URI (preferably dereferenceable); rdfs:literal</td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      <p>
        The HistSex dataset states that the Special Collections and University
        Archives, University of Oregon Libraries is managed by the University of
        Oregon.
      </p>
      <p>
        <code>
          &lt;lincs:Special_Collections_and_University_Archives,_University_of_Oregon_Libraries>
          → crm:P109_has_current_or_former_curator →{" "}
          <strong>&lt;lincs:University_of_Oregon></strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to This Pattern</strong>
    </td>
    <td>
      crm:E39_Actor should be implemented through one of its subclasses:
      crm:E21_Person when it is one (1) person, and crm:E74_Group when it is
      more than one.
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Projects Following This Pattern</strong>
    </td>
    <td>HistSex</td>
  </tr>
</table>

**Pattern in TTL:**

```turtle
<collection> a crm:E78_Curated_Holding ; 
    rdfs:label "<collection>" ;
    crm:P109_has_current_or_former_curator <curator> .

<curator> a crm:E39_Actor ; 
    rdfs:label "<curator>" .
```
