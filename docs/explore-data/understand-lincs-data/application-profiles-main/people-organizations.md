---
sidebar_position: 4
title: "People & Organizations Application Profile"
description: "Represent information about persons and groups of persons"
---

## Purpose

To document how various facets of LINCS data are modelled, along with reference :Term[authorities]{#authority-file} for the populating :Term[vocabularies]{#vocabulary}. This will provide a basis for instruction for how to model data in a LINCS-compatible manner, as well as aid in navigation and discovery.

“People and Organizations” describes patterns that are unique or specific to representing information about persons and groups of persons.

This document introduces the concepts as used by LINCS, and are not complete definitions of the :Term[CIDOC CRM]{#cidoc-crm} ontology class or :Term[property]{#property} concepts. Consult [CIDOC CRM v. 7.1.1 documentation](https://cidoc-crm.org/Version/version-7.1.1) for full class descriptions and property descriptions.

## Acronyms

**Ontology Acronyms:**

* CRM - [CIDOC Conceptual Reference Model](http://cidoc-crm.org/)
* FRBRoo - [Object-Oriented Functional Requirements for Bibliographic Records](http://www.cidoc-crm.org/frbroo/home-0)

**Vocabulary and Authority Acronyms:**

* Biography - [The LINCS Biography Vocabulary](https://vocab.lincsproject.ca/Skosmos/biography/en/)
* Context - [The LINCS Context Vocabulary](https://vocab.lincsproject.ca/Skosmos/context/en/)
* Event - [The LINCS Event Vocabulary](https://vocab.lincsproject.ca/Skosmos/event/en/)
* Identity - [The LINCS Identity Vocabulary](https://vocab.lincsproject.ca/Skosmos/identity/en/)
* LINCS - LINCS minted entities
* Occupation - [The LINCS Occupation Vocabulary](https://vocab.lincsproject.ca/Skosmos/occupation/en/)
* Persrel - [The LINCS Personal Relations Vocabulary](https://vocab.lincsproject.ca/Skosmos/persrel/en/)
* Wikidata - [Wikimedia Knowledge Base](https://www.wikidata.org/wiki/Wikidata:Main_Page)

## Main Classes

<table>
  <tr>
    <td>
      <strong>Entity type</strong>
    </td>
    <td>
      <strong>Class</strong>
    </td>
    <td>
      <strong>Declaration Snippet (TTL)</strong>
    </td>
  </tr>
  <tr>
    <td>Actor</td>
    <td>crm:E39_Actor</td>
    <td>
      ```turtle
      <actor> a crm:E39_Actor ;
        rdfs:label "<actor_name>" .
      ```
    </td>
  </tr>
  <tr>
    <td>Person</td>
    <td>crm:E21_Person</td>
    <td>
      ```turtle
      <person> a crm:E21_Person ;
        rdfs:label "<person_name>" .
      ```
    </td>
  </tr>
  <tr>
    <td>Group</td>
    <td>crm:E74_Group</td>
    <td>
      ```turtle
      <group> a crm:E74_Group ;
        rdfs:label "<group_name>" .
      ```
    </td>
  </tr>
  <tr>
    <td>Name</td>
    <td>crm:E33_E41_Linguistic_Appellation</td>
    <td>
      ```turtle
      <name> a crm:E33_E41_Linguistic_Appellation ;
        rdfs:label "<name>".
      ```
    </td>
  </tr>
  <tr>
    <td>Contact points</td>
    <td>crm:E41_Appellation</td>
    <td>
      ```turtle
      <contact_point> a crm:E41_Appellation ;
        rdfs:label "<contact_point_descriptor>" .
      ```
    </td>
  </tr>
  <tr>
    <td>Activity</td>
    <td>crm:E7_Activity</td>
    <td>
      ```turtle
      <activity> a crm:E7_Activity ; 
        rdfs:label "<activity_descriptor>".
      ```
    </td>
  </tr>
  <tr>
    <td>Birth</td>
    <td>crm:E67_Birth</td>
    <td>
      ```turtle
      <birth_event> a crm:E67_Birth ;
        rdfs:label "Birth event of <actor_name>" .
      ```
    </td>
  </tr>
  <tr>
    <td>Death</td>
    <td>crm:E69_Death</td>
    <td>
      ```turtle
      <death_event> a crm:E69_Death ;
        rdfs:label "Death event of <actor_name>" .
      ```
    </td>
  </tr>
  <tr>
    <td>Occupation</td>
    <td>frbroo:F51_Pursuit</td>
    <td>
      ```turtle
        <occupation_activity> a frbroo:F51_Pursuit ;
          rdfs:label "<occupation_type> occupation of <person>" .
      ```
    </td>
  </tr>
  <tr>
    <td>Identities</td>
    <td>crm:E89_Propositional_Object, crm:E55_Type</td>
    <td>
      ```turtle
      <identity_term> a crm:E89_Propositional_Object, crm:E55_Type ;
        rdfs:label "<identity term label>" .
      ```
    </td>
  </tr>
  <tr>
    <td>Group Formation</td>
    <td>crm:E66_Formation</td>
    <td>
      ```turtle
      <group_formation> a crm:E66_Formation ; 
        rdfs:label "Formation of <group>" .
      ```
    </td>
  </tr>
</table>

## Overview Diagram

Below is an image of the application profile overview diagram. Follow this link for a [zoomable, more readable version](https://drive.google.com/file/d/1943OFEhmQ2QMV6xYKirCCWkDSThzpLhK/view?usp=sharing). The segments below align with the document sections.

![Application profile overview diagram.](/img/documentation/application-profile-people-overview-(c-LINCS).jpg)

## Nodes

### Actors

CIDOC CRM contains three classes for representing people or groups of people: E39_Actor, E21_Person, and E74_Group. E39_Actor, the superclass of the other two, should only be used in cases where it is not possible to state if the entity is one or more persons. This use should be extremely minimal. Otherwise, if the entity is a single person the class E21_Person should be used; if it is more than one person—such as an artist collective or organization like a business or a company—then E74_Group should be used.

#### Names

For more on identifiers, see the [Identifiers section of the Basic Patterns Application Profile](/docs/explore-data/understand-lincs-data/application-profiles-main/basic-patterns#identifiers).

For how names are made of up parts (such as a full name having a first and last name), see the [Identifiers section of the Basic Patterns Application Profile](/docs/explore-data/understand-lincs-data/application-profiles-main/basic-patterns#identifiers).

Different types of names are differentiated by the P2_has_type → E55_Type pattern associated with them. For more on this, see the [Types section of the Basic Patterns Application Profile](/docs/explore-data/understand-lincs-data/application-profiles-main/basic-patterns#types). For example:

* Personal name: `http://id.lincsproject.ca/biography/personalName`
* Preferred name: `http://id.lincsproject.ca/biography/preferredName`
* Pseudonym: `http://id.lincsproject.ca/biography/pseudonym`
* First name: `http://id.lincsproject.ca/biography/forename`
* Last name: `http://id.lincsproject.ca/biography/surname`

![Application profile names](/img/documentation/application-profile-people-names-(c-LINCS).png)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>This pattern declares that an actor is identified by a name.</td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      <p>
        <code>
          crm:E39_Actor → crm:P1_is_identified_by →
          crm:E33_E41_Linguistic_Appellation
          <br />→ crm:P2_has_type → <strong>crm:E55_Type</strong>
          <br />→ crm:P190_has_symbolic_content → <strong>rdfs:Literal</strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td rowspan="3">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI); literal value (text)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>
      URI from project dataset, existing linked data authority, or minted by
      LINCS; literal value from project dataset
    </td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>URI (preferably dereferenceable); rdfs:literal</td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      <p>
        The Yellow Nineties Personography states that Rosamund Marriott Watson
        was identified by the name Rosamund Thompson.
      </p>
      <p>
        <code>
          &lt;y90s:marriott-watson-rosamund/> → crm:P1_is_identified_by →
          crm:E33_E41_Linguistic_Appellation
          <br />→ crm:P2_has_type →
            **&lt;biography:additionalName>**
          <br />→ crm:P190_has_symbolic_content →{" "}
          <strong>“Thomson, Rosamund”</strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>
      Brown, S., Cummings, J., Drudge-Willson, J., Faulkner, C., Lemak, A.,
      Martin, K., Mo, A., Penancier, J., Simpson, J., Singh, G., Stacey, D., &
      Warren, R. (2020, July 14). <em>The CWRC Ontology Specification</em>{" "}
      0.99.86. The Canadian Writing Research Collaboratory.{" "}
      <a href="https://sparql.cwrc.ca/ontologies/cwrc.html#">
        https://sparql.cwrc.ca/ontologies/cwrc.html#
      </a>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to This Pattern</strong>
    </td>
    <td>
      Use at least one (1) E55_Type on each linguistic identifier specifying
      what it is (e.g., full name, first name, last name, etc.).
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Projects Following This Pattern</strong>
    </td>
    <td>
      AdArchive, Anthologia graeca, Canadian Centre for Ethnomusicology, MoEML
      Persons, Orlando Yellow Nineties Personography
    </td>
  </tr>
</table>

**Pattern in TTL:**

```turtle
<actor> a crm:E39_Actor ; 
    rdfs:label "<actor>" ; 
    crm:P1_is_identified_by <name>.

<name> a crm:E33_E41_Linguistic_Appellation ; 
    rdfs:label "<name>" ; 
    crm:P2_has_type <type> ;
    crm:P190_has_symbolic_content "<name>" .

<type> a crm:E55_Type ; 
    rdfs:label "<type" .
```

#### Contact Points

Different types of contact points are differentiated by the P2_has_type → E55_Type pattern associated with them. For more on this, see the [Types section of the Basic Patterns Application Profile](/docs/explore-data/understand-lincs-data/application-profiles-main/basic-patterns#types). For example:

* Address: `http://www.wikidata.org/entity/Q319608`
* Phone number: `http://www.wikidata.org/entity/Q214995`

![Application profile contact points](/img/documentation/application-profile-people-contactpoints-(c-LINCS).png)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>
      This pattern declares that an actor is associated with contact points
      (such as addresses and phone numbers).
    </td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      <p>
        <code>
          crm:E39_Actor → crm:P74_has_current_or_former_residence →
          crm:E53_Place
          <br />→ crm:P1_is_identified_by → crm:E41_Appellation
          <br />→ crm:P190_has_symbolic_content → <strong>rdfs:literal</strong>
          <br />→ crm:P2_has_type → <strong>crm:E55_Type</strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td rowspan="3">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI); literal value (text)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>
      URI from project dataset, existing linked data authority, or minted by
      LINCS; literal value from project dataset
    </td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>URI (preferably dereferenceable); rdfs:literal</td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      <p>
        The AdArchive dataset states that the Women’s Caucus for Art resided at
        a place identified by the address “WCA, 731 - 44th Avenue, San
        Francisco, CA 94121.”
      </p>
      <p>
        <code>
          &lt;wikidata:Q8030842> → crm:P74_has_current_or_former_residence →
          crm:E53_Place
          <br />→ crm:P1_is_identified_by → crm:E41_Appellation
          <br />→ crm:P190_has_symbolic_content →{" "}
          <strong>“WCA, 731 - 44th Avenue, San Francisco, CA 94121”</strong>
          <br />→ crm:P2_has_type → <strong>&lt;wikidata:Q319608></strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>
      <p>The address also serves as a direct contact point for the actor.</p>
      <p>
        <code>
          &lt;wikidata:Q8030842> → crm:P76_has_contact_point →
          crm:E41_Appellation
          <br />→ crm:P190_has_symbolic_content → **“WCA, 731 - 44th Avenue, San Francisco, CA 94121”**
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>
      The Wikimedia Foundation. (2021). <em>Wikidata</em>.{" "}
      <a href="https://www.wikidata.org/wiki/Wikidata:Main_Page">
        https://www.wikidata.org/
      </a>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to This Pattern</strong>
    </td>
    <td>
      Use at least one (1) E55_Type on each linguistic identifier specifying
      what it is (e.g., full name, first name, last name, etc).
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Projects Following This Pattern</strong>
    </td>
    <td>AdArchive</td>
  </tr>
</table>

**Pattern in TTL:**

```turtle
<actor> a crm:E39_Actor ;
    rdfs:label "<actor>" ;
    crm:P74_has_current_or_former_residence <residence> ;
    crm:P76_has_contact_point <contact> . 

<residence> a crm:E53_Place ; 
    rdfs:label "<residence>" ; 
    crm:P1_is_identified_by <contact> .

<contact> a crm:E41_Appellation ;
    rdfs:label "<contact>" ;
    crm:P2_has_type <type> ; 
    crm:P190_has_symbolic_content "<contact>" .

<type> a crm:E55_Type ; 
    rdfs:label "<type>" .

```

#### Activities

For information about how to say that an actor was involved in or carried out an activity, see the [Roles section of the Basic Patterns Application Profile](/docs/explore-data/understand-lincs-data/application-profiles-main/basic-patterns#roles).

### Persons

#### Birth

A birth date adds temporal information to the birth event. For birthdate, see the [Time-Spans of Activities section of the Basic Patterns Application Profile](/docs/explore-data/understand-lincs-data/application-profiles-main/basic-patterns#time-spans-of-activities).

A birth place adds location information to the birth event. For birthplace, see the [Location of Activities section of the Basic Patterns Application Profile](/docs/explore-data/understand-lincs-data/application-profiles-main/basic-patterns#locations-of-activities).

![Application profile birth](/img/documentation/application-profile-people-birth-(c-LINCS).png)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>This pattern declares that a person was born.</td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      E21_Person → crm:P98i_was_born → <strong>crm:E67_Birth</strong>
    </td>
  </tr>
  <tr>
    <td rowspan="3">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>
      URI from project dataset, existing linked data authority, or minted by
      LINCS
    </td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>URI (preferably dereferenceable)</td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      <p>The Orlando dataset states that Rosamund Marriort Watson was born.</p>
      <p>
        <code>
          cwrc_data:9e381429-5019-48dd-88fd-79e8b3f4825f → crm:P98i_was_born →{" "}
          <strong>lincs:kwgNbE4T0lz</strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>
      Brown, S., Cummings, J., Drudge-Wilson, J., Faulkner, C., Lemak, A.,
      Martin, K., Mo, A., Penancier, J., Simpson, J., Singh, G., Stacey, D., &
      Warren, R. (2020, July 14). <em>The CWRC Ontology Specification</em>{" "}
      0.99.86. The Canadian Writing Research Collaboratory.{" "}
      <a href="https://sparql.cwrc.ca/ontologies/cwrc.html#">
        https://sparql.cwrc.ca/ontologies/cwrc.html#
      </a>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to This Pattern</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Projects Following This Pattern</strong>
    </td>
    <td>
      Anthologia graeca, Canadian Centre for Ethnomusicology, MoEML Persons,
      Orlando, Yellow Nineties Personography
    </td>
  </tr>
</table>

**Pattern in TTL:**

```turtle
<person> a crm:E21_Person ; 
    rdfs:label "<person>" ;
    crm:P98i_was_born <birth> .

<birth> a crm:E67_Birth ; 
    rdfs:label "birth event of <person>" .
```

#### Death

A death date adds temporal information to the death event. For deathdate, see the [Time-Spans of Activities section of the Basic Patterns Application Profile](/docs/explore-data/understand-lincs-data/application-profiles-main/basic-patterns#time-spans-of-activities).

A death place adds location information to the death event. For deathplace, see the [Location of Activities section of the Basic Patterns Application Profile](/docs/explore-data/understand-lincs-data/application-profiles-main/basic-patterns#locations-of-activities).

![Application profile death](/img/documentation/application-profile-people-death-(c-LINCS).png)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>This pattern declares that a person died.</td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      crm:E21_Person → crm:P100i_died_in → <strong>crm:E69_Death</strong>
    </td>
  </tr>
  <tr>
    <td rowspan="3">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>
      URI from project dataset, existing linked data authority, or minted by
      LINCS
    </td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>URI (preferably dereferenceable)</td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      <p>The Orlando dataset states that Louisa May Alcott died.</p>
      <p>
        <code>
          data:Alcott_Louisa_May → crm:P100i_died_in →{" "}
          <strong>data:alcolo_DeathEvent_0</strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>
      Brown, S., Cummings, J., Drudge-Wilson, J., Faulkner, C., Lemak, A.,
      Martin, K., Mo, A., Penancier, J., Simpson, J., Singh, G., Stacey, D., &
      Warren, R. (2020, July 14). <em>The CWRC Ontology Specification</em>{" "}
      0.99.86. The Canadian Writing Research Collaboratory.{" "}
      <a href="https://sparql.cwrc.ca/ontologies/cwrc.html#">
        https://sparql.cwrc.ca/ontologies/cwrc.html#
      </a>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to This Pattern</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Projects Following This Pattern</strong>
    </td>
    <td>
      Anthologia graeca, Canadian Centre for Ethnomusicology, MoEML Persons,
      Orlando, Yellow Nineties Personography
    </td>
  </tr>
</table>

**Pattern in TTL:**

```turtle
<person> a crm:E21_Person ; 
    rdfs:label "<person>" ;
    crm:P100i_died_in <death> .

<death> a crm:E69_Death ; 
    rdfs:label "death event of <person>" .
```

##### Cause of Death

![Application profile cause of death](/img/documentation/application-profile-people-causeofdeath-(c-LINCS).png)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>
      This pattern declares that a person died because of or in connection to a
      relevant factor.
    </td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      <p>
        <code>
          crm:E21_Person → crm:P100i_died_in → crm:E69_Death
          <br />→ crm:P140i_was_attributed_by → crm:E13_Attribute_Assignment
          <br />→ crm:P141_assigned → <strong>crm:E55_Type</strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td rowspan="3">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>
      URI from project dataset, existing linked data authority, or minted by
      LINCS
    </td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>URI (preferably dereferenceable)</td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      <p>The Orlando dataset states that Jennifer Dawson died of cancer.</p>
      <p>
        <code>
          data:Dawson_Jennifer → crm:P100i_died_in → crm:E69_Death
          <br />→ crm:P140i_was_attributed_by → crm:E13_Attribute_Assignment
          <br />→ crm:P141_assigned → <strong>&lt;cwrc_ii:cancer></strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>
      Brown, S., Cummings, J., Drudge-Wilson, J., Faulkner, C., Lemak, A.,
      Martin, K., Mo, A., Penancier, J., Simpson, J., Singh, G., Stacey, D., &
      Warren, R. (2020, July 14). <em>The CWRC Ontology Specification</em>{" "}
      0.99.86. The Canadian Writing Research Collaboratory.{" "}
      <a href="https://sparql.cwrc.ca/ontologies/cwrc.html#">
        https://sparql.cwrc.ca/ontologies/cwrc.html#
      </a>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to This Pattern</strong>
    </td>
    <td>
      The attribute assignment pattern connected to the death event often
      references the CWRC Injury & Illness (ii:) Vocabulary.
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Projects Following This Pattern</strong>
    </td>
    <td>Orlando</td>
  </tr>
</table>

**Pattern in TTL:**

```turtle
<person> a crm:E21_Person ; 
    rdfs:label "<person>" ;
    crm:P100i_died_in <death> .

<death> a crm:E69_Death ; 
    rdfs:label "death event of <person>" ;
    crm:P140i_was_attributed_by <attribution> .

<attribution> a crm:E13_Attribute_Assignment ; 
    rdfs:label "cause of death event of <person>" ;
    crm:P141_assigned <cause> . 

<cause> a crm:E55_Type ; 
    rdfs:label "<cause>" .
```

#### Activities

##### Educational Activities

![Application profile educational activities](/img/documentation/application-profile-people-educationalactivities-(c-LINCS).png)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>
      This pattern declares that a person engaged in educational activities.
    </td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      <p>
        <code>
          crm:E21_Person → crm:P02i_is_range_of → crm:PC14_carried_out_by
          <br />
          (→ crm:P14.1_in_the_role_of → crm:E55_Type[occupation:student])
          <br />→ crm:P01_has_domain → crm:E7_Activity
          <br />
          (→ crm:P2_has_type → crm:E55_Type[event:EducationEvent])
          <br />→ crm:P14_carried_out_by → <strong>crm:E74_Group</strong> (→
          crm:P2_has_type → crm:E55_Type [biography:educationalOrganization]) →
          rdfs:label → <strong>rdfs:Literal</strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td rowspan="3">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI); literal value (text)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>
      URI from project dataset, existing linked data authority, or minted by
      LINCS; literal value from project dataset
    </td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>URI (preferably dereferenceable); rdfs:literal</td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      <p>
        The Yellow Nineties Personography states that Patrick Geddes was
        educated at the Royal School of Mines.
      </p>
      <p>
        <code>
          &lt;https://personography.1890s.ca/persons/geddes-patrick/> →
          crm:P02i_is_range_of → crm:PC14_carried_out_by
          <br />
          (→ crm:P14.1_in_the_role_of → &lt;occupation:student>)
          <br />→ crm:P01_has_domain → crm:E7_Activity
          <br />
          (→ crm:P2_has_type → &lt;event:EducationEvent>)
          <br />→ crm:P14_carried_out_by →{" "}
          <strong>&lt;lincs:KoE46wAZrNy></strong>
          <br />
          (→ crm:P2_has_type → &lt;biography:educationalOrganization>) →
          rdfs:label → <strong>“Royal School of Mines”</strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>
      <p>
        Brown, S., Cummings, J., Drudge-Wilson, J., Faulkner, C., Lemak, A.,
        Martin, K., Mo, A., Penancier, J., Simpson, J., Singh, G., Stacey, D., &
        Warren, R. (2020, July 14). <em>The CWRC Ontology Specification</em>{" "}
        0.99.86. The Canadian Writing Research Collaboratory.{" "}
        <a href="https://sparql.cwrc.ca/ontologies/cwrc.html#">
          https://sparql.cwrc.ca/ontologies/cwrc.html#
        </a>
      </p>
      <p>
        The Wikimedia Foundation. (2021). <em>Wikidata</em>.{" "}
        <a href="https://www.wikidata.org/wiki/Wikidata:Main_Page">
          https://www.wikidata.org/
        </a>
      </p>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to This Pattern</strong>
    </td>
    <td>
      When using E7_Activity and not a more specific subclass, use at least one
      (1) E55_Type on each activity specifying what it is.
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Projects Following This Pattern</strong>
    </td>
    <td>Orlando, Yellow Nineties Personography</td>
  </tr>
</table>

**Pattern in TTL:**

```turtle
<person> a crm:E21_Person ; 
    rdfs:label "<person>" ; 
    crm:P02i_is_range_of <reified_P14> .

<reified_P14> a crm:PC14_carried_out_by ;
    rdfs:label "<actor> in the role of <role>" ;
    crm:P01_has_domain <activity> ; 
    crm:P14.1_in_the_role_Of <role> .

<activity> a crm:E7:Activity ; 
    rdfs:label "<activity>" ;
    crm:P14_carried_out_by <school> .

<school> a crm:E74_Group ;
    rdfs:label "<school>" . 

<role> a crm:E55:Type ; 
    rdfs:label "<role>" .
```

##### Professional Activities and Occupations

![Application profile professional activities](/img/documentation/application-profile-people-professionalactivities-(c-LINCS).png)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>
      This pattern declares that a person engaged in professional occupations.
    </td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      <p>
        <code>
          crm:E21_Person → crm:P14i_performed → frbroo:F51_Pursuit
          <br />→ crm:P2_has_type → <strong>crm:E55_Type</strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td rowspan="3">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>
      URI from project dataset, existing linked data authority, or minted by
      LINCS
    </td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>URI (preferably dereferenceable)</td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      <p>
        The Yellow Nineties Personography states that Rosamund Marriott Watson
        held the occupation of “author.”
      </p>
      <p>
        <code>
          &lt;y90s:marriott-watson-rosamund/> → crm:P14i_performed →
          frbroo:F51_Pursuit
          <br />→ crm:P2_has_type → <strong>&lt;occupation:writer></strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>
      <p>
        The Yellow Nineties Personography states that Walter Crane held the
        apprenticeship of “pictoral engraver.”
      </p>
      <p>
        <code>
          &lt;Walter Crane> → crm:P02_is_range_of → crm:PC14_Carried_Out_By
          <br />
          (→ crm:P14.1_in_the_role_of →
          <br />
          <strong>&lt;wikidata:Q253567>[“Apprentice”]</strong>)
          <br />
          →crm:P01_has_domain → frbroo:F51_Pursuit
          <br />→ crm:P2_has_type →
            **&lt;y90s:pictorial-engraver>**
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>
      Brown, S., Cummings, J., Drudge-Wilson, J., Faulkner, C., Lemak, A.,
      Martin, K., Mo, A., Penancier, J., Simpson, J., Singh, G., Stacey, D., &
      Warren, R. (2020, July 14). <em>The CWRC Ontology Specification</em>{" "}
      0.99.86. The Canadian Writing Research Collaboratory.{" "}
      <a href="https://sparql.cwrc.ca/ontologies/cwrc.html#">
        https://sparql.cwrc.ca/ontologies/cwrc.html#
      </a>
      <p>
        The Wikimedia Foundation. (2021). <em>Wikidata</em>.{" "}
        <a href="https://www.wikidata.org/wiki/Wikidata:Main_Page">
          https://www.wikidata.org/
        </a>
      </p>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to This Pattern</strong>
    </td>
    <td>
      F51_Pursuit is a subclass of E7_Activity. Use F51_Pursuit when describing
      occupations related to the professional and creative activities of the
      person. Use E7_Activity to describe jobs held that are not necessarily
      related to the reason that the person is captured in the dataset. For
      example, Louisa May Alcott would have an E51_Pursuit as a writer, but been
      involved in the E7_Activity of nursing.
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Projects Following This Pattern</strong>
    </td>
    <td>Orlando, Yellow Nineties Personography</td>
  </tr>
</table>

**Pattern in TTL:**

```turtle
<person> a crm:E21_Person ; 
    rdfs:label "<person>" ; 
    crm:14i_performed <occupation>

<occupation> a frbroo:F51_Pursuit ;
    rdfs:label "<role> occupation of <person>” ; 
    crm:P2_has_type <role> .

<role> a crm:E55_Type ; 
    rdfs:label "<role>" .
```

#### Relationships

##### Parents & Children

###### Mother

![Application profile mother](/img/documentation/application-profile-people-mother-(c-LINCS).png)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>
      This pattern declares that a person is the mother of the person born in
      the birth event.
    </td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      <p>
        <code>
          E21_Person → crm:P98i_was_born → crm:E67_Birth
          <br />→ crm:P96_by_mother → <strong>E21_Person</strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td rowspan="3">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>
      URI from project dataset, existing linked data authority, or minted by
      LINCS
    </td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>URI (preferably dereferenceable)</td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      <p>
        The Orlando dataset states that Sylvia Ball is Rosamund Marriott
        Watson’s mother.
      </p>
      <p>
        <code>
          cwrc_data:9e381429-5019-48dd-88fd-79e8b3f4825f → crm:P98i_was_born →
          crm:E67_Birth → crm:P96_by_mother →{" "}
          <strong>cwrc_data:cfea70a8-022b-4f74-9cc9-e25b60e95e56</strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to This Pattern</strong>
    </td>
    <td>
      This is a full path for representing a child-parent relationship that can
      be shortcut using P152_has_parent if there is no desire to represent the
      birth event or specific motherhood.
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Projects Following This Pattern</strong>
    </td>
    <td>Orlando</td>
  </tr>
</table>

**Pattern in TTL:**

```turtle
<person> a crm:E21_Person ; 
    rdfs:label "<person>" ;
    crm:P98i_was_born <birth> .

<birth> a crm:E67_Birth ; 
    rdfs:label "birth event of <person>" ;
    crm:P96_by_mother <mother> .

<mother> a crm:E21_Person ; 
    rdfs:label "<mother>" .
```

###### Father

![Application profile father](/img/documentation/application-profile-people-father-(c-LINCS).png)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>
      This pattern declares that a person is the father of the person born in
      the birth event.
    </td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      <p>
        <code>
          E21_Person → crm:P98i_was_born → crm:E67_Birth
          <br />→ crm:P97_from_father → <strong>E21_Person</strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td rowspan="3">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>
      URI from project dataset, existing linked data authority, or minted by
      LINCS
    </td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>URI (preferably dereferenceable)</td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      <p>
        The Orlando dataset states that Benjamin Williams Ball is Rosamund
        Marriott Watson’s mother.
      </p>
      <p>
        <code>
          cwrc_data:9e381429-5019-48dd-88fd-79e8b3f4825f → crm:P98i_was_born →
          crm:E67_Birth → crm:P97_from_father →{" "}
          <strong>cwrc_data:9066fe2d-8bf3-4db6-a75c-59d5cd74fb0d</strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to This Pattern</strong>
    </td>
    <td>
      This is a full path for representing a child-parent relationship that can
      be shortcut using P152_has_parent if there is no desire to represent the
      birth event or specific fatherhood.
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Projects Following This Pattern</strong>
    </td>
    <td>Orlando</td>
  </tr>
</table>

**Pattern in TTL:**

```turtle
<person> a crm:E21_Person ; 
    rdfs:label "<person>" ;
    crm:P98i_was_born <birth> .

<birth> a crm:E67_Birth ; 
    rdfs:label "birth event of <person>" ;
    crm:P97_from_father <father> .

<father> a crm:E21_Person ; 
    rdfs:label "<father>" .
```

###### Parent-Child Shortcut

This pattern declares the parentage of a person using a parent-child relationship shortcut. Parentage can also be declared in relation to the birth event.

![Application profile parent-child shortcut](/img/documentation/application-profile-people-parentchildshortcut-(c-LINCS).png)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>This pattern declares that a person has or is a parent.</td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      <p>
        <code>
          crm:E21_Person1 → crm:P152_has_parent →{" "}
          <strong>crm:E21_Person2</strong>
          <br />→ crm:P152i_is_parent_of → <strong>crm:E21_Person1</strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td rowspan="3">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>
      URI from project dataset, existing linked data authority, or minted by
      LINCS
    </td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>URI (preferably dereferenceable)</td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      <p>
        The Yellow Nineties Personography declares that Walter Crane is the
        child of Thomas Crane , and inversely Thomas Crane is the parent of
        Walter Crane.
      </p>
      <p>
        <code>
          &lt;y90s:3154> → crm:P152_has_parent → <strong>&lt;y90s:3122></strong>
          <br />→ crm:152i_is_parent_of → <strong>&lt;y90s:3154></strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to This Pattern</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Projects Following This Pattern</strong>
    </td>
    <td>Yellow Nineties Personography</td>
  </tr>
</table>

**Pattern in TTL:**

```turtle
<Person> a crm:E21_Person ; 
    rdfs:label "<name>" ;
    crm:P152_has_parent <Person2> .

<Person2> a crm:E21_Person ;
    rdfs:label "<name>" .

<Person2> a crm:E21_Person ; 
    rdfs:label "<name>" ;
    crm:P152i_is_parent_of <Person> .

<Person> a crm:E21_Person ; 
    rdfs:label "<name>" .

```

##### Interpersonal Relationships

Relationships are structured as an activity that is performed by the participants in the relationship. This allows for spatiotemporal information to be included, such as the length of time for which the relationship held. This supports representation of different kinds of relationships that may evolve between people over a period of time.

![Application profile interpersonal relationships](/img/documentation/application-profile-people-interpersonalrelationships-(c-LINCS).png)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>
      This pattern declares that there existed a relationship between two
      persons.
    </td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      <p>
        <code>
          crm:E21_Person1 → crm:P14i_performed → crm:E7_Activity
          <br />→ crm:P2_has_type → <strong>crm:P55_Type</strong>
          <br />→ crm:P14_carried_out_by → crm:E21_Person2
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td rowspan="3">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>
      URI from project dataset, existing linked data authority, or minted by
      LINCS
    </td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>URI (preferably dereferenceable)</td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      <p>
        The Yellow Nineties Personography dataset declares that Rosamund
        Marriott Watson shared a friendship with William Sharp.
      </p>
      <p>
        <code>
          &lt;y90s:marriott-watson-rosamund/> → crm:P14i_performed →
          crm:E7_Activity
          <br />→ crm:P2_has_type → <strong>&lt;wikidata:Q491></strong>
          <br />→ crm:P14_carried_out_by → &lt;y90s:sharp-william/>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>
      <p>
        In some cases, each person’s contribution to the relationship activity
        may be qualified through roles.
      </p>
      <p>
        The Yellow Nineties Personography dataset declares that Rosamund
        Marriott Watson and Andrew Lang shared a mentorship relationship wherein
        Marriott Watson was the mentee and Lang was the mentor.
      </p>
      <p>
        <code>
          &lt;y90s:marriott-watson-rosamund/> → crm:P02i_is_range_of
          <br />→ crm:PC14_carried_out_by
          <br />→ crm:P14.1_in_the_role_of →{" "}
          <strong>&lt;persrel:mentor></strong>
          <br />→ crm:P01_has_domain → crm:E7_Activity
          <br /> → crm:P2_has_type →<strong> &lt;persrel:mentorship></strong>
          <br />→ crm:P01i_is_domain_of → crm:PC14_carried_out_by
          <br />→ crm:P14.1_in_the_role_of →{" "}
          <strong>&lt;persrel:mentee></strong>
          <br />→ crm:P02_has_range → &lt;y90s:lang-andrew/>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>
      Brown, S., Cummings, J., Drudge-Wilson, J., Faulkner, C., Lemak, A.,
      Martin, K., Mo, A., Penancier, J., Simpson, J., Singh, G., Stacey, D., &
      Warren, R. (2020, July 14). <em>The CWRC Ontology Specification</em>{" "}
      0.99.86. The Canadian Writing Research Collaboratory.{" "}
      <a href="https://sparql.cwrc.ca/ontologies/cwrc.html#">
        https://sparql.cwrc.ca/ontologies/cwrc.html#
      </a>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to This Pattern</strong>
    </td>
    <td>
      When using E7_Activity and not a more specific subclass, use at least one
      (1) E55_Type on each activity specifying what it is.
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Projects Following This Pattern</strong>
    </td>
    <td>Orlando, Yellow Nineties Personography</td>
  </tr>
</table>

**Pattern in TTL:**

```turtle
<person1> a crm:E21_Person ; 
    rdfs:label "<person1>" ; 
    crm:P14i_performed <relationship_activity> .

<relationship_activity> a crm:E7_Activity ;
    rdfs:label "<type> relationship between <person1> and <person2>" ;
    crm:P2_has_type <type> ; 
    crm:P14_carried_out_by <person2> .

<type> a crm:E55_Type ;
    rdfs:label "<type>"

<person2> a crm:E21_Person ; 
    rdfs:label "<person2>" .
```

##### Marriage

Married couples and other concepts of family are regarded as particular examples of E74 Group. For this pattern, see [Group Membership](#group-membership) below.

The Yellow Nineties Personography dataset does not follow this pattern, but instead uses the Relationships pattern (above) to represent marriages.

#### Identities

For broader context regarding assertions and the attribute assignment pattern, see the [Assertions and Attribution Assignment section of the Basic Patterns Application Profile](/docs/explore-data/understand-lincs-data/application-profiles-main/basic-patterns#assertions-and-attribution-assignment).

The Cultural Forms pattern, developed by the [Canadian Writing Research Collaboratory](https://cwrc.ca/) for the [Orlando Project](https://cwrc.ca/orlando) and other biographical data about persons, is for representing that statements about an actor’s identity have been made without implying anything further about the accuracy of those claims: this pattern is used to state that, for example, a biographer has described Louisa May Alcott as a woman, without stating wither Louisa May Alcott is a woman or not. This pattern recognizes that categorization is endemic to social experience and necessary to knowledge organization, but that social identities in particular are historically and geographically situated, frequently contested and shifting, constructed through discursive and social practices. It can be used to represent identities adopted by persons themselves, or identities attributed to them by others. Multiple attribute assignments can assist in representing intersectional identities. This pattern can be built upon to assert who made the attribution and when. See the [Assertions and Attribute Assignment section of the Basic Patterns Application Profile](/docs/explore-data/understand-lincs-data/application-profiles-main/basic-patterns#assertions-and-attribution-assignment).

![Application profile identities](/img/documentation/application-profile-people-identities-(c-LINCS).png)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>
      This pattern declares that a person has had an attribute about their
      identity associated with them.
    </td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      <p>
        <code>
          crm:E21_Person → crm:P140i_was_attributed_by →
          crm:E13_Attribute_Assignment
          <br />→ crm:P2_has_type → crm:E55_Type[context:CulturalFormContext]
          <br />→ crm:P141_assigned → crm:E7_Activity
          <br />→ crm:P2_has_type → crm:E55_Type[event:CulturalFormEvent]
          <br />→ crm:P16_used_specific_object →
          <br />
          <strong>
            crm:E89_Propositional_Object, crm:E55_Type [identity:CulturalForm]
          </strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td rowspan="3">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>
      URI from project dataset, existing linked data authority, or minted by
      LINCS; likely LINCS Identity vocabulary, Homosaurus, or other nuanced
      vocabulary for identities
    </td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>URI (preferably dereferenceable)</td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      <p>
        The Orlando dataset states that Rosamund Marriort Watson was attributed
        with having a gender of woman.
      </p>
      <p>
        <code>
          cwrc_data:9e381429-5019-48dd-88fd-79e8b3f4825f →
          crm:P140i_was_attributed_by → crm:E13_Attribute_Assignment
          <br />→ crm:P2_has_type → &lt;context:GenderContext>
          <br />→ crm:P141_assigned → crm:E7_Activity
          <br />→ crm:P2_has_type → &lt;event:GenderEvent>
          <br />→ crm:P16_used_specific_object →{" "}
          <strong>&lt;identity:woman></strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>
      <p>
        Billey, A., Cifor, M., Kronk, C., Noland, C., Rawson, K.J., van der Wel,
        J., Watson, B. M., & Colbert, J. L. (2020, December).{" "}
        <em>Homosaurus</em> v. 2.2. The Homosaurus Editorial Board.{" "}
        <a href="https://homosaurus.org/v2">https://homosaurus.org/v2</a>
      </p>
      <p>
        Brown, S., Cummings, J., Drudge-Wilson, J., Faulkner, C., Lemak, A.,
        Martin, K., Mo, A., Penancier, J., Simpson, J., Singh, G., Stacey, D., &
        Warren, R. (2020, July 14). “Preamble: Cultural Formations.”{" "}
        <em>The CWRC Ontology Specification</em> 0.99.86. The Canadian Writing
        Research Collaboratory.{" "}
        <a href="https://sparql.cwrc.ca/ontologies/cwrc-preamble-EN.html#culturalformationsontology">
          https://sparql.cwrc.ca/ontologies/cwrc-preamble-EN.html#culturalformationsontology
        </a>
      </p>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to This Pattern</strong>
    </td>
    <td>
      <p>
        Because of the origins of this concept in the CWRC Orlando project, the
        majority of cases will reference CWRC vocabularies for all three
        elements: Cultural Form Context, Cultural Form Event, and Cultural Form
        Label. There may be some use of other vocabularies such as Homosaurus
        for the Cultural Form Label, but the use of CWRC vocabularies to
        describe the E13_Attribute_Assignment and E7_Activitiy entities remains.
      </p>
      <p>
        Brown, S., “Categorically Provisional.” <em>PMLA</em>, vol. 135, no. 1,
        2020, pp. 165-174.
      </p>
      <p>
        Brown, S., Faulkner, C., Lemak, A., Martin, K., Warren, R. “Cultural
        (Re-)formations: Structuring a Linked Data Ontology for Intersectional
        Identities.” Digital Humanities 2017 Conference Abstracts.
        https://dh2017.adho.org/abstracts/580/580.pdf
      </p>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Projects Following This Pattern</strong>
    </td>
    <td>
      Canadian Centre for Ethnomusicology, Orlando, Yellow Nineties
      Personography
    </td>
  </tr>
</table>

**Pattern in TTL:**

```turtle
<person> a crm:E21_Person ; 
    rdfs:label "<person>" ;
    crm:P140i_was_attributed_by <identity_assignment> .

<identity_assignment> a crm:E13_Attribute_Assignment ; 
    rdfs:label "<person> - Cultural Form Context - Assigning" ;
    crm:P2_has_type <context:CulturalFormContext> ;        
    crm:P141_assigned <identity_assertion> .

<context:CulturalFormContext> a crm:E55_Type ;
    rdfs:label "cultural form context" .

<identity_assertion> a crm:E7_Activity ;
    rdfs:label "<person> - Cultural Form Label - Connecting" ; 
    crm:P2_has_type <event:CulturalFormEvent> ;  
    crm:P16_used_specific_object <identity:CulturalForm> .

<event:CulturalFormEvent> a crm:E55_Type ; 
    rdfs:label "cultural form event" .

<identity:CulturalForm> a crm:E89_Propositional_Object ;
    rdfs:label "cultural form label" .
```

### Groups

#### Group Membership

![Application profile group membership](/img/documentation/application-profile-people-groupmembership-(c-LINCS).png)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>This pattern declares that a person is a member of a group.</td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      <code>
        crm:E21_Person → crm:P107i_is_current_or_former_member_of →{" "}
        <strong>crm:E74_Group</strong>
      </code>
    </td>
  </tr>
  <tr>
    <td rowspan="3">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>
      URI from project dataset, existing linked data authority, or minted by
      LINCS
    </td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>URI (preferably dereferenceable)</td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      <p>
        The Yellow Nineties Personography states that Patrick Geddes is a member
        of the British Sociological Society.
      </p>
      <p>
        <code>
          &lt;y90s:geddes-patrick/> → crm:P107i_is_member_of →{" "}
          <strong>&lt;lincs:lfEWkpvhuK6></strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>
      Married couples and other concepts of family are regarded as particular
      examples of E74 Group.
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to This Pattern</strong>
    </td>
    <td>Use at least one (1) E55_Type on each group specifying what it is.</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Projects Following This Pattern</strong>
    </td>
    <td>Orlando, Yellow Nineties Personography</td>
  </tr>
</table>

**Pattern in TTL:**

```turtle
<person> a crm:E21_Person ; 
    rdfs:label "<person>" ; 
    crm:P107i_is_current_or_former_member_of <group> .

<group> a crm:E74_Group ;
    rdfs:label "<group>" .
```

#### Formation

For adding temporal information, such as the date of the group formation, see the [Time-Spans of Activities section of the Basic Patterns Application Profile](/docs/explore-data/understand-lincs-data/application-profiles-main/basic-patterns#time-spans-of-activities).

![Application profile formation](/img/documentation/application-profile-people-formation-(c-LINCS).png)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>This pattern declares that a group was formed.</td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      <code>
        crm:E74_Group → crm:P95i_was_formed_by →{" "}
        <strong>crm:E66_Formation</strong>
      </code>
    </td>
  </tr>
  <tr>
    <td rowspan="3">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>
      URI from project dataset, existing linked data authority, or minted by
      LINCS
    </td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>URI (preferably dereferenceable)</td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      <p>
        The AdArchive dataset states that the Women’s Caucus for Art
        (wikidata:Q8030842) is a group that was formed.
      </p>
      <p>
        <code>
          &lt;wikidata:Q8030842> → crm:P95i_was_formed_by →{" "}
          <strong>&lt;lincs:HKa2CAjHuYd></strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to This Pattern</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Projects Following This Pattern</strong>
    </td>
    <td>AdArchive</td>
  </tr>
</table>

**Pattern in TTL:**

```turtle
<group> a crm:E74_Group ;
    rdfs:label "<group>" ;
    crm:P95i_was_formed_by <group_formation> .

<group_formation> a crm:E66_Formation ;
    rdfs:label "Formation of <group>" .

```
