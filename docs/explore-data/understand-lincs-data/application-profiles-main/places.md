---
sidebar_position: 5
title: "Places Application Profile"
description: "Represent places as geographic and social concepts"
---

## Purpose

To document how various facets of LINCS data are modelled, along with reference :Term[authorities]{#authority-file} for the populating :Term[vocabularies]{#vocabulary}. This will provide a basis for instruction for how to model data in a LINCS-compatible manner, as well as aid in navigation and discovery.

“Places” describes patterns that are unique or specific to representing information about places as both geographic and social concepts.

This document introduces the concepts as used by LINCS, and are not complete definitions of the :Term[CIDOC CRM]{#cidoc-crm} ontology class or :Term[property]{#property} concepts. Consult [CIDOC CRM v. 7.1.1 documentation](https://cidoc-crm.org/Version/version-7.1.1) for full class descriptions and property descriptions.

## Acronyms

**Ontology Acronyms:**

* CRM - [CIDOC Conceptual Reference Model](http://cidoc-crm.org/)

**Vocabulary and Authority Acronyms:**

* EML - [Early Modern London Place Types Vocabulary](https://vocab.lincsproject.ca/Skosmos/eml)
* GeoJSON - [The GeoJSON Format](https://www.rfc-editor.org/rfc/rfc7946)
* LINCS - LINCS minted entities
* Wikidata - [Wikimedia Knowledge Base](https://www.wikidata.org/wiki/Wikidata:Main_Page)

## Main Classes

<table>
  <tr>
    <td>
      <strong>Entity type</strong>
    </td>
    <td>
      <strong>Class</strong>
    </td>
    <td>
      <strong>Declaration Snippet (TTL)</strong>
    </td>
  </tr>
  <tr>
    <td>Place</td>
    <td>crm:E53_Place</td>
    <td>
      ```turtle
      <place> a crm:E53_Place ;
        rdfs:label "<place>" .
      ```
    </td>
  </tr>
  <tr>
    <td>Placename</td>
    <td>crm:E33_E41_Linguistic_Appellation</td>
    <td>
      ```turtle
      <place_name> a crm:E33_E41_Linguistic_Appellation ;
        rdfs:label "<place_name>" .
      ```
    </td>
  </tr>
  <tr>
    <td>Address</td>
    <td>crm:E41_Appellation</td>
    <td>
      ```turtle
      <address> a crm:E41_Appellation ;
        rdfs:label "<address_descriptor>" .
      ```
    </td>
  </tr>
  <tr>
    <td>Coordinates</td>
    <td>rdfs:literal, e.g., GeoJSON string</td>
    <td>
      ```turtle
      "<coordinates>" .
      ```
    </td>
  </tr>
</table>

## Overview Diagram

Below is an image of the application profile overview diagram. Follow this link for a [zoomable, more readable version](https://drive.google.com/file/d/1hwxpQLHG_yjw7egKNG-q_JfXBCyHJymk/view?usp=sharing). The segments below align with the document sections.

![Application profile overview diagram.](/img/documentation/application-profile-places-overview-(c-LINCS).jpg)

## Nodes

### Place Names

For more on identifiers, see the [Identifiers section of the Basic Patterns Application Profile](/docs/explore-data/understand-lincs-data/application-profiles-main/basic-patterns#identifiers).

For how names are made of up parts (such as a full name having a first and last name), see the [Identifiers section of the Basic Patterns Application Profile](/docs/explore-data/understand-lincs-data/application-profiles-main/basic-patterns#identifiers).

Different types of names are differentiated by the P2_has_type → E55_Type pattern associated with them (for more on this, see the [Types section of the Basic Patterns Application Profile](/docs/explore-data/understand-lincs-data/application-profiles-main/basic-patterns#types)). For example:

> Preferred name: `http://vocab.getty.edu/aat/300404670`

![Application profile names](/img/documentation/application-profile-places-names-(c-LINCS).png)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>This pattern declares that a place is identified by a name.</td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      <p>
        <code>
          crm:E53_Place → crm:P1_is_identified_by →
          crm:E33_E41_Linguistic_Appellation
          <br />→ crm:P2_has_type → <strong>crm:E55_Type</strong>
          <br />→ crm:P190_has_symbolic_content → <strong>rdfs:Literal</strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td rowspan="3">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI); literal value (text)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>
      URI from project dataset, existing linked data authority, or minted by
      LINCS; literal value from project dataset
    </td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>URI (preferably dereferenceable); rdfs:literal</td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      <p>
        The Map of Early Modern London Gazetteer states that St. Saviour
        (Southwark) is named “St. Saviour (Southwark)” which is in English.
      </p>
      <p>
        <code>
          &lt;moeml:STSA1> → crm:P1_is_identified_by →
          &lt;moeml:St._Saviour_(Southwark)>
          <br />→ crm:P2_has_type → <strong>&lt;lincs:name></strong>
          <br />→ crm:P72_has_language → <strong>&lt;lincs:english></strong>
          <br />→ crm:P190_has_symbolic_content → **“St. Saviour (Southwark)”**
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to This Pattern</strong>
    </td>
    <td>
      Use at least one (1) E55_Type on each linguistic identifier specifying
      what it is.
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Projects Following This Pattern</strong>
    </td>
    <td>AdArchive, MoEML Gazetteer</td>
  </tr>
</table>

**Pattern in TTL:**

```turtle
<place> a crm:E53_Place ; 
    rdfs:label "<place>" ; 
    crm:P1_is_identified_by <name>.

<name> a crm:E33_E41_Linguistic_Appellation ; 
    rdfs:label "<name>" ; 
    crm:P2_has_type <type> ;
    crm:P190_has_symbolic_content "<name>" .

<type> a crm:E55_Type ; 
    rdfs:label "<type" .
```

## Uses/Functions (Types)

For more on types, categorization, and classification, as well as vocabularies, see the [Types section of the Basic Patterns Application Profile](/docs/explore-data/understand-lincs-data/application-profiles-main/basic-patterns#types).

![Application profile type](/img/documentation/application-profile-places-type-(c-LINCS).png)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>
      This pattern declares that a place has a classification, including by use
      or function.
    </td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      crm:E53_Place → crm:P2_has_type → <strong>crm:E55_Type</strong>
    </td>
  </tr>
  <tr>
    <td rowspan="3">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>
      URI from project dataset, existing linked data authority, or minted by
      LINCS
    </td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>URI (preferably dereferenceable)</td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      <p>
        The Map of Early Modern London Gazetteer states that St. Saviour
        (Southwark) is a type of eml:Church.
      </p>
      <p>
        <code>
          &lt;moeml:STSA1> → crm:P2_has_type → <strong>&lt;eml:Church></strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>
      MoEML Team and Martin D. Holmes. (2022). “Locations in Early Modern
      London.” <em>The Map of Early Modern London, Edition 7.0</em>. Ed. Janelle
      Jenstad. University of Victoria.{" "}
      <a href="https://mapoflondon.uvic.ca/edition/7.0/mdtEncyclopediaLocation_subcategories.htm">
        https://mapoflondon.uvic.ca/edition/7.0/mdtEncyclopediaLocation_subcategories.htm
      </a>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to This Pattern</strong>
    </td>
    <td>
      This pattern connects entities to vocabularies. For more on this, see the
      Vocabularies section of the Basic Patterns Application Profile.
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Projects Following This Pattern</strong>
    </td>
    <td>AdArchive, MoEML Gazetteer, Orlando</td>
  </tr>
</table>

**Pattern in TTL:**

```turtle
<place> a crm:E53_Place ;
    rdfs:label "<place>" ;
    crm:P2_has_type <type>.

<type> a crm:E55_Type ;
    rdfs:label "<type>".
```

## Address

![Application profile address](/img/documentation/application-profile-places-address-(c-LINCS).png)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>This pattern declares that a place was identified by an address.</td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      <p>
        <code>
          crm:E53_Place → crm:P1_is_identified_by → crm:E41_Appellation
          <br />→ crm:P190_has_symbolic_content → <strong>rdfs:literal</strong>
          <br />→ crm:P2_has_type → <strong>crm:E55_Type</strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td rowspan="3">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI); literal value (text)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>
      URI from project dataset, existing linked data authority, or minted by
      LINCS; literal value from project dataset
    </td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>URI (preferably dereferenceable); rdfs:literal</td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      <p>
        The AdArchive dataset states that there is a place identified by the
        address “WCA, 731 - 44th Avenue, San Francisco, CA 94121.”
      </p>
      <p>
        <code>
          &lt;lincs:xijMbv6moWq> → crm:P1_is_identified_by → crm:E41_Appellation
          <br />→ crm:P190_has_symbolic_content → **“WCA, 731 - 44th \ Avenue, San Francisco, CA 94121”**
          <br />→ crm:P2_has_type → <strong>&lt;wikidata:Q319608></strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>
      The Wikimedia Foundation. (2021). <em>Wikidata</em>.{" "}
      <a href="https://www.wikidata.org/wiki/Wikidata:Main_Page">
        https://www.wikidata.org/
      </a>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to This Pattern</strong>
    </td>
    <td>
      Use at least one (1) E55_Type on each linguistic identifier specifying
      what it is (e.g., full name, first name, last name, etc).
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Projects Following This Pattern</strong>
    </td>
    <td>AdArchive</td>
  </tr>
</table>

**Pattern in TTL:**

```turtle
<place> a crm:E53_Place ; 
    rdfs:label "<place>" ; 
    crm:P1_is_identified_by <address> .

<address> a crm:E41_Appellation ;
    rdfs:label "<address>" ;
    crm:P2_has_type <type> ; 
    crm:P190_has_symbolic_content "<address>" .

<type> a crm:E55_Type ; 
    rdfs:label "<type>" . 
```

## Geographies and Coordinates

![Application profile define by](/img/documentation/application-profile-places-definedby-(c-LINCS).png)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>
      This pattern declares that a place is identified by a geographic
      reference.
    </td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      <code>
        crm:E53_Place → crm:P168_place_is_defined_by →{" "}
        <strong>rdfs:literal</strong>
      </code>
    </td>
  </tr>
  <tr>
    <td rowspan="3">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>literal value (text)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>literal value from project dataset</td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>rdfs:literal</td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      <p>
        The AdArchive dataset states that there is a place located at the
        coordinates “-122.505020, 37.774750.”
      </p>
      <p>
        <code>
          &lt;wikidata:Q8030842> → crm:P74_has_current_or_former_residence →
          crm:E53_Place
          <br />→ crm:P168_place_is_defined_by → **“-122.505020, 37.774750”**
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>
      The Map of Early Modern London Gazetteer states that St. Saviour
      (Southwark) is located at the coordinates “-0.089722,51.506111”.
      <p>
        <code>
          &lt;moeml:STSA1> →
          <br />→ crm:P168_place_is_defined_by → **“POINT(51.506111 -0.089722)”**
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>
      <p>
        H. Butler et. al. (2016). <em>RFC 7946: The GeoJSON Format</em>.{" "}
        <a href="https://www.rfc-editor.org/rfc/rfc7946">
          https://www.rfc-editor.org/rfc/rfc7946
        </a>
      </p>
      <p>
        The Wikimedia Foundation. (2021). <em>Wikidata</em>.{" "}
        <a href="https://www.wikidata.org/wiki/Wikidata:Main_Page">
          https://www.wikidata.org/
        </a>
      </p>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to This Pattern</strong>
    </td>
    <td>
      <p>
        <a href="https://geojson.org/">GeoJSON</a> can use 6 types of
        coordinates: Point, MultiPoint, LineString, Polygon, MultiPolygon,
        MultiLineString.
      </p>
      <p>This dataset uses all of these; most commonly used is “point.”</p>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Projects Following This Pattern</strong>
    </td>
    <td>AdArchive, Anthologia graeca, MoEML Gazetteer</td>
  </tr>
</table>

**Pattern in TTL:**

```turtle
<place> a crm:E53_Place ; 
    rdfs:label "<place>" ; 
    crm:P168_place_is_defined_by "<coordinates>" .
```

## Places as Parts of Other Places

![Application profile part of other place](/img/documentation/application-profile-places-partofother-(c-LINCS).png)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>
      This pattern declares that a place is geographically within a larger
      space.
    </td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      <code>
        crm:E53_Place → crm:P89_falls_within → <strong>crm:E53_Place</strong>
      </code>
    </td>
  </tr>
  <tr>
    <td rowspan="3">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI); literal value (text)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>
      URI from project dataset, existing linked data authority, or minted by
      LINCS; literal value from project dataset
    </td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>URI (preferably dereferenceable); rdfs:literal</td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      <p>
        The Yellow Nineties Personography declares that the place that Rosamund
        Marriott Watson was born falls within the London Borough of Hackney.
      </p>
      <p>
        <code>
          &lt;lincs:1i8nVqCadyT> → crm:P89_falls_within →{" "}
          <strong>&lt;wikidata:Q205679></strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>
      The Wikimedia Foundation. (2019, December 30). Wikidata: Main Page.
      Wikidata.{" "}
      <a href="https://www.wikidata.org/wiki/Wikidata:Main_Page">
        https://www.wikidata.org/wiki/Wikidata:Main_Page
      </a>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to the Pattern</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Projects Following This Pattern</strong>
    </td>
    <td>Yellow Nineties Personography</td>
  </tr>
</table>
