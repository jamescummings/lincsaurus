---
title: "ResearchSpace (Review) Documentation"
sidebar_position: 3
---



<div className="primary-button-row">
  <PrimaryButton
    link="https://rs-review.lincsproject.ca/"
    buttonName="To ResearchSpace (Review)"
    />
</div>

## Overview

LINCS maintains **ResearchSpace (Review)** as a distinct ResearchSpace environment for researchers with data in LINCS.  

**ResearchSpace (Review)** gives you the same features as the ResearchSpace environment with some additional functionality:

- View and [edit your own pre-published data](#edit-an-entity)
- Edit the information on the [dataset project page](#edit-your-dataset-project-page)

### Prerequisites
- Create an account in [Researchspace (Review)](http://rs-review.lincsproject.ca)
- [Contact LINCS](/docs/about-lincs/get-involved/contact-us) to request or confirm dataset editing permissions.

Consult the documentation for ResearchSpace to learn how to:

- [Create an account](/docs/tools/researchspace/researchspace-documentation#create-an-account)
- [Browse LINCS data](/docs/tools/researchspace/researchspace-documentation#browse-lincs-data)
- [Search LINCS data](/docs/tools/researchspace/researchspace-documentation#search-lincs-data)
- [SPARQL Search](/docs/tools/researchspace/researchspace-documentation#sparql-search)
- [Create a Knowledge Map](/docs/tools/researchspace/researchspace-documentation#create-a-knowledge-map)
- [Create a Semantic Narrative](/docs/tools/researchspace/researchspace-documentation#create-a-semantic-narrative)

:::note

Knowledge Maps and Semantic Narratives should be created in ResearchSpace, _**not**_  in ResearchSpace (Review). In the Review environment, you won't be able to share your work. If your Knowledge Map contains unpublished data, the data may change, affecting the work.

:::

## Edit an Entity

You can add, change, or delete properties from entities in your dataset with the **entity editing form**. 

1. Search or Browse ResearchSpace to find an entity you wish to edit.
2. Look for the **pencil icon/ edit entity link** on the entity summary page or entity card.

<img src="/img/documentation/researchspace-entity-summary-edit-properties-(c-LINCS).png" title="edit entity link on entity summary page" alt="" height="" width="100%"/>

_Location of the edit entity link on an entity summary page._

<img src="/img/documentation/researchspace-entity-card-edit-properties-(c-LINCS).png" title="edit entity link on entity card" alt="" height="100%" width=""/>

_Location of the edit entity link on an entity card._

3. If permissions are set correctly, you should see an **entity editing form**. If you receive an error message, [contact LINCS](/docs/about-lincs/get-involved/contact-us). Each input field on the form is mapped to a permitted property (i.e. predicate) and value type (i.e., IRI, string, number, date, etc.).
To change an existing string or numeric value, edit the existing value directly in the field. When adding a new IRI value, the full-text lookup allows you to type inside the input box, revealing a list of entity values from existing IRIs within ResearchSpace. It is always preferable to reuse an existing IRI from within ResearchSpace to enhance shared connections within and across datasets in ResearchSpace.

![search-within](/img/documentation/researchspace-entity-editing-form-edit-field-(c-LINCS).png)
_Edit a value directly in the field._

An existing IRI value cannot be changed. It can only be deleted using the “x” to the right of the field. 
![search-within](/img/documentation/researchspace-entity-editing-form-delete-field-(c-LINCS).png)
_Remove an IRI._

To add a new value, click **+Add [label]**. A new field appears where you can enter a new value.

![search-within](/img/documentation/researchspace-entity-editing-form-add-new-value-(c-LINCS).png)
_Find and add a new value._

If you can’t find the property you wish to edit for a given entity, you may need to look for that property on one of the connected entities.

If you can't find a value that suits your needs, then you can create a new entity using the **Create new** button to the right of the input field. Creating a new value will open an overlay where you can create an entirely new entity. It will automatically connect to the primary entity you are editing.

![search-within](/img/documentation/researchspace-entity-editing-form-create-new-(c-LINCS).png)
_Create a new entity to attach to the entity you are editing._

4. Click **Save** at the bottom of the form. Changes will be saved to the :Term[triplestore]{#triplestore}.

## Reconcile an Entity

Use the **Same As** field in the entity editing form to reconcile an entity. You can enter a LINCS URI or a URI from an external authority like Wikidata or Getty.

## Delete an Entity

1. Follow the steps under Edit an Entity to access the entity editing form for the entity you wish to delete. 
2. Click **Delete** at the bottom of the form. This removes the entity from the :Term[triplestore]{#triplestore}

## Create a New Entity

In some cases, a new entity cannot be created and attached through an overlay form. You will need to create the new entity on a separate form. This is an advanced task that requires you to understand clearly what kind of entity you want to create and for what purpose.

Option 1: If you want to attach a new entity to an existing entity as an object, click **Create New** uder an input field on an entity you are currently editing. 

![search-within](/img/documentation/researchspace-entity-editing-create-new-entity-(c-LINCS).png)
_Open a new entity editing form to create an entity from scratch._

Option 2: If you want to add a new entity with the purpose of having it stand on its own as a completely new thing (e.g., adding a new work, person, or place to your dataset), open a new form from the homepage. Click the link **Create/Edit Entities** in the **Manage Data** box. 

![search-within](/img/documentation/researchspace-entity-editing-create-new-entity-from-homepage-(c-LINCS).png)
_Open a new entity editing form from the homepage to create a new entity from scratch._

For either option, follow these steps:

1. Select an entity type on the **Entity Editor** form. If you do not know the type of entity you want to create, [Contact LINCS](/docs/about-lincs/get-involved/contact-us).
It is currently not possible to assign more than one entity type to a new entity through the ResearchSpace editing interface.

![search-within](/img/documentation/researchspace-entity-editor-form-select-entity-type-(c-LINCS).png)

_Select an entity type._

2. A unique aspect of editing a new entity is the ability to choose the graph you are saving in. If you have permission to edit more than one graph, you will see them at the top of the editing form. 
3. After selecting a new entity type, you have the option to **Use External Entity** i.e., search for an existing entity from an external source such as Wikidata or Getty. Alternatively, you can select **Create New Entity** to mint a new IRI.

![search-within](/img/documentation/researchspace-create-new-entity-use-external-or-mint-new-(c-LINCS).png)
_Search for an entity from an external source or mint a new one._

4. If using an external entity, the IRI and a label will be generated, you can proceed to add more information to the entity using the entity editing form. If you are minting a new IRI, enter values into the entity editing form.  This process is the same as [editing an existing entity](#edit-an-entity) is the same.
5. If you selected Option 1 (attach a new entity to an existing entity), you can now add the entity you just created to the entity you were editing previously. To do this, search for the new entity in the input field where you intend to attach the newly created entity.

## Edit Your Dataset Project Page

1. Click **Edit Dataset Information** on the homepage.
2. Click **Edit** (pencil icon) on your dataset card.
3. Cick the **Use** button where it says **Metadata (Project/Dataset) Form**.
4. Edit the Metadata form as needed. (See details about the form below).
5. Click **Save**.

### Dataset Project Page Metadata Form 

These are the fields on the metadata form. Hover over the **?** icons on the form for explanations about each field. Fields marked with an asterisk (*) are required.

- **Named Graph** : [Contact LINCS](/docs/about-lincs/get-involved/contact-us) before editing this field.
- **Title**: [Contact LINCS](/docs/about-lincs/get-involved/contact-us) before editing this field.
- **Short Title**: [Contact LINCS](/docs/about-lincs/get-involved/contact-us) before editing this field. The Short Title is used to label tabs in ResearchSpace.
- **Description**: Use this field to briefly describe your dataset. Example: "The Orlando Project's collaborative experiment in feminist literary history started in 1995, published its flagship resource, Orlando: Women’s Writing in the British Isles, in 2006 with Cambridge University Press, and has produced an accompanying linked dataset for LINCS based on the semantic tagging in its 8+ million words of original scholarship about women writers’ lives, works, and cultures."
- **Short Description**: Use this field to describe your dataset in 1-2 sentences. Example: "The Orlando Project explores and harnesses the power of semantic tools and methods to advance feminist literary scholarship. View Orlando as LOD!"
- **Creator**: Add as many project creators as desired. Your can add, remove, or edit the list of creators. Use the **Edit** button to add details about each contributor. These details include:
  - **Type**: Person or Group/Organization
  - **Full Name**: Name Lastname
  - **Same As**: Use this field for personal identifiers like [ORCID](https://orcid.org/) or [VIAF](https://viaf.org/).
  - **Role Type**: Options include: Researcher, Literary Director, Director, Associate Director, Research Director, Founding Director, Technical Director, Contributor, Registrar
  - **Email Address**: Enter a valid email address.
  - **Contributor**: Add as many additional contributors as desired. You can add, remove, or edit the list of contributors. Contributors and creators are not distinguishable on the dataset landing page, so you can simply the use the Creator field for all project creators and contributors.
- **Status**: The status of your data may be draft, pre-published, or published. [Contact LINCS](/docs/about-lincs/get-involved/contact-us) before editing this field.
- **Language**: Use this field to add as many languages as are reflected in your dataset.
- **Source Dataset**: This field must be a URL.
- **Image**: [Contact LINCS](/docs/about-lincs/get-involved/contact-us) before editing this field.
- **Logo (Or Cover Image)**: [Contact LINCS](/docs/about-lincs/get-involved/contact-us) before editing this field.
- **Dataset Landing Page Carousel Images**: Your carousel includes three entities from your dataset. If you leave this field blank, ResearchSpace will randomly select three entities from your dataset. To choose the entities which will appear in the carousel, click **+Add dataset landing page carousel images**. If you know the IRI for the image enter it in the field. 
  You can find the IRI on the entity's [Entity Summary Page](/docs/tools/researchspace/researchspace-documentation#understand-the-entity-summary-page). 

Under the Outgoing Statements/Entity as Subject tab, locate the **has representation** property and click the associated **value** link.

  ![search-within](/img/documentation/researchspace-has-representation-(c-LINCS).png)
  _Location of the **Has representation** property link on the entity summary page._
  
  Click the **copy** icon next to the Resource Identifier. A confirmation message will appear: "The IRI has been copied to clipboard!"

![search-within](/img/documentation/researchspace-copy-IRI-(c-LINCS).png)
_Location of the Resource Identifier link._

  Paste the IRI into the field on the metadata form.

   ![search-within](/img/documentation/researchspace-paste-IRI-(c-LINCS).png)
   _Paste the Resource Identifier link into this field on the dataset project metadata form._

- **Identifier**: Use this field to add an affiliated website, project search page, dataset source URL, archived dataset URL, or Converted LOD Source URL.
- **Subject/ Topic**: Use this field to add IRIs representing the subject(s) of your dataset:
  - Click **+Add subject/topic** to search for an existing subject or topic. 
  - Scroll through the list of IRIs provided. In this example, the topic of the dataset is "feminism." This is how you decode the IRIs:
    ![search-within](/img/documentation/researchspace-example-IRI-(c-LINCS).png)
     In the highlighted example, _feminism_ is the label, ```(http://id.lincproject.ca/cwrc/feminism)``` is the IRI, ```(a http://cido-crm.org/cidoc-crm/E89_Propositional_Object)``` is the type or class, and from ```http://id.lincsproject.ca/cwrc``` indicates the dataset this IRI derives from.  
- **Temporal Coverage**: Use this field to indicate the date range covered in the dataset.
- **Disclaimer**: Use this field if you wish to add a disclaimer to the dataset landng page and/or on the entity pages.