---
description: "Map and convert XML data."
title: "X3ML (3M)"
last translated: 2023-07-26
---



# X3ML (3M)

<div className="function-button-row">
    <FunctionButton buttonName="Convert"></FunctionButton>
</div>

3M, the Mapping Memory Manager, is an open-source schema-mapping tool run on the X3ML engine. 3M can be used to map and convert data from XML into :Term[Linked Data (LD)]{#linked-data} following the :Term[Resource Description Framework (RDF)]{#resource-description-framework}. 3M allows users to create, edit, and save mapping rules so that they can be reused in future data conversion. 

<div className="banner">
    <img src="/img/documentation/x3ml-overview-logo-(c-owner).png" alt="3M logo"/>
</div>

<div className="primary-button-row">
    <PrimaryButton link="https://mapper.lincsproject.ca/" buttonName="To the Tool" />
    <PrimaryButton link="https://github.com/isl/x3ml" buttonName="To GitHub (X3ML Engine)" />
    <PrimaryButton link="https://gitlab.isl.ics.forth.gr/cci/3m-docker" buttonName="To GitLab (X3ML Interface)" />
</div>

## 3M and LINCS

LINCS uses 3M in the [Conversion](/docs/create-data/publish-data/publish-implement-mapping) step of the [Structured Data Workflow](/docs/create-data/publish-data/publish-workflows) to convert structured data to :Term[Linked Open Data (LOD)]{#linked-open-data} following the rules of selected :Term[ontologies]{#ontology}. LINCS uses :Term[CIDOC CRM]{#cidoc-crm} as its core ontology, along with other ontologies, such as the :Term[Web Annotation Data Model (WADM)]{#web-annotation-data-model} for specific domain requirements. However, 3M can be configured to work with any properly formed ontology.

LINCS likes 3M because it:

* Creates consistent data following the constraints of the ontology
* Converts new data following the same structure as past conversions
* Supports intermediary :Term[nodes]{#node}, which are often required for CIDOC CRM mappings
* Requires no coding knowledge thanks to its graphical interface

If you are preparing data for contribution to the LINCS :Term[triplestore]{#triplestore}, please consult with the LINCS team before mapping your data in 3M. The LINCS team will work with you to create a [conceptual mapping](/docs/create-data/publish-data/publish-develop-mapping/) of your data, which you will use to ensure that the LD you create matches your original dataset and supports your research.

You can use the LINCS instance of 3M even if you are not contributing your data to the LINCS triplestore. You simply need to [register for an account](https://mapper.lincsproject.ca/3m/Registration), wait for account approval from LINCS, and then you are free to use the instance with your data.

:::info

LINCS recommends you spend some time learning 3M before you start working with your data:
* Students typically take: 
  * 1–3 days to become familiar with 3M
  * 2–3 weeks to become intermediate users
* Intermediate users typically take:
  * 1–4 weeks to complete a mapping, depending on the complexity of the data
{/*Remember to add in estimate about learning time from AdArchive */}

For more information, see our [Conversion Workflows](/docs/create-data/publish-data/publish-implement-mapping).

:::

## Prerequisites

Users of 3M:

* Need to come with their own dataset
* Need to come with a conceptual mapping for their dataset
* Need to create a [user account](https://mapper.lincsproject.ca/3m/Registration)

3M supports the following inputs and outputs:

* **Input:** XML
* **Output:** RDF-XML, TTL, N-Triples

:::info

Need to get your data into XML? Learn more about [Preparing Your Data for 3M](/docs/tools/x3ml/preparing-data).

:::

## Resources

To learn more about 3M, see the following resources or [continue to explore our 3M documentation](/docs/tools/x3ml/x3ml-introduction). Resources prior to 2022 refer to a previous version of 3M, but the information contained in them is useful nonetheless.

**Technical Manuals:**

* Kritsotakis et al. (2022) [“3M Guidelines”](https://gitlab.isl.ics.forth.gr/cci/3m-docker/-/blob/master/manual.pdf)
* Theodoridou et al. (2010) [“Mapping Memory Manager (3M) Instance and Label Generator Rule Builder”](https://mapping.d4science.org/3M/Manuals/en/X3ML_Generators_Manual.pdf)

**Introductions and Tutorials:**

* Bruseker (2018) [“General Introduction to the Use of X3ML Toolkit”](https://cidoc.mini.icom.museum/wp-content/uploads/sites/6/2018/12/25.09_WS_CRM.5_Bruseker_X3MLToolkitTutorial.pdf)
* Kräutli (2018) [“CIDOC-CRM by Practice”](https://dh-tech.github.io/workshops/2018-10-15-CIDOC-CRMbyPractice/#/) [Video]
* Theodoridou (2021) [“Mapping & Transforming Data for Semantic Integration: The X3ML Toolkit”](https://www.rd-alliance.org/system/files/documents/4_3M-X3ML-RDA%2017VPM%20%5B2021-04-20%5D.pptx)
* Theodoridou (2018) [“The X3ML Toolkit”](https://projects.ics.forth.gr/isl/cci/demos/brochures/CIDOC2018-X3ML-Tutorial.pdf) 

**Technical Articles and Background Information:**

* Marketakis et al. (2017) [“X3ML Mapping Framework for Information Integration in Cultural Heritage and Beyond”](https://dl.acm.org/doi/10.1007/s00799-016-0179-1)
* Minadakis et al. (2015) [“X3ML Framework: An Effective Suite for Supporting Data Mappings”](http://users.ics.forth.gr/~fgeo/files/X3ML15.pdf)

**Case Study:**

* Theodoridou (2014) [“Mapping Cultural Heritage Information to CIDOC-CRM”](https://www.slideshare.net/MariaTheodoridou/london-meetup2014-mappingchi2cidoccrm)

Information about the team that developed [X3ML](/docs/about-lincs/credits/tools-credits#x3ml) is available on the Tool Credits page.
