---
title: "Context Explorer Documentation"
---

## Prerequisites

### Install Explorer

To install the explorer:

1. Navigate to the [Context Explorer page](https://chrome.google.com/webstore/detail/lincs-entity-search-tool/ingihipdallefgbcbnkanjnhlkdkjecf) on the Chrome Webstore:

`https://chrome.google.com/webstore/detail/lincs-entity-search-tool/ingihipdallefgbcbnkanjnhlkdkjecf`

2. Click **Add to Chrome** and then **Add Extension**
3. Click **Extensions** (puzzle piece icon, located on the right-hand side of the Chrome menu bar) and pin the explorer to your browser

:::note

There is currently no French support for the Context Explorer.

:::

## Use Explorer

To use the explorer, navigate to a webpage you would like to scan and click the LINCS icon that you pinned to your browser's menu bar. There are two scanning options:

- Click **Scan Highlighted** to scan a highlighted section of text on the page
- Click **Scan Whole Page** to scan the entire webpage, including its :Term[metadata]{#metadata}

The Context Explorer will then highlight :Term[entities]{#entity} (persons, places, groups, or works) on the webpage. If you hover over a highlighted entity, the explorer will search for possible matches in the LINCS :Term[triplestore]{#triplestore} and will list the matches in a right-hand sidebar. The explorer will try to display the most-relevant match first. To display additional information, select a match from the list of options provided.

When you click on one of the suggested matches, a context tree will open. This tree shows how your selected entity relates to other entities in the triplestore. Click the page icon on the right-hand side of the sidebar to view information about the entity. Click on **View More** to access further information, including a link to the entity in [ResearchSpace](https://rs.lincsproject.ca). If you do not like the matches provided, click **Back to results** at the top of the sidebar to return to the list of matches and select another option.

To exit the context tree for an entity, click the **escape key** or click somewhere else on the page. You can also click the **X** button in the top right corner of the explorer sidebar to close the scan.

## Customize Explorer

You can customize the Context Explorer.

### Edit Highlights

Click on the **Context Explorer icon** in your browser's menu bar to open the explorer's main window. In this window, click on the settings wheel in the top right corner to change the explorer’s highlight settings.

- Select **Display First Occurrence Only** to make the explorer only highlight the first mention of an entity. Note that if the first occurrence of an entity is in the webpage’s metadata (which is not displayed on your screen), or if two different entities are spelled the same (e.g., the page refers to both a person and a place named _Virginia_), some entities will be missed.
- The **Highlight Relevance Score** changes the score needed for the explorer to highlight an entity in the text. Too high of a relevance score will result in few entities being highlighted.

### Edit Filters

You can add filters to the Context Explorer so that it only highlights entities of a certain :Term[Named Entity Recognition (NER)]{#named-entity-recognition} type or entities from a specific LINCS dataset. For example, if you select _people_, the explorer will only highlight the entities that are people. If you select _Orlando_, the explorer will only highlight entities found in the Orlando dataset.

It is possible to filter by the below NER types and datasets:

|                        NER Type                        |       Dataset       |
| :----------------------------------------------------: | :-----------------: |
|                      Named Events                      |      AdArchive      |
|    Facilities (buildings, airports, highways, etc.)    |  Anthologia graeca  |
|               Countries, cities, states                |   Ethnomusicology   |
|             Named documents made into law              |       HistSex       |
| Non-state locations (mountains, bodies of water, etc.) |        MoEML        |
|      Nationalities, religious or political groups      |       Orlando       |
|                     Organizations                      |      USask Art       |
|                         People                         |   Yellow Nineties   |
|          Objects, vehicles, food items, etc.           |                     |
|         Works of art (books, songs, paintings)         |                     |

:::warning

The Context Explorer is frequently updated. Your web browser may or may not update plugins automatically depending on your settings. It is a good idea to ensure you are always using the most current version. You can check whether your version of the Context Explorer is up to date by clicking on the **extensions button** (puzzle piece) in the Chrome menu bar and to **Manage Extensions** at the bottom of the window that opens, or by visiting the Context Explorer page on the Chrome webstore.

:::
