---
sidebar_position: 7
title: "2023 Conference"
description: "May 5-7, 2023, Guelph"
---

 ![alt="Making Links event logo"](/img/documentation/events-making-links-2023-(c-LINCS).png)

The Making Links: Connections, Cultures, Contexts conference (May 5-7, 2023 at the University of Guelph) addresses a wide range of topics relevant to creating and using linked data for research on cultural scholarship and cultural memory in the Canadian context and beyond. It marks the beta launch of the Linked Infrastructure for Networked Cultural Scholarship (LINCS) suite of tools for the creation, exploration, and use of linked open data.

## Panels, Papers, and Keynotes

**Friday 5 May**

### Land Acknowledgement and Introduction

- Susan Brown (Guelph)
- Ruediger Mueller (Acting Dean of Arts; Guelph)
- Barbara McDonald (Associate University Librarian, Research; Guelph)
- Karina McInnis (Associate Vice-President; Research) (Guelph)

### LINCS Beta Launch

- Pieter Botha (Guelph)
- Susan Brown (Guelph)
- Kim Martin (Guelph)
- Sarah Roger (Guelph)
- Deb Stacey (Guelph)

### Keynote: “Form in Forms: A Conversation about Meaning Making in Data Visualization”

**Chair:** Susan Brown (Guelph)

- Isabel Meirelles (OCADU)
___

**Saturday 6 May**

### Ontologies for Situated, Contextualized Linked Open Data

**Chair:** Stacy Allison-Cassin (Métis Nation of Ontario; Dalhousie)

- Susan Brown (Guelph)
- Erin Canning (Oxford)
- Jessica Ye (Toronto)

### Linking Up: Data Reconciliation

- “3M and OpenRefine Extension”
    - Natalie Hervieux (Alberta)
    - Jessica Ye (Toronto)
- “VERSD”
    - Alliyya Mo (Guelph)

### LINCS User Experience Panel

- Robin Bergart (Guelph)
- Nem Brunell (Toronto)
- Jordan Lum (Toronto)
- Kim Martin (Guelph)

### Linked Browsing: The Context Explorer

**Chair:** Susan Brown (Guelph)

- Pieter Botha (Guelph)
- Dawson MacPhee (Guelph)

### Feminist Explorations

**Parallel Session A** | **Chair:** Janelle Jenstad (Victoria)

- “Representing Missing Links with Linked Data: Archival Ghosts, Digital Haunting, and Absence in AdArchive”
    - Timothy Arthur (Alberta)
    - Michelle Meagher (Alberta)
    - Jana Smith Elford (Medicine Hat)
- “Feminism, Art and Politics ... and Data”
    - Diane Jakacki (Bucknell)
    - Heresies Project Team (Bucknell)
- “From SPARQL to Tableau: How to Transform LINCS Data into Tableau Visualizations to Explore Datasets and Investigate Ideas”
    - John Brosz (Alberta)

### Infrastructural Ecosystem Futures

**Parallel Session B** | **Chair:** Constance Crompton (Ottawa)

- “Ten Years of Semantic Web in a Research Infrastructure, What Uses for the Future?” [en français]
    - Stéphane Pouyllau (CNRS, Huma-Num Research Infrastructure, France)
- “Persistent Identifiers as Research Infrastructure”
    - Lisa Goddard (Victoria)
- “Towards a New LOD-forward Research Infrastructure in Canada”
    - James MacGregor (Canadian Research Knowledge Network)
    - Angela Joosse (Canadian Research Knowledge Network)

### On-ramps for Creating Linked Open Data

**Chair:** Diane Jakacki (Bucknell)

- “Changing Languages, Preserving Meaning: Converting TEI to CIDOC-CRM with XTriples”
    - Constance Crompton (Ottawa)
    - Alice Defours (Ottawa)
    - Rajvi Khatri (Ottawa)
- “The LINCS-LEAF Virtual Research Environment to Linked Data Bridge”
    - Mihaela Ilovan (Alberta)
- “Linked Editing Academic Framework LEAF-Writer + the Named Entity Recognition Vetting Environment”
    - Luciano Frizzera (Concordia)
___

**Sunday 7 May**

### Linked Data in the TEI (Text Encoding Initiative) Panel

**Parallel Session C** | **Chair:** Janelle Jenstad (Victoria)

- Janelle Jenstad (Victoria)
- James Cummings (Newcastle)
- Diane Jakacki (Bucknell)
- Constance Crompton (Ottawa)
- Magdalena Turska (e-editiones/TEI Publisher)

### Linking Collections

**Parallel Session D** | **Chair:** Lisa Goddard (Victoria)

- “Challenges in Disambiguating Entity Data: The Advanced Research Consortium’s Approach to Vetting URIs”
    - Lauren Liebe (Texas A&M)
- “Towards Theoretical Linking in the History of Sexuality”
    - Bri Watson (UBC)
- “Here, There, Everywhere: Reconciling as a Reckoning with Place”
    - Paul Barrett (Guelph)
    - Sarah Roger (Guelph)

### Respectful Terminologies and Decolonizing Linked Data

**Chair:** Deanna Reder (SFU)

- Stacy Allison-Cassin (Métis Nation of Ontario; Dalhousie)
- Camille Callison (Tahltan Nation; Fraser Valley)

### Interfaces for Exploration

**Chair:** Jon Bath (Saskatchewan)

- “LINCS ResearchSpace”
    - Robin Bergart (Guelph)
    - Zach Schoenberger (Victoria)
- “Stratocumulus”
    - Akseli Pálen (Texas A&M)
    - Bryan Tarpley (Texas A&M)

### Linking In: Natural Language Content

**Chair:** Deb Stacey (Guelph)

- “Linking to and within Voyant Tools”
    - Andrew MacDonald (McGill)
    - Pieter Botha (Guelph)
- “Natural Language Processing for Linking and Relating Data”
    - Natalie Hervieux (Alberta)

### Linking Forward: Next Steps

- Jon Bath (Saskatchewan)
- Susan Brown (Guelph)
- Kim Martin (Guelph)
- Deb Stacey (Guelph)

### Keynote: “I Want You to See What I See: Linked Data in the Canadian Context”

**Chair:** Kim Martin (Guelph)

- Amy Buckland (Assistant Deputy Minister, Collections; Library and Archives Canada)

## Call for Papers

“Making Links: Connections, Cultures, Contexts” is a conference on linked open data for cultural scholarship and cultural memory taking place May 5–7, 2023 at the University of Guelph.

Making Links is an opportunity to present and learn about exciting research and tool development on a wide variety of topics covering all aspects of linked data for the study of culture in the Canadian context and beyond. It will mark the launch of the Linked Infrastructure for Networked Cultural Scholarship (LINCS) suite of tools for the creation, exploration, and use of linked open data. The conference will be followed May 9–12 by the [DH@Guelph Summer Workshops](https://www.uoguelph.ca/arts/dhguelph/summer2023), which will feature an array of offerings including how to use LINCS. Both events will emphasize diversity, inclusion, and a supportive environment for intellectual exchange and learning.

We invite proposals for posters/digital demonstrations, short papers (10 min), or full papers (20 min), on any of the following topics:

- Linked open usable data (LOUD)
- Linked data projects and initiatives from Canada and beyond
- Humanities research on the semantic web (specific projects, ideas, datasets)
- Linked library metadata applications and challenges
- Linked data and Traditional/Indigenous Knowledges
- Linked data in the cultural heritage sector
- Linked data and intersectional identities, diversity, equity, and inclusion
- Ontology architecture, ontology mapping, and dynamic ontologies
- Representing changing knowledge (epistemes, vocabularies, paradigms)
- Practical strategies, tools, and workflows for creating and refining linked data
- Storage, versioning, and preservation of linked data
- Dissemination strategies for the humanities: visualization and beyond
- Search, browse, and serendipitous discovery
- Artificial intelligence and linked data
- Ethics, privacy, and access implications of linked data
- Evidence, provenance, and context in linked open data

## Program Committee

- Stacy Allison-Cassin (Métis Nation of Ontario; Dalhousie)
- Jon Bath (Saskatchewan)
- Susan Brown (Guelph)
- Janelle Jenstad (Victoria)
- Kim Martin (Guelph)
- Sarah Roger (Guelph)
- Deb Stacey (Guelph)

## Support

Conference generously supported by the Connections Grant program of the Social Sciences and Humanities Research Council, by the School of English and Theatre Studies, the College of Arts, and the McLaughlin Library at the University of Guelph, and by the Canada Research Chairs Program.

The LINCS Project is funded by the Canada Foundation for Innovation.

 ![alt=""](/img/documentation/events-making-links-2023-support-(c-LINCS).png)

