---
sidebar_position: 5
title: "OpenRefine Workshop Materials"
description: "Resources for LINCS OpenRefine workshop attendees"
---

This page has all of the information you need to prepare for attending a LINCS OpenRefine workshop.

We recommend you install OpenRefine, download the files, and read up on Linked Open Data at least a few days in advance of the workshop. If you have any questions or encounter any problems, please [contact us](mailto:lincs@uoguelph.ca) ahead of the workshop so that we can help get you set up.

## Install OpenRefine

To participate in the workshop, you must have OpenRefine installed on your computer:

1. [Download OpenRefine](https://openrefine.org/download.html).
2. Follow the [installation instructions](https://docs.openrefine.org/manual/installing).
3. Launch OpenRefine on your computer to confirm that it has been installed correctly.

OpenRefine runs on your computer in a web browser window, typically at [http://127.0.0.1:3333/](http://127.0.0.1:3333/).

### Optional Installation Steps

The following steps are optional. They will increase OpenRefine's performance but are not necessary for participating in the workshop. You can follow these steps now, or you can return to them in future.

- [Change the settings](https://docs.openrefine.org/manual/installing#increasing-memory-allocation) to provide OpenRefine with access to more memory on your computer. Access to more memory makes it possible for OpenRefine to process larger files.

- Download and [install](https://openrefine.org/docs/manual/installing#installing-extensions) OpenRefine extensions. Extensions provide OpenRefine with additional functions. LINCS recommends the [RDF Extension](https://github.com/stkenny/grefine-rdf-extension), which although not used in the workshop, you may find handy in future.

:::note

If you have previously installed OpenRefine on your computer, we recommend you check for updates, and if necessary, [install](https://openrefine.org/docs/manual/installing#installing-or-upgrading) the [most recent release](https://openrefine.org/docs/manual/installing#installing-or-upgrading).

:::

## Download the Sample Data Files

During the workshop, you will have opportunities to clean and reconcile data using OpenRefine. You will need to download two files of sample data to use:

- [Data cleaning sample](https://drive.google.com/file/d/1S8VObIUZopoL2d75vB5uIsv3RGIon_qT/view?usp=sharing)
- [Data reconciling sample](https://drive.google.com/file/d/1eW6GKR9g7bBAL8SsC-V9QF_oWDNj_Lve/view?usp=sharing)

Make sure to save these files where you will be able to find them during the workshop.

All workshop attendees will work from the same sample data to begin. Depending on time available, you may also have an opportunity during the workshop to try out your own data. You can also bring questions about working with your own data to our drop-in sessions.

## Background Learning

If this is your first time attending a LINCS workshop and/or your first exposure to Linked Open Data, we ask you to familiarize yourself with LINCS and the principles of Linked Open Data in advance. Check out the following pages:

- LINCS [Learning Resources](https://lincsproject.ca/docs/get-started/learning-resources), especially [Linked Open Data Basics](/docs/get-started/linked-open-data-basics) for an overview of important Linked Data concepts
- LINCS [Conversion Workflows](/docs/create-data/publish-data/publish-workflows/), especially [Clean Data](/docs/create-data/publish-data/publish-clean/) and the [Data Cleaning Guide](/docs/create-data/publish-data/publish-clean/cleaning-guide), and [Reconcile Data](/docs/create-data/publish-data/publish-reconcile/) and the [Entity Reconciliation Guide](/docs/create-data/publish-data/publish-reconcile/reconcile-guide)
