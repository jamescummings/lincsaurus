---
sidebar_position: 2
title: "Project Charter"
description: "Our guide for relating to each other to achieve our goals"
sidebar_class_name: "hide"

---

:::info

The LINCS Project Charter can be signed digitally using an [online form](https://uoguelph.eu.qualtrics.com/jfe/form/SV_8AmdNB3AjYyMq4m).

If you are a **student** working on the LINCS project, please discuss the terms of the charter with your supervisor. This form will ask you to provide the name and email address of your supervisor. Your supervisor will receive an email asking them to sign to confirm that you have discussed the charter together.

If you are a **supervisor**, please review the charter with your student and ask them to sign it. Once they have signed, you will receive an email asking you to sign alongside your student. Supervisors must also sign the project charter.

:::

# LINCS Project Charter

## Core Values

LINCS works (1) to enable Canadian researchers to create interlinked and contextualized online data about culture to benefit scholars and the public, and (2) to make cultural data, more equitable, accessible, findable, shareable, and reusable, specifically by:

* Empowering researchers to link their data
* Hosting linked datasets
* Providing tools for creating, querying, filtering, visualizing, sharing, annotating, and refining linked data; and
* Prioritizing voices and communities that are underrepresented on the Web with a view to promoting equity, diversity, inclusion, and Indigenous ways of knowing.

A full description of the project’s core values can be found in the [LINCS Mission Statement](/docs/about-lincs/policies/mission-statement).

This charter is designed to guide how participants in the LINCS Project relate to each other to achieve LINCS’s mission. It is complemented by policies that support LINCS’s commitment to creating and disseminating data that is accessible and sustainable; data that fosters equity and diversity; and data that contributes to reconciliation and anti-racism. More information about these commitments can be found in the [LINCS Mission Statement](/docs/about-lincs/policies/mission-statement), [LINCS Data Publication License Agreement](/docs/about-lincs/policies/data-publication-license-agreement), and [LINCS Ontologies Adoption and Development Policy](/docs/about-lincs/policies/ontologies-adoption-and-development). Signing the LINCS Project Charter indicates acknowledgement of—and a commitment to work towards—these principles.

In addition to the text of this Charter, we endorse the values and practices articulated in the following documents that have emerged from various communities and contexts:

* [A Student Collaborators’ Bill of Rights](https://humtech.ucla.edu/news/a-student-collaborators-bill-of-rights/)
* [Postdoctoral Bill of Rights](https://hcommons.org/deposits/item/hc:26741/)
* [Contributors Covenant](https://www.contributor-covenant.org/)
* [Collaborators’ Bill of Rights](http://mcpress.media-commons.org/offthetracks/part-one-models-for-collaboration-career-paths-acquiring-institutional-support-and-transformation-in-the-field/a-collaboration/collaborators%E2%80%99-bill-of-rights/)
* [ICMJE Guidelines on the roles of authors and contributors](http://www.icmje.org/recommendations/browse/roles-and-responsibilities/defining-the-role-of-authors-and-contributors.html)

This is a living document that we will revisit and revise as occasions for doing so arise or as we are invited to by participants. Anyone with questions or who wishes to open up discussion of this document is invited to contact the LINCS Project Manager at [lincs@uoguelph.ca](mailto:lincs@uoguelph.ca).

### Participants
People participate in LINCS in a range of ways, and roles often shift over time. See [LINCS Membership Roles](/docs/about-lincs/policies/membership-roles) for an outline of roles and responsibilities of participants.

Students are participants of the project in their own right and should sign this charter. It is the responsibility of their supervisor to inform students working with LINCS or LINCS-affiliated projects about this Charter.

### Progress

**We intend this work to move forward at a steady pace, given due awareness of unanticipated life events.** Project participants will make every effort to attend meetings as arranged and to keep in regular contact by email or other electronic means. Frequent absence may result in being warned, then cautioned, then asked to leave the team.

Project participants will jointly establish and attempt to meet self-imposed deadlines, in part through providing the Project Manager with lists of commitments, so that reminders will be sent out as a matter of routine.

In the event the task is overdue by a considerable amount of time (for instance, whichever is lesser—two months, or double the original timeframe), other participants may at their discretion notify the offender that the task will be reassigned, without prejudice to the constitution of the team or the public credit of any participant.

Project phases will be arranged so as to minimize the need for sequential completion of one phase before another can begin: wherever possible, phases will run in parallel, with communication occurring between people as they work on each phase, rather than waiting to communicate until the end.

### Reporting

**We recognize the need to track our work and to report upon it.** Reporting is required by CFI and other funders, and is sometimes required to release payments.

Participants will keep track of the work they do for the LINCS project and will report on this work in a timely manner.

On an annual basis, participants will send a list of all outputs related to LINCS, including (but not limited to):

* **Publications:** peer-reviewed publications, conference papers, workshop presentations, books, research and technical reports, reference and training tools and materials, blogs and other online publications
* **Funding:** additional funding secured, formal research collaborations
* **Personnel:** project hires (including highly qualified personnel, postdoctoral fellows, and graduate and undergraduate students)
* **Further Ventures:** intellectual property rights, spin-off companies

### Credit and Intellectual Property

**We recognize all kinds of work are equally deserving of credit.** Participants should discuss possible publication venues before submitting abstracts or articles.

For presentations or papers where this work is the main topic, all participants who worked directly on this subproject should be co-authors. Any participant can elect at any time not to be listed, but may not veto publication. For presentations or papers that spin off from this work, only those participants directly involved need to be listed as co-authors. Others should be mentioned if possible in the acknowledgements, credits, or article citations.

Copyright in scholarly works and publications will remain the property of the author.

All work drawing on LINCS data, tools, code, or other products should include acknowledgement of the project with a link to the LINCS website in a form appropriate to the context.

All work drawing on LINCS data, tools, code, or other products should include acknowledgement of the project with a link to the LINCS website in a form appropriate to the context. We encourage crediting individuals where appropriate. Information about acknowledging LINCS, including [citing the project](/docs/about-lincs/cite" and [crediting contributors](/docs/about-lincs/credits), is available on the project website.

Project participants at LINCS Member institutions are bound by the intellectual property provisions of the Inter-Institutional Agreement, which stipulate that all data, code, and intellectual outputs produced with CFI resources will be open and published under the Creative Commons Attribution-NonCommercial 4.0 for information, as described in the LINCS [Data Publication License Agreement](/docs/about-lincs/policies/data-publication-license-agreement), and an open-source license for software and code, as described in the LINCS [Software License](/docs/about-lincs/policies/software-license). This may be complemented by policies and procedures of partner institutions.

The Members agree that certain subsets of data produced by LINCS, such as that related to Indigenous or Traditional Knowledge, may require different hosting arrangements or may not be freely shared.

### Dissemination

**We are interested in disseminating the results of this project as widely as possible, with credit to us for doing it.** Project participants may refer to LINCS tools, procedures, or collaborations as examples in presentations, papers, interviews, and other media opportunities. They may post any of the material to which they contribute to their websites. Wherever possible, they should mention the names of the other project participants who were directly involved, as well as the name of the project.

The project team will maintain a collaborative [project website](https://lincsproject.ca/), which will contain links to all the presentations and publications of the group.

The project will create a graph of the project participants with links to their LINCS roles and datasets to which they have contributed, their publications, and other publicly available data related to their research.

The [LINCS project logo](/docs/about-lincs/cite#acknowledge-lincs) is available to all participants to add to the above-mentioned methods of dissemination.

### Communication

**We recognize the importance of both internal and external communication to this project.** Project participants should remain in communication with their teams about current work (via email or Slack), and should contact the Project Manager in times they are unable to do so.

The project website is one place that participants can share their achievements publicly. The project Twitter handle (@lincsproject) can be tagged in social media. Participants are encouraged to submit news, achievements, and items of interest from within and beyond the project to the project newsletter.

### Funding

**We would prefer for this work to be funded.** Project participants will watch for and notify each other of funding opportunities and participate wherever possible in the writing of appropriate grant proposals.

### Dignity

**We wish to communicate in such a way as to preserve professional dignity.** We will strive to maintain a tone of mutual respect whenever we write or meet, and to forgive lapses if they occur.

The LINCS [Code of Conduct](https://lincsproject.ca/docs/about-lincs/policies/code-of-conduct) provides a framework for participating in all LINCS events, whether virtual or in person. Its principles for creating collaborative, inclusive, respectful environments are to be upheld in all of our interactions.

We will attempt to keep communications transparent, for example, by copying everyone involved in any given discussion, and by directly addressing with each other any questions or concerns that may arise.

### Goodwill

**We would like to foster goodwill among all the participants.** In making financial decisions, we will attempt to allocate resources in ways that indicate commitment to each of the people on the team.

Participants will also watch for and notify each other of opportunities for commercialization and licensing. Any commercial agreements or plans will be made so as to include and equally benefit all participants.

We will strive to be a group working towards different parts of a larger, coherent, and important whole—one that promises to exceed the sum of its parts.

### Risk and Harm

**Certain activities are more likely to result in harm or threat for some participants than others, especially (but not exclusively) with regards to security online.** Participants should make their own decisions regarding their safety, and be respected and supported for those decisions.

We will bear power imbalances in mind when making requests of one another.

We will avoid assuming that every participant has the same needs, and plan (and listen) accordingly.

We will give particular attention to the realities of international collaborators since those might be less visible to some of us. We will work to maintain awareness that some disabilities and oppressions may not be apparent.

We will create opportunities for networking and community building that do not centre on alcohol consumption. We recognize that not everyone has access to the same set of resources (time, money, travel, access to support systems, etc.) and will provide a variety of methods of involvement.

### Conflict Resolution

**Conflicts or grievances related to the LINCS project itself will, where possible, be handled via the lines of authority and communication outlined in the LINCS organizational structure, with a member of the Executive Committee copied on all documentation of the issue and its resolution.** The Project Manager will act as an ombud, should informal guidance for how to proceed be desired. Decisions will typically be handled by the Executive Committee. Should this arrangement prove insufficient, for instance if a conflict involves someone in the line of authority, the Executive Committee will seek advice on how to handle the situation in consultation with some or all of the relevant University’s or Research offices, or the chair of the Advisory Board. Grievance or conflict settlement may result in a request for leave, resignation, or dismissal, in which case the appropriate policies will be followed depending on the individual and situation involved.

Conflicts and grievances related to employment will need to be handled according to the processes of the institution or organization involved.

### Privacy

**We wish to respect each participant’s privacy.** The LINCS project will adhere to ethical guidelines about communication and information sharing, while continuing to encourage a community mindset.

### Future Phases

**We understand that the work we do on this project may have future phases. Modifications and additions may be made to further the project by other participants.** To enable future work, participants will store in a shared drive all collaboratively or project-produced documents, presentations, images, including all original files for designs and illustrations in unflattened and editable form. Where copyright restrictions do not apply, fonts should also be included in shared files. Code shall be shared with open licenses as noted in the IP policy. Insofar as ethics clearances allow, project-specific (rather than contributed) data backup will be provided through central project servers or cloud drives

As projects progress to new phases, each participant will have the right of first choice over whether or not to continue with the project.

## Document Details

**Version:** 1.3 {/* UPDATE */}

**Authors:** Susan Brown (University of Guelph), Jon Bath (University of Saskatchewan), Constance Crompton (University of Ottawa), Janelle Jenstad (University of Victoria), Kim Martin (University of Guelph), Sarah Roger (University of Guelph)

**Last Updated:** 2023-12-12 {/* UPDATE */}

**Released:** 2023-12-12 {/* UPDATE */}

