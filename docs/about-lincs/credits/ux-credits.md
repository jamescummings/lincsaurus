---
sidebar_position: 4
title: "User Experience Credits"
description: "Credits for LINCS’s User Experience (UX) and user testing"
sidebar_class_name: "hide"
---

Credits are provided for the User Experience (UX) design and user testing of LINCS’s tools and materials. For more information on how to construct citations for LINCS, see [Cite](/docs/about-lincs/cite). To sign up to be a UX tester for LINCS, see [Participate in UX Testing](/docs/about-lincs/get-involved/participate).

## UX Testing and Design

- Kim Martin (UX Lead)
- Robin Bergart
- Nem Brunell
- Jordan Lum
- Farhad Omarzad
- Evan Rees

## UX Testers

- Sandra Ansic
- Timothy Arthur
- Sehrish Basir
- Jon Bath
- Reg Beatty
- Carrie Breton
- Grace Campagna
- Chiara Cremona
- Meg Ecclestone
- Iman El Gamal
- Kailey Fallis
- Michael Frischkopf
- Amelia Flynn
- Marion Tempest Grant
- Janelle Jenstad
- Diane Jakacki
- Wayne Johnston
- Tim Knight
- Lorraine Janzen Kooistra
- Jingyi Long
- Filomena Lopedoto
- Jenny Marvin
- Juliene McLaughlin
- Michelle Meagher
- Antonio Munoz Gomez
- Jaehoon Pyon
- Ibrahim Rather
- Moni Razavi
- Sarah Roger
- Sarah Simpkin
- Thomas Smith
- Jana Smith Elford
- Elena Spadini
- Marion Tempest Grant
- Mathilde Verstraete
- Marcello Vitali-Rosati
- Bri Watson
- Caroline Winter
