---
sidebar_position: 1
title: "Documentation Credits"
description: "Credits for LINCS’s documentation"
sidebar_class_name: "hide"
---

Credits are provided for the documentation written by LINCS collaborators. For more information on how to construct citations for LINCS, see [Cite](/docs/about-lincs/cite/).

## Translation

- Els Thant (Lead Translator)
- Constance Crompton
- Alice Defours
- Jasmine Drudge-Willson

## Account Service

- Sarah Roger (Lead Author)
- Pieter Botha
- Alliyya Mo

## Authority Service

- Natalie Hervieux (Lead Author)
- Kate LeBere

## Context Explorer

- Kate LeBere (Lead Author)
- Sarah Roger
- Pieter Botha

## Conversion Workflows

- Natalie Hervieux (Lead Author)
- Sarah Roger
- Kate LeBere
- Constance Crompton

## Corpora

- Lauren Liebe
- Bryan Tarpley

## CWRC

- Luciano Frizzera
- Mihaela Ilovan

## LEAF-Writer

- Kate LeBere (Lead Author)
- Luciano Frizzera
- Mihaela Ilovan
- Sam Peacock
- Sarah Roger
- Megan Sellmer

## Linked Data Enhancement API

- Natalie Hervieux

## NERVE

- Luciano Frizzera

## OpenRefine

- Natalie Hervieux (Lead Author)
- Sarah Roger

## ResearchSpace

- Robin Bergart (Lead Author)
- Sarah Roger
- Zach Schoenberger

## Rich Prospect Browser

- Bryan Tarpley

## SPARQL Endpoint

- Kate LeBere
- Alliyya Mo
- Sarah Roger

## Spyral

- Andrew MacDonald
- Jingyi Long

## VERSD

- Kate LeBere (Lead Author)
- Alliyya Mo
- Sarah Roger

## Vocabulary Browser

- Kate LeBere

## Voyant

- Andrew MacDonald
- Jingyi Long

## X3ML (3M)

- Jessica Ye (Lead Author)
- Erin Canning
- Natalie Hervieux
- Sarah Roger
