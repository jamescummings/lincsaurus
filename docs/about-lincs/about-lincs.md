---
sidebar_position: 1
title: "About LINCS"
sidebar_class_name: "landing-page"
description: "LINCS enables researchers to create interlinked and contextualized online data about culture to benefit scholars and the public."
---

## Our Mission

The Linked Infrastructure for Networked Cultural Scholarship (LINCS) enables researchers to create interlinked and contextualized online data about culture to benefit scholars and the public.

LINCS’s [mission](/docs/about-lincs/policies/mission-statement) is to make cultural data more readily available, shareable, searchable, and reusable.

We are committed to:

- Empowering researchers to link their data
- Hosting linked datasets
- Providing tools for creating, querying, filtering, visualizing, sharing, annotating, and refining :Term[Linked Data (LD)]{#linked-data}

## How Does LINCS Advance Scholarship?

Humanities researchers engage with, analyze, and synthesize heterogeneous bodies of information: information about everything from people and organizations to places and events to concepts and artifacts—as well as the vital relationships between them.

This information informs researchers’ investigations of many questions, including: How do the Canadian government agencies impact creative industries? How do global trade routes relate to resource locations? How do Indigenous storytelling cultures negotiate writing and publishing systems infused with colonial categories? How do social identities impact communities?

### Finding New Connections Through the Semantic Web

Although contemporary scholars have unprecedented quantities of material online for addressing such complex questions, their searches for answers are often hampered by the lack of meaningful connections between resources. As a result, most scholars interact with cultural data only through reading—and not by leveraging algorithmic processes to draw on a wider range of evidence. Researchers need a smarter, :Term[Semantic Web]{#semantic-web} that builds meaning into machine-readable links to elucidate the diverse interconnections, impacts, and significance of human action and expression.

Semantic Web technologies make the web smarter by structuring and linking data. LINCS uses these technologies to interlink Canadian research and heritage data from across the web, converting, connecting, enhancing, and making accessible previously heterogeneous and siloed datasets. Such linking provides pathways towards new insights through networked knowledge production, both within and beyond Canada.

### Transforming Access to Culture

In mobilizing the cultural record, LINCS is transforming access to human culture. It is enabling new and serendipitous insights into shifting social identities related to gender and indigeneity. It is tracing relationships between commodities and natural resources, and exposing cross-fertilizations and tensions that result from immigration, cultural diversity, and social justice movements. Using the resources and tools produced by LINCS, Canadians will be able to explore and investigate culture in transformative ways, informed by newly accessible books, manuscripts, photographs, periodicals, postcards, music, and more.

## Join Us

Individuals and organizations interested in advancing linked data for cultural scholarship are welcome to become involved with the LINCS project. We are happy to see the LINCS network grow!

There are many ways to get involved:

- Use LINCS tools without any formal association with the project
- Contribute your data to LINCS without necessarily having any other formal association with the project
- Become a research collaborator who is formally involved in LINCS
- Become a technical collaborator who is formally involved in the development of the LINCS infrastructure
- Become a project partner—partners include universities funded through the initial :Term[Canada Foundation for Innovation (CFI)]{#canada-foundation-for-innovation} grant, as well as universities, public- and private-sector partners, projects, and organizations that are involved beyond the scope of the CFI grant

To find out more and get involved, please [contact us](mailto:lincs@uoguelph.ca).
