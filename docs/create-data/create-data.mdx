---
title: "Create Data"
sidebar_class_name: "landing-page"
last translated: 2023-08-09
---

import ToolCatalogue from "@site/src/components/lists/toolCatalogue.js";
import DocCardList from '@theme/DocCardList';
import {useCurrentSidebarCategory} from '@docusaurus/theme-common';

When you turn your data into :Term[Linked Open Data (LOD)]{#linked-open-data} you will be able to see your data in new ways, discover surprising connections between your data and other LOD, and contribute to the development of the :Term[Semantic Web]{#semantic-web}.

## Create Linked Open Data 

You can create LOD from any kind of existing data, whether it is a spreadsheet or a database (:Term[structured data]{#structured-data}), text with markup or encoding (:Term[semi-structured data]{#semi-structured-data}), or plain text (:Term[natural language data]{#natural-language-data}). {/* You can also [create LOD from scratch](/docs/create-data/create-linked-data-from-scratch). */}

LINCS provides the tools, infrastructure, and expertise to get you there.

At LINCS, the process of creating LOD is called the **conversion workflow**. There are various paths through this workflow. The path you take depends on the structure and size of your dataset, your timeline, and your research goals. 

You will encounter some or all of these steps along the way:   

- **[Cleaning](#clean)** ensures your data is internally consistent, correctly formatted, and complete.   
- **[Reconciling](#reconcile)** matches (reconciles) your data to :Term[Uniform Resource Identifiers (URIs)]{#uniform-resource-identifier} so that your data will link accurately to other datasets on the web.  
- **[Converting](#convert)** is a multistep process that involves :Term[mapping]{#mapping} (fitting your dataset to an :Term[ontology]{#ontology}), converting (turning the data into :Term[triples]{#triple}), reviewing the data for meaning and accuracy, and publishing (:Term[ingesting]{#ingestion} the data into the LINCS :Term[triplestore]{#triplestore}). 

## Before You Begin

Creating and publishing LOD with LINCS is a significant undertaking that can take several months to complete. Before you decide to work with LINCS to convert your data, you can learn more about LOD and try out some of the LINCS tools:  

- Familiarize yourself with [LOD basics](/docs/get-started/linked-open-data-basics).
- [Explore datasets](/docs/explore-data/) that have already been published and imagine what might be possible for your own data.
- Use [OpenRefine](/docs/tools/openrefine) to [clean](/docs/create-data#clean) and [reconcile](/docs/create-data#reconcile) your data on your own.
- Attend [workshops and events](/docs/about-lincs/get-involved/events) to build your knowledge and skills.
- Review [LINCS data policies](/docs/about-lincs/policies).

When you are ready to discuss creating and publishing your data with LINCS, please [contact us](mailto:lincs@uoguelph.ca)!


## Important Steps for Creating Linked Open Data

### Clean

Cleaning tools allow you to make your data internally consistent so any time a data point is referenced, it is done in the same way throughout the dataset. These tools are also useful for sorting your data into the most specific categories needed for :Term[Linked Open Data (LOD)]{#linked-open-data}. Cleaning is an important step in data management regardless of whether you are making LOD. 

#### Cleaning Tools

<ToolCatalogue preFilter={"Clean"} />

### Reconcile

Reconciliation tools allow you to find and match a :Term[Uniform Resource Identifier (URI)]{#uniform-resource-identifier} from an external database to an entity that exists in your data. This process enhances your dataset with additional information and links it to other datasets and :Term[authorities]{#authority-file} through the shared URIs.

#### Reconciliation Tools

<ToolCatalogue preFilter="Reconcile" />

### Convert

Conversion tools allow you to convert your data from one format to another. Conversion can be used to change data from standard relational formats such as :Term[XML]{#xml} and CSV into :Term[Linked Open Data (LOD)]{#linked-open-data} formats that are structured by :Term[ontologies]{#ontology}. Conversion tools are an important aspect of creating LOD through LINCS’s [conversion workflows](/docs/create-data/publish-data/publish-workflows).

#### Conversion Tools

<ToolCatalogue preFilter="Convert" />


