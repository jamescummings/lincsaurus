---
sidebar_position: 2
title: "Dataset Keywords Instructions"
description: "Choose keywords for your dataset"
---

## Introduction

LINCS uses keywords to help users explore the data. Keywords appear in places such as ResearchSpace, Borealis Dataverse, and on the LINCS website.

:::info
You must keywords for your dataset using the [dataset keywords form](https://uoguelph.eu.qualtrics.com/jfe/form/SV_0lhmZjj47BAAyLI) before LINCS can create a dateset page for it on the LINCS portal or publish and archive your data.
:::

## Principles

As a linked data project, we require keywords that are associated with URIs.

We want keywords to converge, where possible, as this increases their usefulness. We also want researchers to be able to choose keywords from a list that best aligns with their principles and their research domains. To facilitate convergence while respecting domain-specific priorities, we recommend researchers either (a) choose keywords from one of our recommended keywords lists or (b) propose a list to add to our recommendations to promote their use by other researchers.

To ensure a meaningful but manageable number of keywords for each dataset, we ask researchers to provide 3–5 (up to a maximum of 10) keywords.

## Providing Keywords for Your Dataset

* Generate a list of 3–5 keywords (and up to a maximum of 10) for your dataset
* Use the recommended keywords lists to find URIs for your keywords
    * If there is another source for keywords that is better suited to your domain, you are encouraged to use that list instead or as well as the recommended lists
    * You are welcome to mix and match keywords from more than one list, if needed, but we ask that you do not provide URIs for the same keyword from more than one source
    * When choosing among keywords and URIs, we encourage you to consult the LINCS keywords page [coming soon] and to make choices, where possible, that converge on the selections made by other LINCS researchers
* Keywords can be in English or French
* Provide keywords for your dataset using the [dataset keywords form](https://uoguelph.eu.qualtrics.com/jfe/form/SV_0lhmZjj47BAAyLI).

## Recommended Keyword Lists

### General Keywords Lists

* [FAST](http://fast.oclc.org/searchfast/) ([FAST linked data search](https://experimental.worldcat.org/fast/))
* [Wikidata](https://www.wikidata.org/wiki/Wikidata:Main_Page)

### Subject-specific Keywords Lists

* [Arts and Architecture Thesaurus](https://www.getty.edu/research/tools/vocabularies/aat/?find=&logic=AND&note=)
* [Brian Deer Classification Scheme](https://xwi7xwa-library-10nov2016.sites.olt.ubc.ca/files/2019/06/Xwi7xwa-Classification-ver-04March2013P.pdf)
* [Homosaurus](https://homosaurus.org/)

To recommend additional keywords lists to be added to our recommendations, [contact us](mailto:lincs@uoguelph.ca).