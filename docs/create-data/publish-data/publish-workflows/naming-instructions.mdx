---
sidebar_position: 1
title: "Dataset Naming Instructions"
description: "Name your dataset"
---

## Introduction

The dataset title is the title we use to refer to a project’s dataset as it appears in LINCS.

Dataset title should be consistent across LINCS (e.g., in our notes, documentation, [ResearchSpace](/docs/tools/researchspace)). To facilitate this, titles should be chosen as early in the process as possible.

Each dataset must have three titles: long title, short title, and dataset identifier. Projects are asked to provide their own long and short titles. LINCS will recommend a dataset identifier, which the project will be invited to approve.

## All Title Forms

All forms of a dataset title must be:

* Unique to the project
    * No two projects in LINCS can use the same dataset title
    * One project can use identical forms for more than one title (e.g., long title and short title can be the same)
    * If a project has more than one dataset, each dataset must have its own unique title; where possible, these titles should share a common element (e.g., project-people, project-places, etc.)
* Easy to type, spell, and pronounce
    * No random strings
    * No ambiguous combinations like 1l or 0O
    * No offensive content
    * Canadian spellings preferred

## Long Title

The long title is used where a dataset is referred to in full.

**Examples:** the project page on the portal, at first mention in publications, in citations 

The long title must be:

* Maximum 54 characters
* Punctuation, accents, and other special characters are allowed 

A long title can be provided in either or both of English and French. Long title capitalization will be governed by the context in which the title is used (e.g., on the Portal, titles will adhere to the portal style guidelines) and by language (e.g., French capitalization rules will always take precedence for French titles).

:::note

LINCS offers tools and resources in both English and French and is able to support bilingual content. We encourage projects to provide bilingual material, but we are unable to provide translation services. 

:::

## Short Title

The short title is used where space is at a premium.

**Examples:** in drop-down menus and tab headings on the Portal, on entity cards, on second and subsequent mentions in publications, on slides, in labels within the linked data form of your data 

The short title must be:

* Easy to pronounce (e.g., AdArchive) _or_ a long-established project acronym (e.g., MoEML)
* Maximum 20 characters
* Must be clear that the name is specific to the project, and not a more general class of LINCS data:

<table>
  <tr>
    <td>**Project**</td>
    <td>**Acceptable Short Title**</td>
    <td>**Unacceptable Short Title**</td>
  </tr>
  <tr>
    <td>Orlando Project</td>
    <td>Orlando</td>
    <td>Women Writers</td>
  </tr>
  <tr>
    <td>AdArchive: Tracing Pre-Digital Networked Feminisms</td>
    <td>AdArchive</td>
    <td>Advertisements</td>
  </tr>
  <tr>
    <td>University of Saskatchewan Art Collection</td>
    <td>USask Art</td>
    <td>Art Gallery</td>
  </tr>
</table>


A short title can be provided in both English and French. Short title capitalization will be governed by the context in which the title is used (e.g., on the portal, titles will adhere to the portal style guidelines) and by language (e.g., French capitalization rules will always take precedence for French titles).

## Dataset Identifier

The dataset identifier is the shortest version of the dataset title and is used where space is very limited, such as for file names. LINCS creates dataset identifiers and invites projects to approve them before they are implemented.

:::note

The dataset identifier is used in the linked data creation process. It cannot be changed after LINCS publishes the data without breaking the applications that query the data.

:::

**Examples:** in named graphs, in URLs, in file names

The dataset identifier must be:

* Maximum 20 characters
* No spaces or underscores
* No capital letters (lowercase only)
* No special characters or accents 
