---
id: authority-record
title: Authority Record
definition: A stable, persistent Uniform Resource Identifier (URI) for a concept in the Linked Data (LD) ecosystem.
---

An authority record is a stable, persistent :Term[Uniform Resource Identifier (URI)]{#uniform-resource-identifier} for a concept in the :Term[Linked Data (LD)]{#linked-data} ecosystem. Authority records are usually aggregated in an :Term[authority file]{#authority-file}.
