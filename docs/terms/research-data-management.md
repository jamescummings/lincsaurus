---
id: research-data-management
title: Research Data Management (RDM)
definition: The processes and activities performed by researchers throughout the lifecycle of a research project to guide the collection, organization, documentation, storage, accessibility, reusability, and preservation of data.
---

Research Data Management (RDM) refers to the processes and activities performed by researchers throughout the lifecycle of a research project to guide the collection, organization, documentation, storage, access, reuse, and preservation of data. Proper RDM can help avoid research replication, promote the safety and security of data, improve the comparison and co-analysis of data derived from different sources, ensure reusability of datasets, and contribute to research integrity.

## Further Resources

- American Library Association (2023) [“Keeping Up With... Research Data Management”](https://www.ala.org/acrl/publications/keeping_up_with/rdm)
- Digital Research Alliance of Canada (2023) [“Research Data Management”](https://alliancecan.ca/en/services/research-data-management)
- Goodchild (2020, June 16). [“Data Curation Tool Scholars Portal Dataverse”](https://alliancecan.ca/sites/default/files/2022-05/Webinar_RDMTech_DataCuration.pdf) [PowerPoint]
- Government of Canada (2021, March 15) [“Tri-Agency Research Data Management Policy”](https://science.gc.ca/site/science/en/interagency-research-funding/policies-and-guidelines/research-data-management/tri-agency-research-data-management-policy)
- Scholars Portal (2019) [“RDM 101 – Module 4. Scholars Portal”](https://learn.scholarsportal.info/modules/portage/rdm-101-module-4/) [PowerPoint]
