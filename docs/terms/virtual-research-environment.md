---
id: virtual-research-environment
title: Virtual Research Environment (VRE)
definition: An online workspace that allows researchers to collaborate.
---

A Virtual Research Environment (VRE) is an online workspace that allows researchers to collaborate. VREs often include features such as wikis/forums, document hosting, and discipline-specific tools. They are particularly beneficial for research teams that span institutions and/or countries because they allow research information to be easily shared.

## Examples

- [LEAF VRE](/docs/tools/leaf-vre/)

## Further Resources

- OCLC (2015) [“Virtual Research Environment (VRE) Study (OCLC/JISC)”](https://www.oclc.org/research/areas/user-studies/vre.html)
- [Virtual Research Environment (Wikipedia)](https://en.wikipedia.org/wiki/Virtual_research_environment)
