---
id: persistent-identifier
title: Persistent Identifier (PID)
definition: A long-lasting reference to a document, file, web page, or other digital object.
---

A Persistent Identifier (PID) is a long-lasting reference to a document, file, web page, or other digital object. PIDs are unlike :Term[Uniform Resource Locators (URLs)]{#uniform-resource-locator} because they cannot break. They are usually provided by services that create identifiers that consistently point to the same digital object, despite its location moving over time.

The :Term[FAIR Principles]{#fair-principles} suggest using PIDs to meet the objectives of open science.

## Examples

- An :Term[ORCID]{#open-researcher-and-contributor-id} ID
- A :Term[Uniform Resource Identifier (URI)]{#uniform-resource-identifier} from the :Term[Virtual International Authority File (VIAF)]{#virtual-international-authority-file}
- A :Term[Digital Object Identifier (DOI)]{#digital-object-identifier}

## Further Resources

- Brown (2021) [“PIDs: What Do Researchers Need to Know?”](https://vimeo.com/525702819) [Video]
- Canadian Research Knowledge Network (2024) [“PIDs”](https://www.crkn-rcdr.ca/en/what-is-a-persistent-identifier-pid)
- CERN Scientific Information Service (2020) [“What are Persistent Identifiers?”](https://scientific-info.cern/submit-and-publish/persistent-identifiers/what-are-pids)
Meadows, Haak, & Brown (2019) [“Persistent Identifiers: the Building Blocks of the Research Information Infrastructure”](https://insights.uksg.org/articles/10.1629/uksg.457)
- Goddard (2020) [“Persistent Identifiers (PIDs) in Canada”](https://www.crkn-rcdr.ca/sites/crkn/files/2020-10/4_PIDs%20in%20Canada_0_Complete%20-%20EN.pdf)
- ORCID (2024) ["What are Persistent Identifiers (PIDs)?"](https://support.orcid.org/hc/en-us/articles/360006971013-What-are-persistent-identifiers-PIDs)
- [Persistent Identifier (Wikipedia)](https://en.wikipedia.org/wiki/Persistent_identifier)
