---
id: qname
title: QName
definition: A shorthand string that is used to substitute a Uniform Resource Identifier (URI) reference.
---

A QName, or “qualified name,” is a shorthand string that is used as a substitute for a :Term[Uniform Resource Identifier (URI)]{#uniform-resource-identifier} reference. QNames are made up of a prefix that has been assigned to a :Term[namespaces]{#namespace} URI, a colon, and a local name. QNames allow URIs to be written out in a more readable format.

## Examples

- `crm:P2_has_type` is a QName for `http://www.cidoc-crm.org/cidoc-crm/P2_has_type`, given that the prefix `crm` is defined as using the namespace URI `http://www.cidoc-crm.org/cidoc-crm/`

## Further Resources

- [QName (Wikipedia)](https://en.wikipedia.org/wiki/QName)
- W3C (2009) [_Namespaces in XML 1.0 (Third Edition)_](https://www.w3.org/TR/xml-names/)
- W3C (2014) [_RDF 1.1 XML Syntax_](https://www.w3.org/TR/rdf-syntax-grammar/#section-Syntax-intro)
