---
id: relation-extraction
title: Relation Extraction (RE)
definition: The task of detecting, classifying, and extracting semantic relationships from a text.
---

Relationship Extraction (RE) is the task of detecting, classifying, and extracting semantic relationships from a text. These relationships tend to occur between two or more :Term[entities]{#entity} of a certain type (e.g., person, organization, place) and can be denoted using :Term[triples]{#triple}. RE is one of the techniques of :Term[Natural Language Processing (NLP)]{#natural-language-processing}.

## Further Resources

- Deep Dive (2023) [“Relation Extraction”](http://deepdive.stanford.edu/relation_extraction)
- NLP Progress (2023). [“Relation Extraction”](https://nlpprogress.com/english/relationship_extraction.html)
- [Relation Extraction (Wikipedia)](https://en.wikipedia.org/wiki/Relationship_extraction)
