---
id: node
title: Node
definition: A representation of an entity or instance to be tracked in a graph database or triplestore, such as a person, object, organization, or event.
---

A node represents an entity or instance to be tracked in a :Term[graph database]{#graph-database} or :Term[triplestore]{#triplestore}, such as a person, object, or organization. Nodes are connected by :Term[edges]{#edge} and information about a node is called a property.

## Examples

- The following image shows a :Term[Resource Description Framework (RDF)]{#resource-description-framework} graph highlighting the edges that connect three nodes.

![alt=""](</img/documentation/glossary-node-example-(c-LINCS).jpg>)
