---
id: tei-data
title: TEI Data
definition: Data that follows the guidelines of the Text Encoding Initiative (TEI).
---

TEI data is data that follows the guidelines of the :Term[Text Encoding Initiative (TEI)]{#text-encoding-initiative}.

## Examples

- The following example shows TEI data about the person Joan Braderman:

```xml
<person xml:id="h-nr254">
   <idno type="Wikidata">http://www.wikidata.org/entity/Q28911659</idno>
   <persName>
      <name>Braderman, Joan</name>
   </persName>
   <persName type="preferred">
      <forename>Joan</forename>
      <surname>Braderman</surname>
   </persName>
   <floruit>
      <date>1977</date>
   </floruit>
   <occupation cert="high">Film maker</occupation>
   <affiliation cert="high">Heresies Collective</affiliation>
</person>
```
