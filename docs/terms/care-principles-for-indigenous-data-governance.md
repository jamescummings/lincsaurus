---
id: care-principles-for-indigenous-data-governance
title: CARE Principles for Indigenous Data Governance
definition: A set of principles (collective benefit, authority to control, responsibility, and ethics) to advance collective and individual data rights in the open data movement.
---

The [CARE Principles for Indigenous Data Governance](https://www.gida-global.org/s/dsj-1158_carroll.pdf) are a set of principles to advance collective and individual data rights in the context of the [United Nations Declaration on the Rights of Indigenous Peoples (UNDRIP)](https://www.un.org/development/desa/indigenouspeoples/wp-content/uploads/sites/19/2018/11/UNDRIP_E_web.pdf) and the open data movement: collective benefit, authority to control, responsibility, and ethics.

Current open data principles, such as :Term[FAIR Principles]{#fair-principles}, ignore power differentials and historical contexts. The CARE principles were developed by the International Indigenous Data Sovereignty Interest Group to encourage data movements to ensure Indigenous governance over data is respected.

CARE stands for:

- **Collective Benefit:** Data systems must be designed to enable equitable outcomes for Indigenous peoples.
- **Authority to Control:** Indigenous peoples’ rights and interests over their data must be recognized and their authority over their data must be empowered.
- **Responsibility:** People who work with Indigenous data must develop positive and respectful relationships and ensure data are being used to support Indigenous peoples’ self-determination.
- **Ethics:** Indigenous peoples’ rights and wellbeing must be the main concern in data governance.

## Further Resources

- Australian Research Data Commons (2024) [“CARE Principles”](https://ardc.edu.au/resource/the-care-principles/)
- [CARE Principles for Indigenous Data Governance (Wikipedia)](https://en.wikipedia.org/wiki/CARE_Principles_for_Indigenous_Data_Governance)
- Global Indigenous Data Alliance (2024) ["CARE Principles for Indigenous Data Governance"](https://www.gida-global.org/care)
- Research Data Alliance International Indigenous Data Sovereignty Interest Group (2019) ["CARE Principles for Indigenous Data Governance"](https://static1.squarespace.com/static/5d3799de845604000199cd24/t/6397b363b502ff481fce6baf/1670886246948/CARE%2BPrinciples_One%2BPagers%2BFINAL_Oct_17_2019.pdf)
- Russo Carroll, et al. (2020) [“The CARE Principles for Indigenous Data Governance”](https://datascience.codata.org/articles/10.5334/dsj-2020-043)
- St. Lawrence Global Observatory (2024) [“CARE Principles”](https://www.ogsl.ca/en/care-principles/)
