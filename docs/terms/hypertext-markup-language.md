---
id: hypertext-markup-language
title: HyperText Markup Language (HTML)
definition: The standard markup language for web pages.
---

HyperText Markup Language (HTML) is the standard markup language for web pages. It is used to semantically describe the structure of documents to allow browsers to render them into multimedia web content.

HTML is often used in conjunction with Cascading Style Sheets (CSS) (to affect the layout of content and webpages) and JavaScript (to affect the behaviour of content and webpages).

## Further Resources

- [HTML (Wikipedia)](https://en.wikipedia.org/wiki/HTML)
- Glass & Boucheron (2024) [“How To Build a Website with HTML”](https://www.digitalocean.com/community/tutorial-series/how-to-build-a-website-with-html) [Tutorial]
- MDN Web Docs (2024) [“HTML: HyperText Markup Language”](https://developer.mozilla.org/en-US/docs/Web/HTML)
- W3Schools (2014) [“HTML Responsive Web Design”](https://www.w3schools.com/html/html_responsive.asp)
- W3Schools (2024) [“HTML Tutorial”](https://www.w3schools.com/html/) [Tutorial]
