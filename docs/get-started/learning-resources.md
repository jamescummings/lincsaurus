---
title: Learning Resources
sidebar_position: 1
description: "Explore different Linked Open Data (LOD) resources"
---

LINCS has created and collected a variety of resources of different levels and formats to help users learn about :Term[Linked Open Data (LOD)]{#linked-open-data}.

## Zotero Library

The [LINCS Zotero Library](https://www.zotero.org/groups/5145265/lincs-project/library) contains citations and links to articles, books, conference presentations, videos, tutorials, and websites on LOD topics, ranging from introductory to advanced. The resources have been divided by topic. Tags have been used to denote the following:

- **Favourite:** Resources that are LINCS’s favourite
- **Introductory Material:** Resources that are ideal for beginners
- **Training Module:** Resources that are interactive
- **LINCS Output:** Resources that have been created by LINCS and/or the project’s collaborators

[Contact LINCS](mailto:lincs@uoguelph.ca) to share resources you would like to add to the Zotero library or to request library editing privileges.

## Linked Open Data Basics

The [Linked Open Data Basics](/docs/get-started/linked-open-data-basics) pages explain the foundational concepts of LOD. These pages are ideal for beginners or those wanting to brush up on their understanding of LOD.

## Glossary

The [Glossary](/docs/get-started/glossary) contains explanations of terms are used in LINCS’s documentation and other LOD resources. Clicking on a term brings up a longer definition with examples and links to further resources.

## YouTube

[LINCS’s YouTube](https://www.youtube.com/@lincsproject/featured) features video tutorials and conference presentations on LOD topics and [LINCS’s tools](/docs/tools). Subscribe to the channel to be notified of new content!

## Events

LINCS holds many events, including tutorials and conferences. See the [Events](/docs/about-lincs/get-involved/events) page to learn about past and upcoming events.
