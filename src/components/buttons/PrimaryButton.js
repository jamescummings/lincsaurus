/** @format */

import React from "react";
import Link from "@docusaurus/Link";
import styles from "./buttons.module.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUpRightFromSquare } from "@fortawesome/free-solid-svg-icons";

function PrimaryButton({ link, buttonName, icon = null }) {
  const title = typeof buttonName == "object" ? buttonName.props.children : buttonName;
  icon = icon ? icon : link.includes("http") ? faUpRightFromSquare : null; // if no icon is provided, check if the link is external and add an external link icon

  if (icon) {
    return (
      <Link className={styles.primaryButton} to={link} title={title}>
        {buttonName}
        <div className={styles.iconShift}>{icon ? <FontAwesomeIcon icon={icon} /> : null}</div>
      </Link>
    );
  }

  return (
    <Link className={styles.primaryButton} title={title} to={link}>
      {buttonName}
      {icon ? <FontAwesomeIcon icon={icon} /> : null}
    </Link>
  );
}

export default PrimaryButton;
