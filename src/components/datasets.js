/** @format */

import React from "react";
import Translate from "@docusaurus/Translate";
import adarchiveImage from "@site/static/img/documentation/project-dataset-adarchive-logo-(cc0).jpg";
import anthologiagraecaImage from "@site/static/img/documentation/project-dataset-anthologia-graeca-logo-(cc0).jpg";
import ethnomusicologyImage from "@site/static/img/documentation/project-dataset-ethnomusicology-logo-(cc0).png";
import histsexImage from "@site/static/img/documentation/project-dataset-histsex-logo-(cc0).jpg";
import moemlImage from "@site/static/img/documentation/project-dataset-moeml-logo-(cc0).png";
import orlandoImage from "@site/static/img/documentation/project-dataset-orlando-logo-(cc0).png";
import usaskImage from "@site/static/img/documentation/project-dataset-usask-art-logo-(cc0).png";
import yellowNinetiesImage from "@site/static/img/documentation/project-dataset-Y90s-banner-(c-public-domain).png";

const datasets = {
  moeml: {
    published: false,
    page: "/docs/explore-data/project-datasets/moeml/",
    src: moemlImage,
    title: (
      <Translate id="dataset.moeml.title" description="Map of Early Modern London dataset title">
        MoEML
      </Translate>
    ),
    description: (
      <Translate
        id="dataset.moeml.description.long"
        description="Map of Early Modern London dataset description"
      >
        Map of Early Modern London maps the spatial imaginary of Shakespeare’s city by asking how
        London’s spaces and places were named, traversed, used, repurposed, and contested by various
        practitioners, writers, and civic officials. MoEML’s maps allow us to plot people,
        historical documents, literary works, and recent critical research onto topography and the
        built environment. An early contributor to the spatial turn and literary Geographic
        Information Systems (GIS), MoEML also provides a virtual space for exploring the meaning and
        representation of cultural space in the London of Shakespeare and his contemporaries.
      </Translate>
    ),
    shortDescription: (
      <Translate
        id="dataset.moeml.description.short"
        description="Map of Early Modern London dataset description"
      >
        Map of Early Modern London maps the spatial imaginary of Shakespeare’s city by asking how
        London’s spaces and places were named, traversed, used, and more.
      </Translate>
    ),
    buttonName1: (
      <Translate
        id="dataset.moeml.exploreButtonLabel"
        description="Map of Early Modern London dataset button label"
      >
        Explore Map of Early Modern London
      </Translate>
    ),
    link1: "https://rs.lincsproject.ca/resource/ThinkingFrames?view=search-moeml",
  },
  "usask-art": {
    published: true,
    page: "/docs/explore-data/project-datasets/usask-art/",
    src: usaskImage,
    title: (
      <Translate id="dataset.usask-art.title" description="USask Art dataset title">
        USask Art
      </Translate>
    ),
    description: (
      <Translate id="dataset.usask.description.long" description="USask Art dataset description">
        The University of Saskatchewan Art Collection is a collection of over 6,000 works, spanning
        many art movements, styles, subjects, and media. Initially guided by the vision of its first
        president, Walter Murray, the collection contains works by Group of Seven painters Arthur
        Lismer and Lawren Harris; works by Saskatchewan based artists Frederick Loveroff, James
        Henderson, and Ernest Lindner; important early Canadian works; modernist North American and
        European works; Inuit and Indigenous art objects; and prairie folk art.
      </Translate>
    ),
    shortDescription: (
      <Translate
        id="dataset.usask.description.short"
        description="USask Art dataset short description"
      >
        The University of Saskatchewan Art Collection is a collection of over 6,000 works, spanning
        many art movements, styles, subjects, and media.
      </Translate>
    ),
    buttonName1: (
      <Translate id="dataset.usask.exploreButtonLabel" description="USask Art dataset button label">
        Explore USask Art
      </Translate>
    ),
    link1: "https://rs.lincsproject.ca/resource/ThinkingFrames?view=search-usaskart",
  },
  AdArchive: {
    published: true,
    page: "/docs/explore-data/project-datasets/adarchive/",
    src: adarchiveImage,
    imageCredits: "Heresies 4 Diana Press Publications advertisement",
    title: (
      <Translate id="dataset.adarchive.title" description="AdArchive dataset title">
        AdArchive
      </Translate>
    ),
    description: (
      <Translate
        id="dataset.adarchive.description.long"
        description="AdArchive dataset description"
      >
        AdArchive expands feminist periodical scholarship with an innovative focus on advertisements
        from feminist-identified journals. To date, there is limited scholarship exploring
        advertisements in social movement periodicals and minimal exploration of how advertisements
        functioned to establish and sustain networks among publications, publishers, readers, and
        social movement organizations.
      </Translate>
    ),
    shortDescription: (
      <Translate
        id="dataset.adarchive.description.short"
        description="AdArchive dataset description"
      >
        AdArchive expands feminist scholarship with an innovative focus on advertisements from
        feminist-identified journals.
      </Translate>
    ),
    buttonName1: (
      <Translate id="dataset.adarchive.exploreButtonLabel" description="Explore Dataset label">
        Explore AdArchive
      </Translate>
    ),
    link1: "https://rs.lincsproject.ca/resource/ThinkingFrames?view=search-adarchive",
    entityLink:
      "https://rs.lincsproject.ca/resource/ThinkingFrames?uri=http://id.lincsproject.ca/LRK8dZhmUxZ",
  },
  "Anthologia graeca": {
    published: true,
    page: "/docs/explore-data/project-datasets/anthologia-graeca/",
    src: anthologiagraecaImage,
    entityLink: "https://digi.ub.uni-heidelberg.de/diglit/cpgraec23/0079/image,info", //preferred to have an RS link
    // "https://rs.stage.lincsproject.ca/resource/ThinkingFrames?uri=http://temp.lincsproject.ca/anthologie/manuscript/1",
    imageCredits: "Codex palatinus graecus 23, page 79 (first page of the palatine anthology)",
    altText: "Cod. Pal. graec. 23, p. 79",
    title: (
      <Translate id="dataset.ap.title" description="Anthologia graeca dataset title">
        Anthologia graeca
      </Translate>
    ),
    description: (
      <Translate
        id="dataset.ap.description.long"
        description="Anthologia graeca dataset description"
      >
        Anthologia graeca gathers information and data on the Greek Anthology: more than 4000 pieces
        of Greek epigrammatic poetry from the classical to the Byzantine period written by 325
        different authors. The platform includes different versions of the Greek texts, various
        translations of the texts into different languages, notes, commentaries, information on the
        authors, images of the codex (the palatinus graecus 23), keywords, and places cited.
      </Translate>
    ),
    shortDescription: (
      <Translate
        id="dataset.ap.description.short"
        description="Anthologia graeca dataset short description"
      >
        Anthologia graeca gathers information and data on the Greek Anthology: more than 4000 pieces
        of Greek epigrammatic poetry from the classical to the Byzantine period.
      </Translate>
    ),
    buttonName1: (
      <Translate id="dataset.ap.exploreButtonLabel" description="Explore Dataset label">
        Explore Anthologia graeca
      </Translate>
    ),
    link1:
      "https://rs.stage.lincsproject.ca/resource/ThinkingFrames?view=search-anthologiaPalatina",
  },
  Y90s: {
    page: "/docs/explore-data/project-datasets/yellow-nineties/",
    src: yellowNinetiesImage,
    title: (
      <Translate id="dataset.yn.title" description="Yellow Nineties dataset title">
        Yellow Nineties 2.0
      </Translate>
    ),
    description: (
      <Translate
        id="dataset.yn.description.long"
        description="Yellow Nineties 2.0 dataset description"
      >
        Yellow Nineties 2.0 uses digital tools to advance knowledge of eight late-Victorian little
        magazines and the people who contributed to their production between 1889 and 1905. Each
        digital edition is accompanied by an editorial overview of the magazine as a whole and
        includes a critical introduction to every volume in its print run.
      </Translate>
    ),
    shortDescription: (
      <Translate
        id="dataset.yn.description.short"
        description="Yellow Nineties 2.0 dataset description"
      >
        Yellow Nineties 2.0 uses digital tools to advance knowledge of eight late-Victorian little
        magazines and the people who contributed to their production between 1889 and 1905.
      </Translate>
    ),
    buttonName1: (
      <Translate
        id="dataset.ynExploreButtonLabel"
        description="Explore Yellow Nineties button label"
      >
        Explore Yellow Nineties
      </Translate>
    ),
    link1: "https://rs.stage.lincsproject.ca/resource/ThinkingFrames?view=search-yellowNineties",
  },
  Orlando: {
    page: "/docs/explore-data/project-datasets/orlando/",
    src: orlandoImage,
    title: (
      <Translate id="dataset.orlandoTitle" description="Orlando dataset title">
        Orlando
      </Translate>
    ),
    description: (
      <Translate id="dataset.orlando.description.long" description="Orlando dataset description">
        The Orlando Project explores and harnesses the power of digital tools and methods to advance
        feminist literary scholarship.
      </Translate>
    ),
    shortDescription: (
      <Translate id="dataset.orlando.description.short" description="Orlando dataset description">
        The Orlando Project explores and harnesses the power of digital tools and methods to advance
        feminist literary scholarship.
      </Translate>
    ),
    buttonName1: (
      <Translate id="dataset.orlando.ExploreButtonLabel" description="Explore Orlando label">
        Explore Orlando
      </Translate>
    ),
    link1: "https://rs.stage.lincsproject.ca/resource/ThinkingFrames?view=search-orlando",
  },
  HistSex: {
    page: "/docs/explore-data/project-datasets/histsex/",
    src: histsexImage,
    title: (
      <Translate id="dataset.yn.title" description="Histsex dataset title">
        Histsex
      </Translate>
    ),
    description: (
      <Translate id="dataset.yn.description.long" description="HistSex dataset description">
        HistSex is a freely-available, peer reviewed resource for information on the history of
        sexuality developed by sex educators, historians, and librarians. It includes a descriptive
        catalog of relevant digital projects on the history of sexuality; a searchable and annotated
        directory of LGBTQ+ research archives; a timeline of major events in the history of
        sexuality; and a bibliography of books tagged and searchable by research interest, reading
        level, topic, and more—all presented online in an open and easily accessible format.
      </Translate>
    ),
    shortDescription: (
      <Translate id="dataset.yn.description.short" description="HistSex dataset description">
        HistSex is a freely-available, peer reviewed resource for information on the history of
        sexuality developed by sex educators, historians, and librarians.
      </Translate>
    ),
    buttonName1: (
      <Translate id="dataset.ynExploreButtonLabel" description="Explore HistSex button label">
        Explore HistSex
      </Translate>
    ),
    link1: "https://rs.stage.lincsproject.ca/resource/ThinkingFrames?view=search-histSex",
  },
  Ethnomusicology: {
    page: "/docs/explore-data/project-datasets/ethnomusicology/",
    src: ethnomusicologyImage,
    title: (
      <Translate id="dataset.yn.title" description="Ethnomusicology dataset title">
        Ethnomusicology
      </Translate>
    ),
    description: (
      <Translate id="dataset.yn.description.long" description="Ethnomusicology dataset description">
        The Canadian Centre for Ethnomusicology is an archive and research resource documenting
        musical and cultural traditions locally and internationally. The collection includes diverse
        instruments and more than 4000 titles in audio/video recordings. The Centre helps users
        understand how people use music to connect, express, and create community and identity. It
        is of value to students and faculty in the social sciences, humanities, education, and fine
        arts.
      </Translate>
    ),
    shortDescription: (
      <Translate
        id="dataset.yn.description.short"
        description="Ethnomusicology dataset description"
      >
        The Canadian Centre for Ethnomusicology is an archive and research resource documenting
        musical and cultural traditions locally and internationally.
      </Translate>
    ),
    buttonName1: (
      <Translate
        id="dataset.ynExploreButtonLabel"
        description="Explore Ethnomusicology button label"
      >
        Explore Ethnomusicology
      </Translate>
    ),
    link1: "https://rs.stage.lincsproject.ca/resource/ThinkingFrames?view=search-ethnomusicology",
  },
};

export default datasets;
