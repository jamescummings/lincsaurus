import React from "react";
import { useLocation } from "react-router-dom";
import Translate from "@docusaurus/Translate";
import PrimaryButton from "./buttons/PrimaryButton";
import terms from "@site/static/glossary-en.json";
import frenchTerms from "@site/static/glossary-fr.json";
import styles from "./homepage.module.css";
import seedrandom from "seedrandom";

const getRandomTerm = (terms, rng) => {
  const keys = Object.keys(terms);
  const key = keys[(keys.length * Math.random()) << 0];
  terms[key].id = key;
  return terms[key];
};

function HomepageTOD({ buttonName1, link1, buttonName2 }) {
  const currentDate = new Date().toISOString().slice(0, 10);

  // Use currentDate as the seed for seedrandom()
  const rng = seedrandom(currentDate);

  const randomTerm = getRandomTerm(terms, rng);
  const randomFrenchTerm = getRandomTerm(frenchTerms, rng);
  const location = useLocation();

  if (location.pathname == "/fr/") {
    return (
      <div className={styles.termOfTheDayContainer}>
        <div className={styles.termOfTheDay}>
          <h2>
            <Translate id="homepage.tod.todHeader" description="LINCS Term of the Day header">
              LINCS Term of the Day:
            </Translate>{" "}
            {randomFrenchTerm.title}
          </h2>
          <p>{randomFrenchTerm.definition}</p>

          <div className="tod-primary-button-row">
            <PrimaryButton link={`/docs/terms/${randomFrenchTerm.id}`} buttonName={buttonName1} />
            <PrimaryButton link={link1} buttonName={buttonName2} />
          </div>
        </div>
      </div>
    );
  }

  return (
    <div className={styles.termOfTheDayContainer}>
      <div className={styles.termOfTheDay}>
        <h2>
          <Translate id="homepage.tod.todHeader" description="LINCS Term of the Day header">
            LINCS Term of the Day:
          </Translate>{" "}
          {randomTerm.title}
        </h2>
        <p>{randomTerm.definition}</p>

        <div className="tod-primary-button-row">
          <PrimaryButton link={`/docs/terms/${randomTerm.id}`} buttonName={buttonName1} />
          <PrimaryButton link={link1} buttonName={buttonName2} />
        </div>
      </div>
    </div>
  );
}

export default HomepageTOD;
