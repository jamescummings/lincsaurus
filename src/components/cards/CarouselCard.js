/** @format */

import React from "react";
import styles from "./cards.module.css";
import PrimaryButton from "../buttons/PrimaryButton";
import EntityButton from "../buttons/EntityButton";
// import ToolTip from "../toolTip";

function EntityLink({ entityLink, imageCredits }) {
  if (imageCredits) {
    return (
      <div className={styles.entity}>
        {entityLink ? (
          //<ToolTip popup={imageCredits}>
          <EntityButton link={entityLink} />
        ) : //</ToolTip>
        null}
      </div>
    );
  } else {
    return (
      <div className={styles.entity}>{entityLink ? <EntityButton link={entityLink} /> : null}</div>
    );
  }
}

function CarouselCard({
  src,
  title,
  description,
  buttonName1,
  link1,
  buttonName2,
  link2,
  entityLink,
  imageCredits,
  altText = "",
  published,
}) {
  return (
    <div className={styles.slide}>
      <div className={styles.carouselBackground}>
        <div className={styles.carouselContent}>
          <h1>{title}</h1>
        </div>

        <div className={styles.carouselImage}>
          {src ? <img src={src} alt={altText}></img> : <div className={styles.rectangle}></div>}
          <EntityLink entityLink={entityLink} imageCredits={imageCredits} />
        </div>

        <div className={styles.carouselDescription}>
          <p>{description}</p>
        </div>

        <div className="carousel-primary-button-row">
          {published ? <PrimaryButton link={link1} buttonName={buttonName1} /> : null}
          {link2 ? <PrimaryButton link={link2} buttonName={buttonName2} /> : null}
        </div>
      </div>
    </div>
  );
}

export default CarouselCard;
