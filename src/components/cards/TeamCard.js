/** @format */

import React from "react";
import styles from "./cards.module.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";
import { faGitlab, faGithub, faOrcid } from "@fortawesome/free-brands-svg-icons";
import { faIdCard } from "@fortawesome/free-solid-svg-icons";
library.add(faOrcid, faIdCard, faGithub, faGitlab);

function containsAccents(str) {
  // This regex matches characters outside the standard ASCII range (0-127)
  // It's a broad check that may include more than just accents.
  return /[^\u0000-\u007f]/.test(str);
}

function removeAccents(str) {
  return str.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
}

const anonymousImage = "anonymous-(c-unknown-icon)";

function sanitizeImageFilename(src) {
  if (src != anonymousImage && containsAccents(src)) {
    return removeAccents(src);
  }
  return src;
}

function TeamCard({
  firstName,
  lastName,
  imageExists,
  uni,
  jobTitle,
  website,
  gitlab,
  github,
  orcid,
  start,
  end,
}) {
  const filename = imageExists ? `${firstName}${lastName}-(c-LINCS)` : anonymousImage;
  const cleanFilename = sanitizeImageFilename(filename);

  const startDate = start || `2020`;
  const endDate = end || ``;
  const date = `${startDate}–${endDate}`;

  return (
    <div className={styles.card} id={`${firstName}${lastName}`}>
      <img
        className="avatar__photo avatar__photo--xl"
        alt=""
        src={`/img/staff/${cleanFilename}.png`}
      />

      <div className={styles["team-card-body"]}>
        <div className="avatar__intro">
          <div className="avatar__name">
            <h4 className="avatar__subtitle">
              {firstName} {lastName}
            </h4>
          </div>
          {jobTitle ? <h4 className="avatar__subtitle">{jobTitle}</h4> : null}
          <h5>{date}</h5>
        </div>
      </div>

      <div className={styles["team-card-footer"]}>
        <small className="avatar__subtitle">{uni}</small>

        <div className={styles["link-row"]}>
          {website && (
            <a href={website} title="Personal Site">
              <FontAwesomeIcon icon={faIdCard} />
            </a>
          )}
          {orcid && (
            <a href={orcid} title="Orcid ID">
              <FontAwesomeIcon icon={faOrcid} />
            </a>
          )}
          {gitlab && (
            <a href={gitlab} title="GitLab Profile">
              <FontAwesomeIcon icon={faGitlab} />
            </a>
          )}
          {github && (
            <a href={github} title="Github Profile">
              <FontAwesomeIcon icon={faGithub} />
            </a>
          )}
        </div>
      </div>
    </div>
  );
}

export default TeamCard;
