import React, { useState } from 'react';
import styles from "./cards.module.css";
import TileButton from '../buttons/TileButton';

function TileCard({ src, title, description, link, buttonName }) {
    const [content, setContent] = useState(false);

    function handleMouseEnter() {
        setContent(true);
    }

    function handleMouseLeave() {
        setContent(false);
    }

    return (
      <div
        onMouseEnter={handleMouseEnter}
        onMouseLeave={handleMouseLeave}
        className={styles.tile}>
        {content ? (
          <img src={src} alt="" className={styles.tileBackgroundOnHover} />
        ) : (
          <img src={src} alt="" className={styles.tileBackground} />
        )}

        {content ? (
          <div className={styles.tileContentOnHover}>
            <h1>{title}</h1>
            <p>{description}</p>
            <div className="tile-primary-button-row">
              <TileButton link={link} buttonName={buttonName} />
            </div>
          </div>
        ) : (
          <div className={styles.tileContent}>
            <h1>{title}</h1>
          </div>
        )}
      </div>
    );
}

export default TileCard;