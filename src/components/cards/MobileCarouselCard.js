/** @format */

import React from "react";
import styles from "./cards.module.css";
import PrimaryButton from "../buttons/PrimaryButton";
import MobileEntityButton from "../buttons/MobileEntityButton";

function MobileCarouselCard({
  src,
  title,
  description,
  buttonName1,
  link1,
  buttonName2,
  link2,
  altText = "",
  entityLink,
  published,
}) {
  return (
    <div className={styles.background}>
      <div className={styles.mobileCarouselContent}>
        <h2>{title}</h2>
      </div>

      <div className={styles.entityContainer}>
        {src ? (
          <img className={styles.mobileCarouselImage} src={src} alt={altText} />
        ) : (
          <div className={styles.mobileRectangle}></div>
        )}

        <div className={styles.mobileEntity}>
          {entityLink ? <MobileEntityButton link={entityLink} /> : null}
        </div>
      </div>

      <div className={styles.mobileCarouselDescription}>
        <p>{description}</p>
      </div>

      <div className="mobile-carousel-primary-button-row">
        {published ? <PrimaryButton link={link1} buttonName={buttonName1} /> : null}
        {link2 ? <PrimaryButton link={link2} buttonName={buttonName2} /> : null}
      </div>
    </div>
  );
}

export default MobileCarouselCard;
