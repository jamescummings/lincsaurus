import React from 'react';
import styles from "./cards.module.css";

function NewsletterCard({ src, alt, date, link }) {
    return (
        <div className={styles.newsletterCard}>
            <a href={link}>
                <img src={src} alt={alt} />
            </a>

            <h3>{date}</h3>
        </div>
    );
}

export default NewsletterCard;