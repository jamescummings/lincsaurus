/** @format */

import React from "react";
import styles from "./cards.module.css";
import PrimaryButton from "../buttons/PrimaryButton";
import EntityButton from "../buttons/EntityButton";
import Link from "@docusaurus/Link";

function DatasetCard({
  src,
  title,
  shortDescription,
  buttonName1,
  page,
  link1,
  entityLink,
  altText = "",
}) {

  return (
    <div className={styles.datasetCardContainer}>
      {entityLink ? (
        <div className={styles.datasetEntity}>
          <EntityButton link={entityLink} />
        </div>
      ) : null}

      <Link to={page} className={styles.DatasetCardLink}>
        <div className={styles.cardTitle}>
          <h2 title={title}>{title}</h2>
        </div>

        <img src={src} alt={altText} className={styles.cardImage} />

        <div className={styles.staticCardDescription}>
          <p title={shortDescription.props.children}>{shortDescription}</p>
        </div>
      </Link>
    </div>
  );
}

export default DatasetCard;
