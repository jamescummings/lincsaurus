/** @format */

import React from "react";
import datasets from "../datasets";
import DatasetCard from "../cards/DatasetCard";
import GridLayout from "../layouts/GridLayout";

const sortedDatasets = Object.keys(datasets).sort((a, b) => {
  const propertyA = datasets[a].page;
  const propertyB = datasets[b].page;

  // Compare the properties for sorting
  if (propertyA < propertyB) {
    return -1;
  }
  if (propertyA > propertyB) {
    return 1;
  }
  return 0;
});

function DatasetList() {
  return (
    <GridLayout maxColumns={2}>
      {sortedDatasets.map((x) => (
        <DatasetCard key={x} {...datasets[x]}></DatasetCard>
      ))}
    </GridLayout>
  );
}

export default DatasetList;
