import eventData from "../events";
import StaticCard from "../cards/StaticCard";
import GridLayout from "../layouts/GridLayout";

const filterTypes = ["All", "Upcoming", "Past", "Ongoing"];

function filterEvents(filterType) {
  const currentDate = new Date();

  const filteredEvents = Object.entries(eventData).filter(([key, event]) => {
    switch (filterType) {
      case "All":
        return true;
      case "Upcoming":
        return new Date(event.endDate) > currentDate;
      case "Past":
        return new Date(event.endDate) < currentDate;
      case "Ongoing":
        return new Date(event.startDate) <= currentDate && new Date(event.endDate) >= currentDate;
      default:
        return true;
    }
  });

  // Convert the filtered entries back into an object
  return Object.fromEntries(filteredEvents);
}

function EventList({ filter = "All" }) {
  const filteredEvents = filterEvents(filter);

  return (
    <GridLayout>
      {Object.keys(filteredEvents).map((x) => (
        <StaticCard
          key={x}
          {...filteredEvents[x]}
          src={filteredEvents[x]["image"]}
          link1={filteredEvents[x]["link"]}
          imageFit="contain"
        ></StaticCard>
      ))}
    </GridLayout>
  );
}

export { EventList, filterEvents };