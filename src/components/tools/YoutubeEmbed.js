import LiteYouTubeEmbed from "react-lite-youtube-embed";
import "react-lite-youtube-embed/dist/LiteYouTubeEmbed.css";


export default function YoutubeEmbed({ id, title, playlist=false}) {
  return (
    <div className="video-container">
      <LiteYouTubeEmbed
        id={id}
        params="autoplay=1&autohide=1&showinfo=0&rel=0"
        title={title}
        poster="maxresdefault"
        playlist={playlist}
        webp
      />
    </div>
  );
}