/** @format */

import React from "react";
import Translate from "@docusaurus/Translate";

import { Navigation, Pagination, A11y, Keyboard } from "swiper/modules";

import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css/scrollbar";
import "swiper/css/keyboard";

import styles from "./homepage.module.css";
import orlandoImage from "@site/static/img/index-page-images/orlando.png";
import yellowNinetiesImage from "@site/static/img/documentation/project-dataset-Y90s-banner-(c-public-domain).png";
import CarouselCard from "./cards/CarouselCard";
import MobileCarouselCard from "./cards/MobileCarouselCard";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronRight, faChevronLeft } from "@fortawesome/free-solid-svg-icons";

const carouselContents = [
  {
    published: true,
    id: "University of Saskatchewan Art Collection",
    src: "https://saskcollections.org/kenderdine/media/sask_kenderdine/images/1/2/5/33668_ca_object_representations_media_12521_page.jpg",
    imageCredits:
      "Joi T. Arcand, ē-kī-nōhtē-itakot opwātisimowiskwēw (she used to want to be a fancy dancer), 2019, neon and associated hardware, 53 x 596 x 6 cm. Collection of the University of Saskatchewan. Purchase, 2019. Photograph by Carey Shaw.",
    title: (
      <Translate
        id="homepage.carousel.usaskTitle"
        description="University of Saskatchewan Art Collection carousel title"
      >
        University of Saskatchewan Art Collection
      </Translate>
    ),
    description: (
      <Translate
        id="homepage.carousel.usaskDescription"
        description="University of Saskatchewan Art Collection carousel description"
      >
        The University of Saskatchewan Art Collection is a collection of over 6,000 works, spanning
        many art movements, styles, subjects, and media.
      </Translate>
    ),
    buttonName1: (
      <Translate id="homepage.carousel.edLabel" description="Carousel Explore Dataset label">
        Explore USask Art
      </Translate>
    ),
    link1: "https://rs.lincsproject.ca/resource/ThinkingFrames?view=search-usaskart",
    buttonName2: (
      <Translate id="homepage.carousel.edLabel" description="Carousel Learn Dataset label">
        Learn about USask Art
      </Translate>
    ),
    link2: "/docs/explore-data/project-datasets/usask-art/",

    entityLink:
      "https://rs.lincsproject.ca/resource/ThinkingFrames?uri=https://saskcollections.org/kenderdine/Detail/objects/6842",
  },
  {
    published: true,
    id: "ad-archive",
    src: "https://iiif.archivelab.org/iiif/heresies_04$127/1308,1791,1067,1362/full/0/default.jpg",
    imageCredits: "Heresies 4 Diana Press Publications advertisement",
    title: (
      <Translate id="homepage.carousel.adarchiveTitle" description="AdArchive carousel title">
        AdArchive: Tracing Pre-­Digital Networked Feminisms
      </Translate>
    ),
    description: (
      <Translate
        id="homepage.carousel.adarchiveDescription"
        description="AdArchive carousel description"
      >
        AdArchive assembles and visualizes records of advertisements from feminist-identified
        journals produced in English between 1977 and 1992.
      </Translate>
    ),
    buttonName1: (
      <Translate id="homepage.carousel.edLabel" description="Carousel Explore Dataset label">
        Explore AdArchive
      </Translate>
    ),
    link1: "https://rs.lincsproject.ca/resource/ThinkingFrames?view=search-adarchive",
    buttonName2: (
      <Translate id="homepage.carousel.edLabel" description="Carousel Learn Dataset label">
        Learn about AdArchive
      </Translate>
    ),
    link2: "docs/explore-data/project-datasets/adarchive/",

    entityLink:
      "https://rs.lincsproject.ca/resource/ThinkingFrames?uri=http://id.lincsproject.ca/LRK8dZhmUxZ",
  },
  {
    id: "Anthologia graeca",
    published: true,
    src: "https://digi.ub.uni-heidelberg.de/iiif/2/cpgraec23:079.jpg/pct:0,0,100,100/full/0/default.jpg",
    entityLink: "https://digi.ub.uni-heidelberg.de/diglit/cpgraec23/0079/image,info", //preferred to have an RS link
    // "https://rs.stage.lincsproject.ca/resource/ThinkingFrames?uri=http://temp.lincsproject.ca/anthologie/manuscript/1",
    imageCredits: "Codex palatinus graecus 23, page 79 (first page of the palatine anthology)",
    altText: "Cod. Pal. graec. 23, p. 79",
    title: (
      <Translate id="homepage.carousel.apTitle" description="Anthologia graeca carousel title">
        Digital collaborative edition of the Greek anthology
      </Translate>
    ),
    description: (
      <Translate
        id="dataset.ap.description.short"
        description="Anthologia graeca dataset short description"
      >
        Anthologia graeca gathers information and data on the Greek Anthology: more than 4000 pieces
        of Greek epigrammatic poetry from the classical to the Byzantine period.
      </Translate>
    ),
    buttonName1: (
      <Translate id="homepage.carousel.edLabel" description="Carousel Explore Dataset label">
        Explore Anthologia graeca
      </Translate>
    ),
    link1:
      "https://rs.stage.lincsproject.ca/resource/ThinkingFrames?view=search-anthologiaPalatina",
    buttonName2: (
      <Translate id="homepage.carousel.edLabel" description="Carousel Explore Dataset label">
        Learn about Anthologia graeca
      </Translate>
    ),
    link2: "/docs/explore-data/project-datasets/anthologia-graeca/",
  },
  {
    id: "Yellow Nineties",
    src: yellowNinetiesImage,
    title: (
      <Translate id="homepage.carousel.ynTitle" description="Yellow Nineties carousel title">
        Yellow Nineties 2.0
      </Translate>
    ),
    description: (
      <Translate
        id="homepage.carousel.ynDescription"
        description="Yellow Nineties carousel description"
      >
        Yellow Nineties 2.0 uses digital tools to advance knowledge of eight late-Victorian little
        magazines and the people who contributed to their production between 1889 and 1905.
      </Translate>
    ),
    buttonName1: (
      <Translate
        id="homepage.carousel.ynLabel"
        description="Carousel Explore Yellow Nineties button label"
      >
        Explore Y90s
      </Translate>
    ),
    link1: "https://rs.stage.lincsproject.ca/resource/ThinkingFrames?view=search-yellowNineties",
    buttonName2: (
      <Translate
        id="homepage.carousel.ynLabel"
        description="Carousel Learn Yellow Nineties button label"
      >
        Learn about Y90s
      </Translate>
    ),
    link2: "/docs/explore-data/project-datasets/yellow-nineties/",
  },
  {
    id: "Orlando",
    src: orlandoImage,
    title: (
      <Translate id="homepage.carousel.orlandoTitle" description="Orlando carousel title">
        Orlando
      </Translate>
    ),
    description: (
      <Translate
        id="homepage.carousel.orlandoDescription"
        description="Orlando carousel description"
      >
        The Orlando Project explores and harnesses the power of digital tools and methods to advance
        feminist literary scholarship.
      </Translate>
    ),
    buttonName1: (
      <Translate id="homepage.carousel.orlandoLabel" description="Carousel Explore Orlando label">
        Explore Orlando
      </Translate>
    ),
    link1: "https://rs.stage.lincsproject.ca/resource/ThinkingFrames?view=search-orlando",
    buttonName2: (
      <Translate id="homepage.carousel.orlandoLabel" description="Carousel Explore Orlando label">
        Learn about Orlando
      </Translate>
    ),
    link2: "/docs/explore-data/project-datasets/orlando/",
  },
];

function HomepageCarousel() {
  return (
    <div>
      <div className={styles.mobileCarousel}>
        <Swiper
          modules={[Navigation, Pagination, A11y, Keyboard]}
          spaceBetween={8}
          breakpoints={{
            880: {
              slidesPerView: 2.75,
              slidesPerGroup: 1,
              spaceBetween: 8,
            },
            780: {
              slidesPerView: 2.5,
              slidesPerGroup: 1,
              spaceBetween: 8,
            },
            680: {
              slidesPerView: 2.25,
              slidesPerGroup: 1,
              spaceBetween: 8,
            },
            580: {
              slidesPerView: 1.75,
              slidesPerGroup: 1,
              spaceBetween: 8,
            },
            480: {
              slidesPerView: 1.5,
              slidesPerGroup: 1,
              spaceBetween: 8,
            },
            380: {
              slidesPerView: 1.25,
              slidesPerGroup: 1,
              spaceBetween: 8,
            },
          }}
          rewind={true}
          pagination={{ clickable: true }}
          keyboard={{ enabled: true }}
          style={{ "--swiper-pagination-color": "#107386" }}
        >
          {carouselContents.map((object) => (
            <SwiperSlide key={object.id}>
              <MobileCarouselCard {...object} />
            </SwiperSlide>
          ))}
        </Swiper>
      </div>

      <div className={styles.carouselContainer}>
        <div className={styles["carousel-swiper-button-next"]}>
          <span>
            <FontAwesomeIcon icon={faChevronRight} />
          </span>
        </div>
        <div className={styles["carousel-swiper-button-prev"]}>
          <span>
            <FontAwesomeIcon icon={faChevronLeft} />
          </span>
        </div>
        <div className={styles.carousel}>
          <Swiper
            modules={[Navigation, A11y, Keyboard]}
            spaceBetween={12}
            slidesPerView={2.25}
            navigation={{
              nextEl: `.${styles["carousel-swiper-button-next"]}`,
              prevEl: `.${styles["carousel-swiper-button-prev"]}`,
            }}
            keyboard={{ enabled: true }}
            style={{ "--swiper-pagination-color": "#107386" }}
          >
            {carouselContents.map((object) => (
              <SwiperSlide key={object.id}>
                <CarouselCard {...object} />
              </SwiperSlide>
            ))}
          </Swiper>
        </div>
      </div>
    </div>
  );
}

export default HomepageCarousel;
