---
id: metadata
title: Metadata
definition: Informations structurées qui décrivent ou expliquent un objet d’information afin qu’il puisse être recherché, récupéré, contextualisé, validé, conservé ou géré.
---

<head>
<script src="https://kit.fontawesome.com/2d8ee6f374.js" crossorigin="anonymous"></script>
</head>

## <i className="fa-solid fa-person-digging"></i> **Cette page est en construction.** <i className="fa-solid fa-person-digging"></i>
