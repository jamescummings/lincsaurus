---
id: open-researcher-and-contributor-id
title: Open Researcher and Contributor ID (ORCID)
definition: A not-for-profit organization that provides free Uniform Resource Identifiers (URIs) to researchers so they can be connected to their scholarship and bibliographic output.
---

<head>
<script src="https://kit.fontawesome.com/2d8ee6f374.js" crossorigin="anonymous"></script>
</head>

## <i className="fa-solid fa-person-digging"></i> **Cette page est en construction.** <i className="fa-solid fa-person-digging"></i>
