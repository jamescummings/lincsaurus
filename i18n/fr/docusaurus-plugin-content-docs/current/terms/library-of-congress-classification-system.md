---
id: library-of-congress-classification-system
title: Library of Congress Classification System (LCC)
definition: Un système de classification des bibliothèques couramment utilisé par les grandes bibliothèques de recherche et universitaires pour organiser les collections imprimées.
---

<head>
<script src="https://kit.fontawesome.com/2d8ee6f374.js" crossorigin="anonymous"></script>
</head>

## <i className="fa-solid fa-person-digging"></i> **Cette page est en construction.** <i className="fa-solid fa-person-digging"></i>
