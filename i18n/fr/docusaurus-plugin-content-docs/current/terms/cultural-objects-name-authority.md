---
id: cultural-objects-name-authority
title: Cultural Objects Name Authority (CONA)
definition: L’un des cinq vocabulaires Getty contenant des identifiants de ressources uniformes (URI) pour les titres, les attributions des créateurs, les caractéristiques physiques et les sujets représentés concernant les œuvres d’art, l’architecture et le patrimoine culturel visuel.
---

<head>
<script src="https://kit.fontawesome.com/2d8ee6f374.js" crossorigin="anonymous"></script>
</head>

## <i className="fa-solid fa-person-digging"></i> **Cette page est en construction.** <i className="fa-solid fa-person-digging"></i>
