---
id: object-oriented-ontology
title: Ontologie orientée objet
definition: Une ontologie qui utilise des objets pour relier des choses, des concepts, des personnes, du temps et des lieux.
---

<head>
<script src="https://kit.fontawesome.com/2d8ee6f374.js" crossorigin="anonymous"></script>
</head>

## <i className="fa-solid fa-person-digging"></i> **Cette page est en construction.** <i className="fa-solid fa-person-digging"></i>
