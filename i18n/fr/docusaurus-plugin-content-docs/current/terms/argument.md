---
id: argument
title: Dispute
definition: Une série de raisons, d’énoncés ou de faits dans un schéma de métadonnées destinés à soutenir ou à établir un point de vue, plutôt qu’un énoncé neutre, qui décrit une personne, un événement ou un objet.
---

Un argument est une série de raisons, de déclarations ou de faits destinés à soutenir ou à établir un point de vue, plutôt qu’une déclaration neutre qui décrit une personne, un événement ou un objet. Le schéma de métadonnées standard ne permet pas nécessairement la démarcation d’un argument ou son affectation à un auteur. Cependant, en utilisant :Term[CIDOC CRM]{#cidoc-crm}, il est possible d’enregistrer des instances d’argumentation dans les métadonnées, ce qui permet de représenter, de comparer, de comparer, :Term[réconcilié]{#reconciliation}, et progressé par les chercheurs.

## Exemples

- British Museum (2021) [“ResearchSpace: Argument and Uncertainty”](https://researchspace.org/argument/)

## Autres ressources

- Doerr, Kritsotaki, & Boutsika (2011) [“Factual Argumentation—A Core Model for Assertions Making”](https://dl.acm.org/doi/10.1145/1921614.1921615)
- Paveprime Ltd (2019) _[CRMinf: The Argumentation Model](https://cidoc-crm.org/crminf/sites/default/files/CRMinf%20ver%2010.1.pdf)_
- Stead (2015) [“CRMinf: The Argumentation Model”](https://www.youtube.com/watch?v=iZz9Q-wdyY0) [Vidéo]
