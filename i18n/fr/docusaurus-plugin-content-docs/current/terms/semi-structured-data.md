---
id: semi-structured-data
title: Données semi-structurées
definition: Données présentant une certaine structure, mais pas d’une manière permettant d’extraire facilement des entités et des relations sans travail manuel.
---

<head>
<script src="https://kit.fontawesome.com/2d8ee6f374.js" crossorigin="anonymous"></script>
</head>

## <i className="fa-solid fa-person-digging"></i> **Cette page est en construction.** <i className="fa-solid fa-person-digging"></i>
