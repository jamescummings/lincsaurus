---
id: event-oriented-ontology
title: Ontologie orientée événement
definition: Une ontologie qui utilise des événements pour relier les choses, les concepts, les personnes, le temps et le lieu.
---

<head>
<script src="https://kit.fontawesome.com/2d8ee6f374.js" crossorigin="anonymous"></script>
</head>

## <i className="fa-solid fa-person-digging"></i> **Cette page est en construction.** <i className="fa-solid fa-person-digging"></i>
