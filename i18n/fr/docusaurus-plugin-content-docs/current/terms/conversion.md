---
id: conversion
title: Conversion
definition: Le processus de modification des données d’un format à un autre.
---

La conversion de données est le processus de modification des données d’un format à un autre. L’objectif de la conversion de données est d’empêcher la perte ou la corruption de données en maintenant l’intégrité des données et des structures intégrées. La conversion des données doit avoir pour objectif de maintenir ou d’augmenter le sens communiqué par les données, leur structure et leur format.

## Autres ressources

- [Data Conversion (Wikipedia)](https://en.wikipedia.org/wiki/Data_conversion)
- Langmann (2022) [“How to Convert an Excel Spreadsheet to XML”](https://spreadsheeto.com/xml/)
- W3C (2015) [“Generating RDF from Tabular Data on the Web”](https://www.w3.org/TR/csv2rdf/)
