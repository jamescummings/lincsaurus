---
id: union-list-of-artist-names
title: Union List of Artist Names (ULAN)
definition: L’un des cinq vocabulaires Getty contenant des identifiants de ressources uniformes (URI) pour les noms, les relations et les informations biographiques concernant les personnes et les personnes morales liées à l’art, à l’architecture et à d’autres cultures matérielles.
---

<head>
<script src="https://kit.fontawesome.com/2d8ee6f374.js" crossorigin="anonymous"></script>
</head>

## <i className="fa-solid fa-person-digging"></i> **Cette page est en construction.** <i className="fa-solid fa-person-digging"></i>
