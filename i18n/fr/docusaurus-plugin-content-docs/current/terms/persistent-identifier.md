---
id: persistent-identifier
title: Persistent Identifier (PID)
definition: Une référence durable à un document, un fichier, une page Web ou un autre objet numérique.
---

<head>
<script src="https://kit.fontawesome.com/2d8ee6f374.js" crossorigin="anonymous"></script>
</head>

## <i className="fa-solid fa-person-digging"></i> **Cette page est en construction.** <i className="fa-solid fa-person-digging"></i>
