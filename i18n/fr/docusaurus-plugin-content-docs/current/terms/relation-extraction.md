---
id: relation-extraction
title: Extraction de relations (RE)
definition: La tâche de détecter, classer et extraire les relations sémantiques d’un texte.
---

<head>
<script src="https://kit.fontawesome.com/2d8ee6f374.js" crossorigin="anonymous"></script>
</head>

## <i className="fa-solid fa-person-digging"></i> **Cette page est en construction.** <i className="fa-solid fa-person-digging"></i>
