---
id: authority-file
title: Fichier d’autorité
definition: Un fichier d’autorité est une liste qui contient la manière faisant autorité de référencer des personnes, des lieux, des choses ou des concepts, généralement sous la forme d’un en-tête ou d’un identifiant numérique.
---

<head>
<script src="https://kit.fontawesome.com/2d8ee6f374.js" crossorigin="anonymous"></script>
</head>

## <i className="fa-solid fa-person-digging"></i> **Cette page est en construction.** <i className="fa-solid fa-person-digging"></i>
