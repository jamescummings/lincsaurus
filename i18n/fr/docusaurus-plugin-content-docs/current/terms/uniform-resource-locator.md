---
id: uniform-resource-locator
title: Localisateur de ressources uniforme (URL)
definition: Une déclaration qui décrit l’emplacement de quelque chose sur le Web spécifiquement pour localiser des actifs en ligne.
---

Une URL (Uniform Resource Locator) décrit l’emplacement de quelque chose sur le Web. C’est un type de :Term[Uniform Resource Identifier (URI)]{#uniform-resource-identifier} (toutes les URL sont des URI mais toutes les URI ne sont pas des URL) spécifiquement pour localiser les actifs en ligne. Les URL sont le plus souvent utilisées pour référencer des pages Web, mais ont des applications supplémentaires telles que les transferts de fichiers et les adresses e-mail. Cette application est indiquée par la première partie de l’URL, appelée le protocole : http désigne une page web, ftp un transfert de fichier, mailto une adresse email, etc.

## Exemples

- Dans l’URI suivante pour la page “À propos” du projet LINCS, vous savez qu’il s’agit d’une page Web sécurisée (https) avec le nom d’hôte LINCS (lincsproject.ca) et un chemin, menant finalement à un nom de fichier (what-is-lincs) à localiser de manière unique cette page sur le site Web de LINCS et sur Internet en général.

`https://lincsproject.ca/what-is-lincs/`

## Autres ressources

- Computer Hope (2021) [“URL”](https://www.computerhope.com/jargon/u/url.htm)
- Themeisle (2022) [“What Is a Website URL? The 3 Most Important Parts Explained”](https://themeisle.com/blog/what-is-a-website-url/)
- [URL (Wikipedia)](https://en.wikipedia.org/wiki/URL)
