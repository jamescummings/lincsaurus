---
id: canada-foundation-for-innovation
title: Fondation canadienne pour l’innovation (FCI)
definition: Une société sans but lucratif qui investit dans l’infrastructure de recherche des universités, des collèges, des hôpitaux de recherche et des établissements de recherche sans but lucratif du Canada.
---

La Fondation canadienne pour l’innovation (FCI) est une société sans but lucratif qui investit dans l’infrastructure de recherche des universités, des collèges, des hôpitaux de recherche et des établissements de recherche sans but lucratif du Canada. Le LINCS est financé par une subvention Cyberinfrastructure de la FCI.

## Autres ressources

- Canada Foundation for Innovation (2022) [“About Us”](https://www.innovation.ca/about)
