---
id: research-data-management
title: Gestion des données de recherche (RDM)
definition: Processus et activités exécutés par les chercheurs tout au long du cycle de vie d’un projet de recherche pour guider la collecte, l’organisation, la documentation, le stockage, l’accessibilité, la réutilisabilité et la préservation des données.
---

<head>
<script src="https://kit.fontawesome.com/2d8ee6f374.js" crossorigin="anonymous"></script>
</head>

## <i className="fa-solid fa-person-digging"></i> **Cette page est en construction.** <i className="fa-solid fa-person-digging"></i>
