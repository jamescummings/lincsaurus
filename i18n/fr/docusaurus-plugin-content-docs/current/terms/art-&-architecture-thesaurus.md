---
id: art-&-architecture-thesaurus
title: Art & Architecture Thesaurus (AAT)
definition: L’un des cinq vocabulaires Getty contenant des identifiants de ressources uniformes (URI) pour les termes génériques liés à l’art, à l’architecture et au patrimoine culturel visuel.
---

<head>
<script src="https://kit.fontawesome.com/2d8ee6f374.js" crossorigin="anonymous"></script>
</head>

## <i className="fa-solid fa-person-digging"></i> **Cette page est en construction.** <i className="fa-solid fa-person-digging"></i>
