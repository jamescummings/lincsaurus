---
id: care-principles-for-indigenous-data-governance
title: Principes de CARE pour la gouvernance des données autochtones
definition: Un ensemble de principes (bénéfice collectif, autorité de contrôle, responsabilité et éthique) pour faire progresser les droits collectifs et individuels en matière de données dans le mouvement des données ouvertes.
---

<head>
<script src="https://kit.fontawesome.com/2d8ee6f374.js" crossorigin="anonymous"></script>
</head>

## <i className="fa-solid fa-person-digging"></i> **Cette page est en construction.** <i className="fa-solid fa-person-digging"></i>
