---
id: mapping
title: Cartographie
definition: Processus conceptuel consistant à associer des valeurs ou des champs de métadonnées équivalents d’un schéma à un autre.
---

<head>
<script src="https://kit.fontawesome.com/2d8ee6f374.js" crossorigin="anonymous"></script>
</head>

## <i className="fa-solid fa-person-digging"></i> **Cette page est en construction.** <i className="fa-solid fa-person-digging"></i>
