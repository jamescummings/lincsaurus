---
id: structured-data
title: Données structurées
definition: Données sous forme de feuilles de calcul, de bases de données relationnelles, de fichiers JSON, de fichiers RDF et de fichiers XML.
---

<head>
<script src="https://kit.fontawesome.com/2d8ee6f374.js" crossorigin="anonymous"></script>
</head>

## <i className="fa-solid fa-person-digging"></i> **Cette page est en construction.** <i className="fa-solid fa-person-digging"></i>
