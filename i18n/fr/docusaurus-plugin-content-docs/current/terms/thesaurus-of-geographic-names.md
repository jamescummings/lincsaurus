---
id: thesaurus-of-geographic-names
title: Thesaurus of Geographic Names (TGN)
definition: L’un des cinq vocabulaires Getty contenant des identifiants de ressources uniformes (URI) pour les noms, les relations, les types de lieux, les dates et les coordonnées.
---

<head>
<script src="https://kit.fontawesome.com/2d8ee6f374.js" crossorigin="anonymous"></script>
</head>

## <i className="fa-solid fa-person-digging"></i> **Cette page est en construction.** <i className="fa-solid fa-person-digging"></i>
