---
id: iconography-authority
title: Iconography Authority (IA)
definition: L’un des cinq vocabulaires Getty contenant des identifiants de ressources uniformes (URI) pour les noms propres, les relations, les thèmes et les dates liés aux récits iconographiques, aux personnages légendaires ou fictifs, aux événements historiques, aux œuvres littéraires et aux arts du spectacle.
---

<head>
<script src="https://kit.fontawesome.com/2d8ee6f374.js" crossorigin="anonymous"></script>
</head>

## <i className="fa-solid fa-person-digging"></i> **Cette page est en construction.** <i className="fa-solid fa-person-digging"></i>
