---
sidebar_position: 3
title: "Participer à la recherche"
description: "Inscrivez-vous pour participer à des études d’utilisabilité"
---

<head>
<script src="https://kit.fontawesome.com/2d8ee6f374.js" crossorigin="anonymous"></script>
</head>

## <i className="fa-solid fa-person-digging"></i> **Cette page est en construction.** <i className="fa-solid fa-person-digging"></i>

{/*

Merci de votre intérêt à aider l'équipe LINCS à créer des plates-formes ouvertes et utilisables pour faire du travail en sciences humaines et sociales avec des données culturelles sur le :Term[Web sémantique]{#semantic-web} !

Votre participation aux études ci-dessous est volontaire. Il n'y a aucune compensation pour votre participation.

Toutes les études sont requises sous le numéro 22-01-009 du comité d'éthique de la recherche de l'Université de Guelph. Vous recevrez une lettre d'information complète avant de commencer les tests utilisateurs pour LINCS.
Pour plus d'informations sur l'une de ces études, contactez :

Kim Martin<br></br>
kmarti20@uoguelph.ca<br></br>
Président du conseil de recherche du LINCS<br></br>
Université de Guelph

:::info

Consultez les opportunités de test utilisateur décrites ci-dessous et remplissez le [formulaire d'inscription au test utilisateur](https://docs.google.com/forms/d/e/1FAIpQLSeTYpbrSbES-8eUdq5SZGlYuWv3Y8Z78MpuVGnnq-kHgAf0NQ/viewform) pour vous inscrire à participer !

:::

---

## Recherche à venir

Il n'y a actuellement aucune étude à venir.
*/}
