---
sidebar_position: 1
title: "Domaines de recherche"
edited: 1
cited: "Brown, Susan, Erin Canning, Kim Martin, and Sarah Roger. “Ethical Considerations in the Development of Responsible Linked Open Data Infrastructure.” In Ethics in Linked Data, edited by Kathleen Burlingame, Alexandra Provo, and B.M. Watson. Litwin Books, 2023."
last translated: 2023-07-26
---

LINCS entreprend un programme de recherche novateur et multidisciplinaire organisé autour de trois thèmes—Établir des liens, Naviguer à travers les échelles et Construire des connaissances—tout en collaborant avec des chercheurs engagés dans une variété de domaines et de champs d’investigation.

Le vaste corpus de données que LINCS mobilise pour la recherche culturelle comprend un éventail de données : des ensembles de données à convertir entièrement en :Term[Données ouvertes et liées (LOD)]{#linked-open-data} avec validation humaine, des ensembles de données qui recevront une certaine validation dans le cadre du processus de conversion, et des ensembles de données qui seront convertis de manière algorithmique et prêts à être validés et améliorés par les chercheurs intéressés et les citoyens chercheurs.

## Thèmes de recherche

### Établir des connexions

**Responsable :** Janelle Jenstad, University of Victoria

LINCS mettra en lumière la matière noire de l’histoire : les liens cachés et inattendus entre les personnes, les lieux, les événements et les œuvres culturelles à travers le temps, l’espace et les médias.

Actuellement, la capacité à poser de grandes questions sur les ensembles de données culturelles numérisées est entravée par le manque de bonnes :Term[métadonnées]{#metadata} qui relient les sources primaires aux contextes : quand elles ont été écrites, par qui, où, comment et pourquoi. LINCS permettra aux chercheurs de s’intéresser simultanément au contenu et au contexte des archives numériques. Une compréhension profondément contextualisée est fondamentale pour répondre à des questions importantes telles que la persistance de la discrimination sociale malgré l’égalité formelle, ou les politiques et pratiques qui alimentent l’économie créative.

L’un des principaux objectifs du projet LINCS est de rassembler des ensembles de données qui se chevauchent dans de multiples domaines académiques et linguistiques. LINCS jettera un pont entre les deux solitudes des ensembles de données culturelles linguistiquement divisées comme jamais auparavant, en s’associant à la plateforme universitaire Érudit sur le développement de l’:Term[ontologie]{#ontology} canadienne française et sur la mobilisation des LOD à des fins universitaires.

Au-delà du monde universitaire, l’interconnectivité créée par LINCS permettra à la culture canadienne d’occuper une place plus importante sur le web, de contextualiser en profondeur les résultats de recherche et d’offrir aux journalistes, aux écoliers et au public un meilleur accès à des sources de connaissances de qualité.

### Gérer le travail à l’échelle

**Responsable :** Stacy Allison-Cassin, Dalhousie University

La capacité de voir des modèles dans de grands ensembles de données, puis de zoomer pour examiner les preuves est essentielle à la recherche en sciences humaines, mais elle est restée insaisissable dans la plupart des contextes jusqu’à présent. LINCS permettra de passer de données granulaires à des vues distantes pour ceux qui étudient les interactions complexes qui contribuent au changement culturel.

Nous mobiliserons un riche ensemble de données musicales de chercheurs—enregistrements de spectacles, partitions de musique ancienne et ensembles de données ethnomusicologiques couvrant les traditions autochtones, indiennes orientales, folkloriques différentes et européennes du Canada—afin de permettre une étude comparative des influences, des mouvements et des réseaux. L’interconnexion de ces données avec celles de nos partenaires permettra une analyse encore plus large de l’impact des politiques culturelles et du financement, comme la création de la NPR aux États-Unis par rapport à la CBC au Canada.

De tels ensembles de données prosopographiques permettent de retracer la manière dont les identités culturelles circulent dans les cercles littéraires d’avant-garde, ou telles qu’elles sont appliquées aux citoyens indigènes et aux colons dans les registres des prisons canadiennes. Ils offrent un aperçu de nombreuses personnes qui sont autrement perdues dans l’histoire, et ont le potentiel d’établir un lien avec les Canadiens dans le cadre de projets inclusifs tels que le Panopticon numérique et d’autres ensembles de données sur les personnes “ordinaires.”

L’interaction entre le macro et le micro est d’une importance vitale dans les travaux sur la culture matérielle et textuelle. Les théoriciens et les praticiens de l’édition utiliseront les LOD pour mobiliser de nouveaux types d’éditions et de contenus de revues savantes. Ces sondages de la dynamique textuelle, utilisant des données qui mettent elles-mêmes en œuvre la textualité en réseau, fourniront des informations cruciales dans un monde où les conventions textuelles ont été perturbées par les outils numériques. LINCS permettra donc d’expérimenter de nouvelles formes de publication et de prototypes d’application.

### Construire les connaissances

**Responsable :** Jon Bath, Université de Saskatchewan

LINCS changera la façon dont les chercheurs travaillent sur le web en combinant les nombreux avantages des :Term[données hautement structurées]{#structured-data}. Par exemple, il fournira une documentation plus explicite sur l’organisation des données que celle disponible dans la plupart des bases de données, avec des liens vers les sources pour faciliter la provenance, l’accès et l’analyse.

L’une des principales promesses des LOD est qu’elle peut nous dire ce que nous ne savons pas encore grâce à l’inférence, l’extrapolation informatique d’informations qui ne sont pas explicitement énoncées dans les LOD, mais qui en découlent. LINCS transformera la découverte de connaissances en extrapolant des déductions à partir de millions de points de données. Dans ce domaine sous-développé de la recherche sur le :Term[web sémantique]{#semantic-web}, les intérêts des membres de l’équipe scientifique recoupent inévitablement ceux de l’équipe technique.

Une autre promesse est que de nouvelles formes de sérendipité naîtront des données sémantiquement significatives. La mise en relation d’ensembles de données offre des possibilités passionnantes de localiser des sources dans des endroits inattendus, recréant ainsi, à une échelle plus grande et plus complexe, les découvertes fortuites qui attendaient autrefois les chercheurs dans les archives ou les bibliothèques. De telles avancées permettront d’accélérer la découverte de connaissances et sont pertinentes pour la conception de moteurs de recherche tels que Google.

L’un des véritables défis des LOD pour la recherche en sciences humaines est l’intégration des nuances et des différences épistémologiques, y compris la prise en compte des objets frontières qui ont des significations différentes selon les domaines. LINCS veillera à ce que ses ontologies puissent représenter des épistémologies non hégémoniques et à intégrer d’autres représentations de la connaissance dans le web sémantique. Les LOD s’amélioreront également avec le temps, en saisissant de plus en plus les nuances et en éclairant davantage les zones d’ombre.

Dans son architecture de l’information, LINCS tiendra compte de la différence et de la diversité, y compris des modes de connaissance des groupes marginalisés. Surtout, LINCS fournira une infrastructure en réseau d’expertise et de connaissances en matière de :Term[Données liées (LD)]{#linked-data} qui permettra aux chercheurs canadiens de contribuer à la construction d’une meilleure écologie de l’information. Le Canada n’a rien de comparable à ce type d’infrastructure pour l’étude de l’histoire et de la culture humaines.

## Domaines de recherche

LINCS téléverse un large éventail de données sur le patrimoine culturel. Nous acceptons des ensembles de données structurés, semi-structurés et non structurés, allant des images aux textes, des cartes à la musique, pour autant que les données sources soient facilement accessibles. Les domaines d’investigation suivants reflètent les intérêts des chercheurs impliqués dans les premières étapes du projet LINCS. Nous sommes conscients qu’ils évolueront et se développeront au fil du temps.

* **Édition canadienne :** Les chercheurs se plongeront dans l’histoire de l’édition canadienne à la recherche de modèles et d’effets locaux—le genre de liens qui élucident les récentes controverses sur les préjugés sexistes, les concepts de nation ou les revendications d’identité autochtone au sein de l’establishment littéraire canadien.

* **Géohumanités :** Ce groupe se concentrera sur les outils et les méthodes permettant de faire face aux changements historiques, à la réconciliation et aux différences culturelles et méthodologiques, dans la mesure où ils s’appliquent à l’utilisation d’entités spatiales dans le cadre des LOD. Les interconnexions entre les données et les sources tireront parti du tournant spatial largement quantitatif de la science récente, de manière à soutenir également les enquêtes qualitatives. Le groupe assurera la liaison avec les réseaux Linked Pasts et Pelagios.

* **Savoirs autochtones :** Les chercheurs s’attaqueront aux défis associés à la représentation des matériaux et des perspectives autochtones sous forme de LD, y compris les questions de souveraineté, de confiance, d’accès et de décolonisation des métadonnées, ainsi que les relations avec la communauté et la terre. Ce groupe pourrait avancer de manière indépendante ou se combiner avec le groupe sur les épistémologies résistantes.

* **Systèmes de connaissance :**​ Des chercheurs spécialisés dans différentes formes de représentation des connaissances s’attaqueront aux promesses et aux défis des LOD afin de faire progresser la recherche pour les chercheurs en sciences humaines du monde entier.

* **Histoire de la littérature et du spectacle :** Une riche couverture de l’histoire de la littérature et du spectacle, en particulier de l’écriture, des réseaux et de la réception des femmes, permettra aux chercheurs d’explorer les modèles de mouvement et les réseaux professionnels formateurs dans l’histoire du divertissement.

* **Londres et l’Empire britannique :** Les chercheurs étudieront l’impact culturel, politique et économique de la Grande-Bretagne, ainsi que le Canada en tant que colonie de peuplement, grâce à l’agrégation de multiples ensembles de données couvrant la période allant du Moyen Âge à nos jours.

* **Cultures matérielles et textuelles :** Les chercheurs qui s’intéressent à la culture matérielle au sein de la communauté muséale mobiliseront les LOD pour inviter à une analyse basée sur l’agrégation des propriétés physiques de nombreux artefacts historiques. Les spécialistes des textes numériques sonderont les documents accessibles par calcul afin d’identifier des modèles textuels croisés qui sont actuellement inaccessibles.

* **Prosopographie :** Les méthodes numériques ont renouvelé le domaine de la prosopographie ou de la personographie, l’étude des personnes en groupes, en mobilisant des données granulaires sur les individus comme base d’une enquête plus large. Les groupes prosopographiques bénéficieront de données biographiques liées pour exploiter les dimensions transdisciplinaires, transgénériques et transnationales non réalisées des écologies de recherche sémantique.

* **Épistemologies résistantes :** LINCS inclut des chercheurs dont les ensembles de données intègrent des compréhensions non hégémoniques de l’histoire ou de la culture contemporaine, et de la manière dont la connaissance fonctionne pour résister aux visions dominantes du monde. L’inclusion de données fondées sur de telles épistémologies contribuera à intégrer dans le web sémantique des représentations de connaissances marginalisées et résistantes.
