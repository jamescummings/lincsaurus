---
sidebar_position: 1
title: "Énoncé de mission"
description: "Notre engagement en faveur de l’accès, de la reconnaissance, de la durabilité et de l’équité"
sidebar_class_name: "hide"
---

<head>
<script src="https://kit.fontawesome.com/2d8ee6f374.js" crossorigin="anonymous"></script>
</head>

## <i className="fa-solid fa-person-digging"></i> **Cette page est en construction.** <i className="fa-solid fa-person-digging"></i>
